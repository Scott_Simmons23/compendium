
## FastParquet to handle large filesize
from platform import release
from re import sub
from fastparquet import ParquetFile
from numpy.core.fromnumeric import shape
from numpy.core.numeric import indices
from numpy.lib.npyio import save
from tsfresh.feature_extraction import data

## tsFresh and project imports
from tsfresh.feature_extraction.settings import from_columns
from tsfresh.feature_selection import relevance
from tsfresh.transformers import FeatureSelector
from RSAM_get_data import get_y,load_target_RSAM
from RSAM_extraction import custom_make_fc
from RSAM_utils import save_dict,find_files

## By procedure
from statsmodels.stats.multitest import multipletests

## 
from multiprocessing import Pool, cpu_count

## General
import sys,os,yaml
from glob import glob
import numpy as np
import pandas as pd


def make_target(datelist):
    ys = []
    for f in datelist: # this approach is needed to handle missing days
        #date = find_dates(f)
        #print(date)
        yi = load_target_RSAM(f)
        ys.append(yi)
    y = pd.concat(ys,axis = 0).sort_index()
    return y

def chunk(index,nchunks,chunksize):
    '''Split up indices into unevenly sized subarrays of indices'''
    idx = []
    [ idx.append(index[chunksize*i:chunksize*(i+1)+1]) for i in range(0,nchunks)  ] # evenly spaced arrays that overlap by 1 point
    idx.append(index[nchunks*chunksize :]) # the remainder
    return idx

def _calc_pval_(X,y,q):
    X.set_index(y.index,inplace=True) ##Xneeded?
    selector = FeatureSelector(ml_task="regression",fdr_level=q).fit(X,y) # intialise selector transformer object
    return selector.features,selector.p_values

def pvalues_from_chunks(fhandle,y,fdr = 1e-6,chunksize = 1000,save = False):

    ## Read in as a parquet file
    pf = ParquetFile(fhandle)
    nrows = pf.count
    ncols = len(pf.columns)-1 # subtract 1 to avoid index being included
    nchunks = int(np.floor(ncols/chunksize))
    chunked_idx = chunk(np.linspace(0,ncols,ncols,dtype=int),nchunks,chunksize)

    #y = get_y(date,ndays = int(nrows/144))

    names = p_vals = []
    #p_vals = []
    
    for i in chunked_idx:           ## iterate over subsets of columns and perform feature_selection/univariate hypothesis tests
        X = pf.to_pandas(columns = pf.columns[i[0]:i[-1] ] ) ## Select features and create feature dictionaries
        X.set_index(y.index,inplace=True) ## give them the same index
        features,p = _calc_pval_(X,y,fdr)
        
        names = names + features # trick to concatenate lists
        p_vals = p_vals + p.tolist()

    return p_vals,names #,relevant

def results_tbl(sorted_names,sorted_p,relevant,fdr_string):
    tbl = pd.DataFrame(data = {'features' : sorted_names,'p_vals' : sorted_p,'relevance' : relevant} )
    chosen = list(tbl.features.loc[tbl.relevance == True])
    f,subf = custom_make_fc( chosen  )

    print('%d Relevant Features Chosen\n' % len(chosen),f,'\n',subf)

    tbl.to_hdf('TABLE_selected_features'+fdr_string+'.h5',key = 'test') # Save the Feature names and p_values table
    save_dict('selected_features'+fdr_string+'.yaml',f)
    save_dict('selected_subfeatures'+fdr_string+'.yaml',subf)
    return

def by_procedure(fname,y,
                fdr = [1e-6],
                chunksize = 1000,
                save = False,
                display = False):

    '''Perform the b-y procedure after doing hypothesis tests on chunks of the problem data'''

    p_vals,names = pvalues_from_chunks(fname,y,*fdr,chunksize,save)
    sort_idx = np.argsort(p_vals)
    sorted_names = [names[i] for i in sort_idx]
    sorted_p = np.array(p_vals)[sort_idx.tolist()]

    if len(fdr) < 2:
        fdr_string = '_fdr_level_'+ '{:.1e}'.format(*fdr)
        relevant =  multipletests(sorted_p, *fdr, 'fdr_by',is_sorted=True)[0] # perform the by procedure
        results_tbl(sorted_names,sorted_p,relevant,fdr_string)
    else:
        ## needs work
        with Pool() as pool:
            func = lambda x: multipletests(sorted_p, x, 'fdr_by',is_sorted=True)[0] ##?? change
            result = pool.map(func,fdr)
    return

if __name__ == '__main__':

    ## Test
    X_name = 'FEAT_ON_FEAT_subfeatures.gzip' #sys.argv[1]
    fdr,chunksize = [[1e-8],70]
    
    datelist = ['2019-12-09 00:00:00','2017-11-07 00:00:00'] #'2016-10-07 00:00:00'
    y = make_target(datelist)
    
    ## file_matrix, fdr, chunksize
    #X_name,fdr,chunksize = sys.argv[1:4]
    print(sys.argv[1:4])
    by_procedure(X_name,y,fdr,chunksize)