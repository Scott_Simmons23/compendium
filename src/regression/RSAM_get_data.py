#!/usr/bin/env python3


''''''

## General imports
import traceback,datetime,os, sys, shutil, warnings, gc, joblib,yaml
from numpy.lib.function_base import disp
from glob import glob
import numpy as np
from datetime import datetime, timedelta, date
import pandas as pd
from pandas._libs.tslibs.timestamps import Timestamp
from multiprocessing import Pool,cpu_count
from time import time
from functools import reduce
from matplotlib import dates


#______________________________________________________________________________IMPORT___________________________________________________________________________________
#The first 5 functions were adapted from https://github.com/ddempsey/whakaari/blob/master/whakaari/__init__.py

# ObsPy imports
try:
    from obspy.clients.fdsn import Client as FDSNClient 
    from obspy import UTCDateTime, read_inventory 
    with warnings.catch_warnings():
        warnings.simplefilter('ignore')
        from obspy.signal.filter import bandpass
    from obspy.io.mseed import ObsPyMSEEDFilesizeTooSmallError
    from obspy.clients.fdsn.header import FDSNNoDataException
    failedobspyimport = False
except:
    failedobspyimport = True

makedir = lambda name: os.makedirs(name, exist_ok=True)

## String/timestamp processing
def datetimeify(t):
    """ Return datetime object corresponding to input string.

        Parameters:
        -----------
        t : str, datetime.datetime
            Date string to convert to datetime object.

        Returns:
        --------
        datetime : datetime.datetime
            Datetime object corresponding to input string.

        Notes:
        ------
        This function tries several datetime string formats, and raises a ValueError if none work.
    """
    if type(t) in [datetime, Timestamp]:
        return t
    fmts = ['%Y-%m-%d %H:%M:%S', '%Y-%m-%d', '%Y %m %d %H %M %S',]
    for fmt in fmts:
        try:
            return datetime.strptime(t, fmt)
        except ValueError:
            pass
    raise ValueError("time data '{:s}' not a recognized format".format(t))

def to_nztimezone(t):
    """ Routine to convert UTC to NZ time zone.
    """
    from dateutil import tz
    utctz = tz.gettz('UTC')
    nztz = tz.gettz('Pacific/Auckland')
    return [ti.replace(tzinfo=utctz).astimezone(nztz) for ti in pd.to_datetime(t)]

## Use Obspy API to obtain  data from the API
def get_data_for_day(station,t0,i = 0):
    """ Download WIZ data for given 24 hour period, writing data to temporary file.

        Parameters:
        -----------
        i : integer
            Number of days that 24 hour download period is offset from initial date.
        t0 : datetime.datetime
            Initial date of data download period.
        
    """
    wd = os.getcwd()
    path = os.path.join(os.path.join(os.path.join(wd,'tmp'),'RSAM_raw'),station) # create a temporary directory
    makedir(path)

    t0 = UTCDateTime(t0)

    # open clients
    client = FDSNClient("GEONET")
    client_nrt = FDSNClient('https://service-nrt.geonet.org.nz')
    
    daysec = 24*3600
    
    data_streams = [2,5] # frequencies to isolate (RSAM)
    names = ['rsam'] ## this is actually an incorrect name for the velocity data.

    # download data
    datas = []
    try:
        site = client.get_stations(starttime=t0+i*daysec, endtime=t0 + (i+1)*daysec, station=station, level="response", channel="HHZ")
    except FDSNNoDataException:
        pass

    try:
        WIZ = client.get_waveforms('NZ',station, "10", "HHZ", t0+i*daysec, t0 + (i+1)*daysec)
        
        # if less than 1 day of data, try different client
        if len(WIZ.traces[0].data) < 600*100:
            raise FDSNNoDataException('')
    except ObsPyMSEEDFilesizeTooSmallError:
        return
    except FDSNNoDataException:
        try:
            WIZ = client_nrt.get_waveforms('NZ',station, "10", "HHZ", t0+i*daysec, t0 + (i+1)*daysec)
        except FDSNNoDataException:
            return

    # process frequency bands
    WIZ.remove_sensitivity(inventory=site)
    data = WIZ.traces[0].data
    ti = WIZ.traces[0].meta['starttime']
    fname1 = station + '_' + str(t0.year) +'_' + str(t0.month) + '_'+ str(t0.day+i) +'.h5'

    Nm = int(8.64e6) # number of measuremets in a day

    if fname1 in os.listdir(path): # Check if the file we want to write already exists
        print('{} already exists in temp folder'.format(fname1))
        return
    else:
            # round start time to nearest 10 min increment
        #tiday = UTCDateTime("{:d}-{:02d}-{:02d} 00:00:00".format(ti.year, ti.month, ti.day))
        #ti = tiday+int(np.round((ti-tiday)/600))*600
        #N = 600*100                             # 10 minute windows in seconds
        #Nm = int(N*np.floor(len(data)/N))
        #####for data_stream, name in zip(data_streams, names):

        filtered_data = bandpass(data, data_streams[0], data_streams[1], 100) # isolate frequencies between 2hz and 5hz
        ## multiplied by scaling factor 1e9
        datas.append(filtered_data[:Nm]*1e9) #####datas.append(filtered_data*1e9)
        ##datas.append(data[:Nm])

        datas = np.array(datas)
        df = pd.DataFrame(zip(*datas), columns=names) ##****************** need to simplify

        # write out temporary file
        fname = os.path.join(path,fname1)
        hdf = pd.HDFStore(fname)
        hdf.put(fname1,df, format= 'table')# write to a hdf5 file format for easy storage
        hdf.close()

def download_RSAM_from_station(station,ti,tf):
    """ Load in several days worth of data from the site

    Parameters:
    -----------
    ti : str, datetime.datetime
        First date to retrieve data (default is first date data available).
    tf : str, datetime.datetime
        Last date to retrieve data (default is current date).
    """
    if failedobspyimport:
        raise ImportError('ObsPy import failed, cannot update data.')

    wd = os.getcwd()
    path = os.path.join(os.path.join(os.path.join(wd,'tmp'),'RSAM_raw'),station)# create a directory to load the files into
    if not os.path.exists(path):
        os.makedirs(path) # check if path exists before doing

    ti = datetimeify(ti)
    tf = datetimeify(tf)

    ndays = (tf-ti).days # collect multiple days
    if ndays > 0:
        fnames = ["{}_{}_{}_{}.h5".format(station,str(ti.year),str(ti.month),str(ti.day + i)) for i in range(ndays) ]
        pars = [[station,ti,i] for i in range(ndays)]
    else: 
        fnames = ["{}_{}_{}_{}.h5".format(station,str(ti.year),str(ti.month),str(ti.day))] ## just extracting 1 day
        pars = [[station,ti,0]]

    exists = all(item in os.listdir(path) for item in fnames)

    if exists: # Check if the file we want to write already exists
        return
    else:
        # parallel data collection - creates temporary files in tmp
        p = Pool(6)
        p.starmap(get_data_for_day, pars)
        p.close()
        p.join()

## Creat an input dataframe with a datetime index and window_id
def create_RSAM_df(date,i,window_length = 10,station = 'WIZ'):
    ''' Load data corresponding to a single day stored in the tmp folder

    Parameters:
    -----------
    N : int
        number of days to download

    window_length : int
        The length of the window in seconds set to 10 by default as 10 = 100 measurements/s * 0.1s
    
    date : string
        date string with format 'YYYY-mm-dd HH:MM:ss'

    i : int
        index of the dataframe, this prevents mutliple dataframes sharing the same window id

    Returns:
    --------
    dfi : pandas dataframe
        a dataframe
    
    '''
    Npoints = 8.64e6 # 8.64e6 measurements per day
    id_start,id_stop = ( int(Npoints*i/window_length) , int(Npoints*(i+1)/window_length) ) #assumes non overlapping windows with 
    Num_windows = int(Npoints/window_length)
    
    arr = date.split(' ')[0].split('-') # split the string up
    yrNum,monthNum,dayNum = (arr[0],int(arr[1]),int(arr[2]))

    # loop through tmp/folder and assign window ids
    path = reduce(os.path.join,[os.getcwd(),'tmp','RSAM_raw',station])
    fname = '{}_{}_{}_{}.h5'.format(station,yrNum,monthNum,dayNum)
    pname = path + os.sep + fname
    dfi = pd.read_hdf(pname,fname)

    # Create id and time columns for 100ms intervals
    dfi['id'] = np.repeat((np.linspace( id_start, id_stop-1, Num_windows, dtype=int)),window_length) 
    dfi['seconds'] = np.linspace(Npoints*i,Npoints*(i+1)-1,int(Npoints))*1/100 # each row corresponds to 1/100 s

    return dfi

def delete_temp(station): # turn into lambda expression????
    '''Remove the temporary directory that stores the raw RSAM only'''
    wd = os.getcwd()
    path = os.path.join(os.path.join(os.path.join(wd,'tmp'),'RSAM_raw'),station)
    shutil.rmtree(path)
    return

## Read in WSRZ_tremor_data
def load_target_RSAM(date,display = False):
    """
    Load in the 10 min processed RSAM (10 minute mean absolute value of the RSAM signal).

    Note:
    -----
    We are predicting 10 min RSAM, to prevent data leakage the window date for the extracted features is set as the end of the 10 minute time interval
    e.g. looking at 2013-05-15 we need times from:
        2013-05-15 00:10:00 --> 2013-05-16 00:00:00
    """
    if isinstance(date,datetime) is False:
        period = pd.to_datetime(date.split(' ')[0]) # split the string up
    else:
        period = date
    start = period + timedelta(minutes=10)
    end = period + timedelta(days = 1)

    fhandle = glob('**{}WSRZ_tremor_data.csv'.format(os.sep),recursive = True)
    assert len(fhandle) != 0,'WSRZ_tremor_data.csv not located in (sub)directory'
    if display:
        print('\nReading in',fhandle[0],date,'\n' )
    
    WSRZ = pd.read_csv(fhandle[0]).drop(columns = ['mf','hf','dsar'])
    WSRZ.index = pd.to_datetime(WSRZ.time,format='%Y-%m-%d %H:%M:%S')
    WSRZ.drop(columns = ['time'], inplace= True)
    y = WSRZ.loc[start:end].squeeze() # convert to a pd.Series
    return y

def get_y(date,ndays):
    
    ''' load in several days worth of WSRZ data'''
    
    if ndays > 0:
        y = []
        for i in range(ndays):
            t = pd.to_datetime(date) + timedelta(days = i)
            y.append(load_target_RSAM(t))
        target = pd.concat(y,axis = 'rows').drop_duplicates()
    else:
        target = load_target_RSAM(date)

    return target

if __name__=='__main__':
    datetimeify(t)
    to_nztimezone(t)
    get_data_for_day(station, t0)
    download_RSAM_from_station(station, ti, tf)
    create_RSAM_df(station, date)
    get_y(date,ndays)