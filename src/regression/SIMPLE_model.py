import pandas as pd
from tsfresh.feature_selection import select_features
from sklearn.model_selection import cross_validate
from sklearn.metrics import r2_score,make_scorer
from tsfresh.feature_selection import select_features
import lightgbm as lgb
import numpy as np

def percentage_error(y_true,y_pred):
    epsilon = np.finfo(np.float64).eps
    return (np.average(np.abs(y_pred - y_true) / np.maximum(np.abs(y_true),epsilon)))

def col(X):
    X.columns = [name.replace(':','').replace('"','').replace('(','').replace(')','').replace(',','') for name in X.columns]
    return X

Xtrain = pd.read_parquet('X_train_simple.gzip')
y_train = pd.read_csv('y_train_simple.csv').set_index('time')
Xtest = pd.read_parquet('X_test_simple.gzip')
y_test = pd.read_csv('y_test_simple.csv').set_index('time')

yy = y_train.iloc[:,0] ## Must be a series
yt = y_test.iloc[:,0]



## Select Features on Training data, cross-validate then test on hold out set

XX = col(Xtrain)
XX = XX.dropna(axis = 1,how = 'any')
XX.set_index(y_train.index,inplace = True)

xTest = col(Xtest)

X_1e__10 = select_features(XX,yy,ml_task = 'regression',fdr_level = 1e-10)
X_1e__8 = select_features(XX,yy,ml_task = 'regression',fdr_level = 1e-10)
X_1e__6 = select_features(XX,yy,ml_task = 'regression',fdr_level = 1e-6)
X_1e__4 = select_features(XX,yy,ml_task = 'regression',fdr_level = 1e-4)
X_1e__2 = select_features(XX,yy,ml_task = 'regression',fdr_level = 1e-2)
X_1e__1 = select_features(XX,yy,ml_task = 'regression',fdr_level = 1e-1)

model = lgb.LGBMRegressor()

## COMPARE FDR LEVELS
fit1 = model.fit(X_1e__10,yy) ## 1e-10
n1 = X_1e__10.columns
s1 = cross_validate(model,XX[n1],yy,cv = 10,scoring = make_scorer(percentage_error),return_train_score = True)
yhat = fit1.predict(xTest[n1])## OOS error

print('1e-10\n',s1)
print(r2_score(yt,yhat))
print(percentage_error(yt,yhat))
print('#---------------------------------------------------------------------------------------------------------------------#')

      
fit2 = model.fit(X_1e__8,yy) ## 1e-8
n1 = X_1e__8.columns
s1 = cross_validate(model,XX[n1],yy,cv = 10,scoring = make_scorer(percentage_error),return_train_score = True)
yhat = fit2.predict(xTest[n1])
      
print('1e-8\n',s1)
print(r2_score(yt,yhat))
print(percentage_error(yt,yhat))
print('#---------------------------------------------------------------------------------------------------------------------#')

fit3 = model.fit(X_1e__6,yy) ## 1e-6
n1 = X_1e__6.columns
s1 = cross_validate(model,XX[n1],yy,cv = 10,scoring = make_scorer(percentage_error),return_train_score = True)
yhat = fit3.predict(xTest[n1])
      
print('1e-6\n',s1)
print(r2_score(yt,yhat))
print(percentage_error(yt,yhat))
print('#---------------------------------------------------------------------------------------------------------------------#')


fit4 = model.fit(X_1e__4,yy) ## 1e-4
n1 = X_1e__4.columns
s1 = cross_validate(model,XX[n1],yy,cv = 10,scoring = make_scorer(percentage_error),return_train_score = True)
yhat = fit4.predict(xTest[n1])

print('1e-4\n',s1)
print(r2_score(yt,yhat))
print(percentage_error(yt,yhat))
print('#---------------------------------------------------------------------------------------------------------------------#')

fit5 = model.fit(X_1e__2,yy) ## 1e-2
n1 = X_1e__2.columns
s1 = cross_validate(model,XX[n1],yy,cv = 10,scoring = make_scorer(percentage_error),return_train_score = True)
yhat = fit5.predict(xTest[n1])

print('1e-2\n',s1)
print(r2_score(yt,yhat))
print(percentage_error(yt,yhat))
print('#---------------------------------------------------------------------------------------------------------------------#')

fit6 = model.fit(X_1e__1,yy) ## 1e-1
n1 = X_1e__1.columns
s1 = cross_validate(model,XX[n1],yy,cv = 10,scoring = make_scorer(percentage_error),return_train_score = True)
yhat = fit6.predict(xTest[n1])

print('1e-1\n',s1)
print(r2_score(yt,yhat))
print(percentage_error(yt,yhat))
print('#---------------------------------------------------------------------------------------------------------------------#')
