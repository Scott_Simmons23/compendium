
## For gridsearch
from os import mkdir
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import make_scorer
import pickle ## to ssave these models

from RSAM_predictions import train_test_split,load_training_data
from RSAM_utils import find_files

import time,sys
import numpy as np
from RSAM_predictions import percentage_error,transform_column_names
from RSAM_utils import*

from sklearn.linear_model import ElasticNet
import warnings
warnings.filterwarnings("ignore")

grid1 = {"alpha": [0.0001, 0.001, 0.01, 0.1, 1, 10, 100],"l1_ratio": np.arange(0.0, 1.0, 0.1),'fit_intercept' : [True,False]}
#grid2 = {'epsilon' : [1.0,1.1,1.2,1.35,1.5,1.75],"alpha": [0.0001, 0.001, 0.01, 0.1, 1, 10, 100],'fit_intercept' : [True,False] }


test_params = {"alpha": [0.0001]}

def optimise_Linear_model(LM,label,extension = '*_subfeatures.gzip',N = 5,params = test_params,feature_names = None,test_size = 0.33,default_scoring = False):

    fs = find_files(label=label,extension = extension,display=True)
    X,y = load_training_data(fs,feature_names = feature_names)
    X_train,X_test,y_train,y_test = train_test_split(X,y,test_size = test_size,random_state=2021)
    ##X1,_ = transform_column_names(X_train)
    ##X2,_ = transform_column_names(X_test)
    if default_scoring:
        GSCV = GridSearchCV(LM,param_grid = params,cv=N,verbose=1)
    else:
        ## use mape
        GSCV = GridSearchCV(LM,param_grid = params,scoring = make_scorer(percentage_error,greater_is_better=False),cv=N,verbose=1)
    
    ## Train
    start = time.perf_counter()
    GSCV.fit(X_train, y_train)
    print('training time:',time.perf_counter() - start,'\n')

    ## Output
    mod = GSCV.best_estimator_
    print('Best parameters:',GSCV.best_params_,'\nBest {:d} fold cross-validated'.format(N),-1*GSCV.best_score_)

    ## Accuracy when predicting unseen test data
    y_pred = mod.predict(X_test)
    mape = percentage_error(y_pred,y_test)
    print('\nMean Average Percentage Error evaluated on hold out set:',mape,'\n')

    return GSCV.best_params_,mod

def save_res(best_params,mod,fname = "Linear_regression"):
    makedir(m_path)
    Pkl_Filename = m_path + fname + '.pkl' 
    
    with open(Pkl_Filename, 'wb') as file:  ## pickle the model
            pickle.dump(mod, file)

    save_dict(m_path + fname + '_best_params.yaml',best_params)## Save parameter dictionary as a yaml
    return


if __name__ == '__main__':
    label = 'MINIMAL_TEST' #'_fdr_1e-8_'    ## Prefix for the feature matrix file names
    Folds = 3                              ## Number of rounds of cross validation

    ## Load in feature names (features selected from RFE)
    feats = None #load_list('200_feat_names.txt')

    ## Optimise
    print('\nElastic Net:\n------------')
    LM1 = ElasticNet(normalize=True,max_iter = 3000)
    p1,mod = optimise_Linear_model(LM1,label,N = Folds,params = test_params,feature_names = feats,default_scoring=False)
    save_res(p1,mod,'Elastic_net1')

    #print('\nHuber:\n------')
    #LM2 = HuberRegressor()
    #p2,mod = optimise_Linear_model(LM2,label,N = Folds,params = test_params,feature_names = feats)
    #save_res(p1,mod,'Huber1')
