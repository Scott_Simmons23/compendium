#!/bin/bash -e

## This code manually perfoms the univariate hypothesis testing and then outputs a table
## referring to the p_values and relevance of features for prediction.

#SBATCH --job-name    WKRI_BY_1e-30		#
#SBATCH --time        06:00:00			#
#SBATCH --mem         30GB			# the total memory required (~80GB)
#SBATCH --cpus-per-task=8       		# cores per task.
#SBATCH --ntasks-per-node=2			#
#SBATCH --nodes=2  				# number of requested nodes

## pass in the:
## feature_matrix filename , datestring , fdr_level

module load Python/3.8.2-gimkl-2020a
python RSAM_select_features.py 2019_12_05to2019_12_07_Efficient_selected_subfeatures.gzip '2019-12-05 00:00:00' 1e-04
