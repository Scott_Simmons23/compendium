#!/bin/bash -e

#SBATCH --job-name    concat_df
#SBATCH --time        2:00:00
#SBATCH --mem         25GB
#SBATCH --cpus-per-task=4                       # cores per task

module load Python/3.8.2-gimkl-2020a
python concat_df.py
