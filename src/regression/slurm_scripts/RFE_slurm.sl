#!/bin/bash -e

#SBATCH --job-name    RFE_500
#SBATCH --time        02:30:00
#SBATCH --mem         15GB
#SBATCH --cpus-per-task=8                       # cores per task.
#SBATCH --ntasks-per-node=2                     #
#SBATCH --nodes=2                               # number of requested nodes

module load Python/3.8.2-gimkl-2020a
python RFE.py
