#!/bin/bash -e

## This script will be used to test the calculation of features using MINIMAL, EFFICIENT and CUSTOM feature calculators.
## This script uses job arrays to perform the computation in parallel.

#SBATCH --job-name    WKRI_EFF
#SBATCH --time        22:30:00
#SBATCH --mem         80GB              ## Total Memory required (~80GB for EFFICIENT)
#SBATCH --cpus-per-task=16               ## cores per task.
#SBATCH --ntasks-per-node=2             ##
#SBATCH --nodes=2                       ## number of requested nodes
#SBATCH --array=0-32                     ## each array represents the index of a list of dates passed into the function


## First command line argument is the slurm array index (for the dates),
## then followed by the feature_calculator then subfeature_calculator (both yamls)
## > python <datestring> <feature_calc> <subfeature_calc>
##
## Alternatively, the code can be run without a feature calculator argument (i.e. the only argument is a datestring)
## > python <datestring>


module load Python/3.8.2-gimkl-2020a
python efficient_estimates.py ${SLURM_ARRAY_TASK_ID}
