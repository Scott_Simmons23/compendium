#!/bin/bash -e

#SBATCH --job-name    33_rand_test
#SBATCH --time        02:00:00
#SBATCH --mem         30GB
#SBATCH --cpus-per-task=8                       # cores per task.
#SBATCH --ntasks-per-node=2                     #
#SBATCH --nodes=2                               # number of requested nodes

module load Python/3.8.2-gimkl-2020a
python simple_RSAM_predict.py
