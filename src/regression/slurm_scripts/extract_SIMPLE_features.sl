#!/bin/bash -e

## This script is for the purpose of extracting all efficient features on 3 days from 
## 2019-11-05 00:00:00 to 2019-11-08 00:00:00

#SBATCH --job-name    WKRI_SIMPLE		##
#SBATCH --time        4:00:00			##
#SBATCH --mem         80GB			## the total memory required (~80GB)
#SBATCH --cpus-per-task=8       		## cores per task
#SBATCH --array=0-58                     	## each array represents the index of a list of dates

module load Python/3.8.2-gimkl-2020a
python simple_features.py ${SLURM_ARRAY_TASK_ID}
