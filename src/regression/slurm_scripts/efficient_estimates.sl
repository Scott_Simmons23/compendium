#!/bin/bash -e

#SBATCH --job-name    WhiteIs_EFF_long
#SBATCH --time        15:00:00
#SBATCH --mem         80GB
#SBATCH --cpus-per-task=4       # cores per task.

module load Python/3.8.2-gimkl-2020a

python efficient_estimates.py
