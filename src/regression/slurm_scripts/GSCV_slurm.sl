#!/bin/bash -e

#SBATCH --job-name    Grid_1500
#SBATCH --time        5:30:00
#SBATCH --mem         25GB
#SBATCH --cpus-per-task=24                       # cores per task

## arguments: output prefix,columns to select from dataframes 

module load Python/3.8.2-gimkl-2020a
python optimise_LGBM_model.py '1500_features_by_pvals_grid_2' '1500_feat_p_vals.txt'
