## Create a csv file with the dates on it called dates.csv
import csv
import pandas as pd

ti = '2019-12-05 00:00:00'
tf = '2019-12-08 00:00:00'

def main():
    times = list(map(str,pd.date_range(ti,tf, freq = 'D')))
    csv_file =  open("dates.csv", 'w')
    wr = csv.writer(csv_file, delimiter=",")
    [wr.writerow([item]) for item in times]
    csv_file.close()

    print(pd.read_csv('dates.csv').head())

if __name__ == '__main__':
    main()

