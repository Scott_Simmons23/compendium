from cProfile import label
import os,yaml,traceback
from unicodedata import name
from tsfresh.feature_extraction import data
from posixpath import dirname
import numpy as np
import pandas as pd
from glob import glob
from functools import reduce
from joblib import dump,load

from RSAM_get_data import get_data_for_day, load_target_RSAM
import dask.array as da
import dask.dataframe as dd

makedir = lambda name: os.makedirs(name, exist_ok=True)

## Globals
## These give a very specific directory distructure that all the model data is stored in
path = reduce(os.path.join,[os.getcwd(),'tmp','feature_matrices',''])
p_path = reduce(os.path.join,[os.getcwd(),'tmp','p_values',''])
m_path = reduce(os.path.join,[os.getcwd(),'tmp','models',''])
r_path = reduce(os.path.join,[os.getcwd(),'tmp','results','']) # store the model results


## IO functions
# Save feature calculator as a yaml file
def save_dict(fname,fc):
    ## Write the output to a yaml file (so that it may be loaded in as a dict)
    with open(fname, 'w') as yaml_file:
        yaml.dump(fc, yaml_file, default_flow_style=False)
    print('\n','Saved: ',fname,'\n')
    return

# Save feature matrix as df
def save_feat(X,dates,string):
    ''' Store the dataframe as a parquet in tmp folder'''
    #path = reduce(os.path.join,[os.getcwd(),'tmp','feature_matrices',''])
    makedir(path)
    fname = path + '{}to{}_{}.gzip'.format(str(dates[0])[:10].replace('-','_'),str(dates[-1])[:10].replace('-','_'),string) # write the dataframe to an parquet file      
    
    if fname in os.listdir(path): # Check if the file we want to write already exists
        print('{} already exists in temp folder'.format(fname))
        return
    try:
        X.to_parquet(fname,compression='gzip')  
    except Exception:
        traceback.print_exc()
    return

# Read in yaml
def open_yaml(fname):
    with open(fname, 'r') as yaml_file:
        d = yaml.load(yaml_file)
    return d

# Save table of p_values and features as a csv
def save_pvals(p_values,feature_names,dates,label):
    #path = reduce(os.path.join,[os.getcwd(),'tmp','feature_matrices',''])
    makedir(p_path)
    fname = p_path + '{}to{}_pvals_{}_.csv'.format(str(dates[0])[:10].replace('-','_'),str(dates[-1])[:10].replace('-','_'),label) # write the dataframe to an parquet file      
    
    pd.DataFrame(data = {'Features':feature_names,'p_values':p_values}).to_csv(fname,index = False)
    #np.savetxt(fname,p_values,delimiter=',')
    return

# Locate a feature matrix in memory
def find_matrix(dates,label,option = 1):
    ''' In the event that subfeature extraxction has failed, we can load the stored feature matrix instead of 
        re-running custom_extract_subfeatures() from scratch
        
        label : string
            e.g. Efficient/custom/Minimal'''
    
    name = 'features' if option == 1 else 'subfeatures' # choose whether to find the subfeature or feature matrix

    #fdir = 'tmp/feature_matrices/'
    f = glob('**{}{}to{}_{}_{}.gzip'.format(os.sep,str(dates[0])[:10].replace('-','_'),str(dates[-1])[:10].replace('-','_'),label,name),recursive = True)
    if len(f) != 0:
        print('Located',f[0],'\n')
        X = pd.read_parquet(os.path.abspath(f[0]))
        return X
    else:
        return

# Find feature matrices with specific name so they can be loaded in
def find_files(label = 'MINIMAL',extension ='*_subfeatures.gzip',display = False):
    files = glob('**/*'+label+extension,recursive=True)
    f_list = [s for s in files if label in s] # case sensitve matching e.g. Minimal vs MINIMAL
    
    if display:
        print('\nLocated files:\n')
        [print(f,'\n') for f in f_list]
    return f_list

## Modelling
# Save data associated with the model as a yaml
def save_model_meta(data,fname):
    '''
    Save key information about the model in a yaml format. 
    It's intended that things like: 
    RMSE,FDR,n_samples,n_predictors,hyperparameters,date (of the data) will
    be included.

    Parameters:
    ----------
    data : dictionary
        All information we want to save e.g. {'RMSE':16,'FDR':1e-6,...}
    fname : string
        prefix of the meta_datafile e.g. '2013_model'

    Returns:
    -------
    None
    '''
    meta = path + fname + '_metadata_.yaml'
    with open(fname, 'w') as yaml_file:
        yaml.dump(data, yaml_file, default_flow_style=False)
    print('\n','Saved: ',fname,'\n')
    return

def save_list(fname,list_obj):
    with open(fname, "w") as f: # Save the list of rankings
        for s in list_obj:
            f.write(str(s) +"\n")
    return

def load_list(fname):
    rank = []
    with open(fname, "r") as f:
        for line in f:
            rank.append(line.strip())
    return rank


if __name__ == '__main__':
    save_dict(fname,fc)
    save_feat(X,dates,string)
    open_yaml(fname)
    save_pvals(p_values,names,dates,label)
    find_matrix(dates)

    save_model_meta(data,fname)
    #save_model(model,metadata,fname)
    #load_model(fname)
    save_list(fname,list_obj)
    load_list(fname,list_obj)