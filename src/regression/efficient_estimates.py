#!/usr/bin/env python3

## Import from our functions
from cProfile import label
from RSAM_extraction import*
from RSAM_get_data import create_RSAM_df, download_RSAM_from_station

## Tsfresh imports
from tsfresh.feature_selection import selection
from tsfresh.feature_extraction.settings import EfficientFCParameters,MinimalFCParameters

## general imports
import sys,yaml
from datetime import date, datetime, timedelta
import pandas as pd
from timeit import time
import warnings
warnings.filterwarnings("ignore")


def efficient_calculate_subfeatures(startdate,
                                    feature_fc = EfficientFCParameters(),
                                    subfeature_fc = None,
                                    Ndays = 1,
                                    frequency = '1s',
                                    seconds_per_window = 1,
                                    station = 'WIZ',
                                    label = 'Efficient',
                                    select = True,
                                    FDR = 1e-6,
                                    save = True):

    ''' Read in a day of data, extreact both features and subfeatures, then output the matrices and time taken to complete 
    
    Ndays is number of days incl the first full day
    '''
    if subfeature_fc == None:
        subfeature_fc == feature_fc

    ## Calculate window size and length parameters
    window_length = seconds_per_window*100 # seconds x sampling rate
    NumSubWindows = 144*Ndays

    ## Date info
    arr = startdate.split(' ')[0].split('-')
    yrNum,monthNum,dayNum = (int(arr[0]),int(arr[1]),int(arr[2]))

    download_end = '{}-{}-{} 23:50:00'.format(yrNum,monthNum,dayNum+Ndays) # might need a try/except if dayNum doesnt exist eg. Feb 30th #*********************************
    d = datetime.strptime('2013-05-15 00:00:00','%Y-%m-%d %H:%M:%S') - timedelta(seconds = seconds_per_window)
    test_end = '{}-{}-{} {}:{}:{}'.format(yrNum,monthNum,dayNum + (Ndays-1),d.hour,d.minute,d.second+d.microsecond/1e6) # automatically adjust the end date

   #_____________________________________________________________TIME DATA DOWNLOAD/LOAD________________________________________________________________
    start = time.time()
    ## Download from API and read in some data as a pandas dataframe
    data = download_RSAM_from_station(station,startdate,download_end)
    
    ## Read in many days at once
    dfs = []
    for i in range(Ndays):
        df = create_RSAM_df('{}-{}-{} 0:00:00'.format(yrNum,monthNum,dayNum+i),i, window_length)
        dfs.append(df)
    windowed_df = pd.concat(dfs,axis = 'rows').drop_duplicates()
    end = time.time()

    stats = []
    stats.append('Time taken to download/read in {:d} day(s) : {:.2f} s'.format(Ndays,(end-start)))
    print('Data aquired!')

    #_______________________________________________________________TIME FEATURE EXTRACTION_________________________________________________________________
    
    ## Features on Features
    window_dates =  list(map(str,pd.date_range(startdate,test_end, freq = frequency)))
    X0,X1,testing = custom_extract_subfeatures(windowed_df,window_dates,NumSubWindows,feature_fc,subfeature_fc,selection=select,q=FDR,label = label)

    f,f_on_f = custom_make_fc(X1.columns)
    
    ## Save extraction results
    s = '_selected_fdr_level_'+ '{:.1e}'.format(FDR) if select == True else ''

    if save:
        save_dict(label + s +'_subfeatures.yaml',f_on_f)
        save_dict(label + s + '_features.yaml',f)
        save_pvals(testing[1],testing[0],window_dates,label) # table of p_values and feature names
    end = time.time()

    ## Summary Stats
    print('\nX0:\n',X0.head(),'\nThe shape of our first feature matrix is:\n', X0.shape,'\nX1:\n',X1.head(),'\nThe shape of our second feature matrix is:\n',X1.shape,'\n')
    #print('p_values:',testing[1][:5],'...','\n')
    stats.append('Time taken to extract and select {:d} features on {:d} windows with subwindow length {:d} datapoints: {:.2f} s'.format(X1.shape[1],int(8.64e6),NumSubWindows,(end-start)))
    [print (stats[i]) for i in range(0,len(stats))]

   #_______________________________________________________________ WRITE OUTPUT TO A FILE  ________________________________________________________
    # Write timing stats to a file
    with open('Whakaari_timing_log.txt', 'a+') as f: #try to append if file already exists
        f.write("\n")
        for item in stats:
            f.write("%s\n" % item)

if __name__ == '__main__':
    try:
        fc = open_yaml(str(sys.argv[2]))
        subfc = open_yaml(str(sys.argv[3]))
        x = pd.read_csv('dates.csv', dtype = 'str', header = None).iloc[int(sys.argv[1]),0].replace("'","") #need to do this in order to remove ' characters
        print(sys.argv[2],'\n',sys.argv[3],'\n',fc,'\n',subfc,'\n',x,'\n')

        efficient_calculate_subfeatures(startdate = x,feature_fc = fc,subfeature_fc = subfc ,label = 'CUSTOM_Test')
    except: 
        #if no command line arguments
        calc = MinimalFCParameters() #EfficientFCParameters()
        efficient_calculate_subfeatures(startdate = '2013-08-19 00:00:00',Ndays = 1,feature_fc = calc,label = '_fdr_level_1e-8_',save = True,select = False)

    
