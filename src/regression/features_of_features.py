#from pandas._libs.tslibs import Hour, Second
from os import stat

from tsfresh import feature_selection
from RSAM_extraction import*
from RSAM_get_data import*

## 
from RSAM_extraction import*
from RSAM_get_data import create_RSAM_df, download_RSAM_from_station
from RSAM_utils import makedir

## Tsfresh imports
from tsfresh.feature_selection import select_features
from tsfresh.feature_extraction.settings import EfficientFCParameters,MinimalFCParameters

## General imports
import sys,warnings
from functools import reduce
from datetime import date, datetime, timedelta
import pandas as pd
warnings.filterwarnings("ignore")


from random import randrange,seed
from datetime import timedelta,datetime

#ind_dates = lambda x: x.replace('_','-') #+ ' 00:00:00' produce date from filename

def make_target(datelist):
    ys = []
    for f in datelist: # this approach is needed to handle missing days
        #date = find_dates(f)
        #print(date)
        yi = load_target_RSAM(f)
        ys.append(yi)
    y = pd.concat(ys,axis = 0).sort_index()
    return y

def make_X(datelist,window_length):
    dfs = []
    for i in range(len(datelist)):
        df = create_RSAM_df(datelist[i],i, window_length)
        dfs.append(df)
    X = pd.concat(dfs,axis = 'rows').drop_duplicates().sort_index()
    return X

def random_date(start, end,set_seed = 2021):
    """
    Taken from:
    https://stackoverflow.com/questions/553303/generate-a-random-date-between-two-other-dates

    This function will return a random datetime between two datetime 
    objects.
    """
    seed(set_seed)
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) #+ delta.seconds
    random_second = randrange(int_delta)

    ti = start + timedelta(seconds=random_second)

    ti = ti - timedelta(hours = ti.hour,seconds = ti.second, minutes = ti.minute)
    tf = ti - timedelta(hours = ti.hour,seconds = ti.second, minutes = ti.minute) + timedelta(hours = 23,seconds = 0, minutes = 50)

    return str(ti),str(tf)

def re_window(X,window_length = 600):
    Npoints = int(X.shape[0])
    i = int(X.shape[0]/8.64e4)

    ## Create two new columns
    X['id'] = np.repeat( (np.linspace(0, 144*i-1,144*i, dtype=int) ),window_length)
    X['seconds'] = np.linspace(0,int(i*Npoints)-1,Npoints)*1/100 # each row corresponds to 1/100 s
    return X

def simple_feature_extraction(feature_fc = EfficientFCParameters(),
                            Ndays = 2,
                            station = 'WIZ',
                            select = False,
                            label = 'FEAT_ON_FEAT',
                            FDR = 1e-6):

    ''' Read in a day of data, extreact both features and subfeatures, then output the matrices and time taken to complete 
    
    Ndays is number of days incl the first full day
    '''
    ## Calculate window size and length parameters
    window_length = int(100) # seconds x sampling rate
    sub_length = 600

    ## Choose 3 random dates + the 2019 eruption
    d1,d2 = datetime.strptime('2013-05-01 00:00:00', '%Y-%m-%d 00:00:00'),datetime.strptime('2019-12-09 00:00:00', '%Y-%m-%d 00:00:00')

    dates = []
    for i in range(2020,2020+Ndays): ## get 3 dates with different seeds
        dx,dx_end = random_date(d1, d2,set_seed = i)
        dates.append(dx)
        download_RSAM_from_station(station,dx,dx_end)
    
    dates.append('2019-12-09 00:00:00') ## 2019-12-09 00:00:00 is the 2019 eruption date
    download_RSAM_from_station(station,'2019-12-09 00:00:00','2019-12-09 23:50:00')

    y = make_target(dates)
    windowed_df = make_X(dates,window_length)
    print('Data aquired!')
    f = label + '.gzip'

    ## Features on Features
    exists = f in os.listdir()
    if exists:
        X = pd.read_parquet(f)
    else:
        X = calc_features(windowed_df,feature_fc,default= True)
        X.columns = [str(col).replace('__', '|') for col in X.columns] # Rename columns
        X.dropna(axis=1,inplace = True)
        X.to_parquet(label+'.gzip')

    df2 = re_window(X)
    X1 = calc_features(df2,feature_fc,default= True)
    X1.dropna(axis=1,inplace = True)

    if select == True:
        X.set_index(y.index,inplace = True)
        X = select_features(X,y,ml_task='regression')
    
    ## Save extraction results
    s = '_selected_fdr_level'+ '{:.1e}'.format(FDR) if select == True else ''
    X1.to_parquet(label + s + '_subfeatures.gzip')

    return

if __name__ == '__main__':
    calc = MinimalFCParameters() #EfficientFCParameters()
    simple_feature_extraction(Ndays = 2,feature_fc = calc)