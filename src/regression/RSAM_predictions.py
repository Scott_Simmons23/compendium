
#Sklearn and lightgbm
#from _typeshed import NoneType#
from operator import xor
from lightgbm.sklearn import LGBMRegressor
from sklearn.model_selection import KFold,train_test_split
from sklearn.feature_selection import RFE
from sklearn.metrics import mean_squared_error #,#make_scorer

import lightgbm as lgb
from sklearn.utils import shuffle

from RSAM_utils import*
from RSAM_get_data import load_target_RSAM
from RSAM_extraction import custom_make_fc

import os,glob,sys,time,string,yaml
import matplotlib.pyplot as plt
import numpy as np
from random import choices

## Globals
rng = 2021

##Parameter set
# 'lambda_l1' : 200,
m_params = {'max_depth': 5, 'random_state': rng, 'objective': "regression_l2" ,'learning_rate': 0.1, 'tree_learner': "data",'n_estimators': 5, 'min_child_samples': 1,'n_jobs' : -1}

metadata = {'Date': None,'RMSE': None, 'CV_mean_RMSE': None,'CV_std_RMSE': None,'CV_mean_MAPE': None,'CV_std_MAPE': None,'training_time':None,'time_to_load_data':None,
        'num_features':None,'In_sample_error': None,'Out_of_sample_error': None,'training_size': None,'test_size':None,'hyper_params': None,'best_features': None}


## I/O Functions

find_dates = lambda x: x.split(os.sep)[-1][:10].replace('_','-') #+ ' 00:00:00' produce date from filename

get_model = lambda x: lgb.Booster(model_file = m_path + x + '.joblib')

def save_feature_ranking(rank):
        with open("feature_rankings.txt", "w") as f: # Save the list of rankings
                for s in rank:
                        f.write(str(s) +"\n")
        return

def load_feature_ranking():
        rank = []
        with open("file.txt", "r") as f:
                for line in f:
                        rank.append(int(line.strip()))
        return rank

## Custom error functions

def percentage_error(y_true,y_pred):
        epsilon = np.finfo(np.float64).eps
        return 100 * (np.average(np.abs(y_pred - y_true) / np.maximum(np.abs(y_true),epsilon)))

def rms(y_pred,y_true):
        return np.sqrt(mean_squared_error(y_true,y_pred))

def load_training_data(f_list,min_files = 7,load_minimum = False,feature_names = None):
        if load_minimum: 
                try:
                        X = pd.read_parquet(f_list) if len(f_list) != 1 else pd.read_parquet(f_list[0]) #if just one file in directory
                except Exception:
                        f_list = f_list[:min_files] #if there are problems reading in all data, then only read in min_files number of files
                        X = pd.read_parquet(f_list)
        else:
                try:
                        X = pd.read_parquet(f_list) if len(f_list) != 1 else pd.read_parquet(f_list[0]) #if just one file in directory
                except Exception:
                        f_list = f_list[:min_files] #if there are problems reading in all data, then only read in min_files number of files
                        X = pd.read_parquet(f_list)

        if feature_names is not None: # select a subset of the columns (e.g. after RFE)
                X = X[feature_names]
        X.sort_index(inplace=True)
        
        X.dropna(axis = 'columns',inplace=True) ## deal with NaN features
        nancols = X.columns[X.isnull().any()].tolist()
        print('\nNaNcols:',nancols,'\n')

        ys = []
        for f in f_list: # this approach is needed to handle missing days
                date = find_dates(f)
                print(date)
                yi = load_target_RSAM(date)
                ys.append(yi)
        y = pd.concat(ys,axis = 0).sort_index()
        assert X.shape[0] == y.shape[0],'Length Mismatch: X has {} rows, y has {}'.format(X.shape[0],y.shape[0])
        
        return X,y

def train_model(tree):
        #fitted_tree = tree.fit(X,y)
        #return fitted_tree
        pass

def transform_column_names(X):
        original_names = list(X.columns)
        mapnames = [name.replace(':','').replace('"','').replace('(','').replace(')','').replace(',','') for name in original_names]
        X.columns = mapnames

        ## Create a dictionary to deal with this
        namedict = dict(zip(mapnames, original_names))
        return X,namedict

def _calculate_p_values():
        pass

def CV_model(model,X,y,N=10,shuffle = True, njobs = -1,error_func = percentage_error,rng = 2021,display = True):

        cv_est = {'train_score':[],'test_score':[]}
        CV = KFold(n_splits=N,shuffle=shuffle,random_state = rng).split(X)       
        #cv_est = cross_validate(model,X,y,cv = CV,n_jobs=njobs,return_train_score=True,scoring=error_func)
        for train,test in CV:
                X_train,X_test,y_train,y_test = X.iloc[train,:],X.iloc[test,:],y[train],y[test]
                mod = model.fit(X_train,y_train)
                ins = mod.predict(X_train) ## train/in sample
                out = mod.predict(X_test)  ## test/out of sample

                cv_est['train_score'].append(error_func(y_true = y_train.values,y_pred = ins ))
                cv_est['test_score'].append(error_func(y_true = y_test.values, y_pred = out ))


        err_train,err_test = cv_est['train_score'], cv_est['test_score']
        mean_test,std_test,mean_train,std_train = np.mean(err_test),np.std(err_test),np.mean(err_train),np.std(err_train)

        if display:
                print('\n{} Fold Cross-Validation \nAverage TEST error: {:.3f} \nStd: {:.3f}'.format(N,mean_test,std_test) )
                print('\nAverage TRAIN error: {:.3f} \nStd: {:.3f}'.format(mean_train,std_train) )
        return cv_est,mean_test,mean_train

def train_model(label,extension,test_size = 0.2,shuffle = True,modelname = 'model',save = True,**kwargs):
        
        ## Load in subfeature matrices
        begin = time.time()
        f_list  = find_files(label,extension,display=True)
        X,y = load_training_data(f_list,min_files=14)
        end = time.time()

        print('Loading time:',end-begin,'\n')
        print(X.head(),'\n',y.head())

        X1,_ = transform_column_names(X)

        ## Out of sample prediction on  33% of the data
        X_train,X_test,y_train,y_test = train_test_split(X1,y,test_size=test_size,shuffle=shuffle,random_state=rng) # random split
        print('Data split!\n')
        
        ## Train model
        tree = lgb.LGBMRegressor(**kwargs) 
        begin = time.time()
        fitted_tree = tree.fit(X_train,y_train)
        end = time.time()
        print('Training time:',end-begin,'\n')

        ## Out of Sample prediction
        print('Out of sample Prediction\n')
        Out_result,err_out = make_prediction(fitted_tree,X_test,y_test,display=True)
        print(Out_result.head(),'\n',err_out)

        ## Test that saving and loading with joblib works
        modname = m_path + modelname + '.joblib'
        fitted_tree.booster_.save_model(modname)
        print('Model saved as ',modname,'\n')

        return

def make_prediction(fitted_tree,X,y,score_func = percentage_error,display = False): #,choose_random_split = False):

        yhat = fitted_tree.predict(X)
        error_score = score_func(y_true = y , y_pred = yhat)
        result = pd.DataFrame(data = {'y': y , 'y_pred' : yhat, 'residuals' : yhat-y }, index = y.index)

        if display:
                print('Prediction Error: {:.3f}'.format(error_score) )

        return result,error_score


def lgbm_rfe(X,y,feature_target = 1000,step = 50,transform = True,verbose = 1,**kwargs):

        names = X.columns
        model = LGBMRegressor(**kwargs)
        X,_ = transform_column_names(X)

        selector = RFE(model, n_features_to_select=feature_target, step=step,verbose=verbose)
        selector = selector.fit(X, y)

        rank_idx = np.argsort(selector.ranking_) #index that sorts the ranking array
        rank = selector.ranking_[rank_idx]
        sorted_names = names[rank_idx]

        if transform:
                print('{:d} Features Eliminated!'.format(X.shape[1]-feature_target))
                out = selector.transform(X).reshape(X.shape[0],feature_target)
                X1  = pd.DataFrame.from_records(out)
                X1.columns = names[selector.support_]
                return X1,rank,sorted_names             ## return transformed feature matrix
        else:
                return X,rank,sorted_names

def remove_features_with_RFE(label,
                        extension = '*subfeatures.gzip',
                        target = 1000,
                        stepsize = 1000,
                        min_files = 15,
                        test_size = 0.33,
                        verbose = 1,
                        fname = '1000_features',
                        display = True,
                        save_features = True):
        
        '''
        Returns: None

        saves a list (as a textfile)
        '''

        f_list  = find_files(label,extension,display=display)
        X,y = load_training_data(f_list,min_files=min_files,load_minimum = True)
        X_train,X_test,y_train,y_test = train_test_split(X,y,test_size=test_size,shuffle=shuffle,random_state=rng)
        
        ## USE LGBM with default params to eliminate features
        X1,rank,sorted_names = lgbm_rfe(X_train,y_train,target,step = stepsize,verbose = verbose)
        #f,ff = custom_make_fc(X.columns)
        
        if save_features:
                X1.to_parquet(fname + '.gzip')
                #save_dict('RFE_selected_features.yaml',ff)
                #save_dict('RFE_selected_subfeatures.yaml',f)
                save_list(fname + '.txt',sorted_names) # save a text file of the column names
                save_feature_ranking(rank)
        
        if display:
                print(X1.head(),'\n',rank,'\n',sorted_names,'\n',f,'\n',ff,'\n')
        return

def evaluate_LGBM_model(X,y,tree = None,resultsname = 'output',test_size = 0.2,shuffle = True,**model_params):
        X1,_ = transform_column_names(X)

        ## Out of sample prediction on  20% of the data
        X_train,X_test,y_train,y_test = train_test_split(X1,y,test_size=test_size,shuffle=shuffle,random_state=rng) # random split
        #########################_,randX_test,_,randy_test = train_test_split(X1,y,test_size=0.20,shuffle=False) # train on last 20% of the data
        print('Data split!\n')
        
        ## Train model
        mod = lgb.LGBMRegressor(**model_params) if isinstance(tree,NoneType) else tree
        begin = time.time()
        fitted_tree = mod.fit(X_train,y_train)
        
        end = time.time()
        metadata['training_time'] = end-begin
        print('Training time:',end-begin,'\n')

        
        ## Kfold cross validation
        Nfold = 10
        MAPE,mu,sigma = CV_model(tree,X_train,y_train,Nfold)
        RMSE,mu1,sigma1 = CV_model(tree,X_train,y_train,Nfold,error_func=rms)
        print('Cross_validated!\n')

        ## In Sample prediction
        print('In sample Prediction\n')
        In_result,err_in = make_prediction(fitted_tree,X_train,y_train,display=True)
        print(In_result.head())

        ## Out of Sample prediction
        print('Out of sample Prediction\n')
        Out_result,err_out = make_prediction(fitted_tree,X_test,y_test,display=True)
        print(Out_result.head())

        metadata['CV_mean_MAPE'] = mu
        metadata['CV_std_MAPE'] = sigma
        metadata['CV_mean_RMSE'] = mu1
        metadata['CV_std_RMSE'] = sigma1
        metadata['In_sample_error'] = err_in
        metadata['Out_of_sample_error'] = err_out
        metadata['training_size'] = X.shape[0]
        metadata['num_features'] = X.shape[1]


        ## Test that saving and loading with joblib works
        #fname = m_path + 'test'+'.joblib'
        #fitted_tree.booster_.save_model(fname)

        #mod = lgb.Booster(model_file= fname)
        return In_result,Out_result,metadata

def get_lgbm_feature_importance():
        pass

def train_lgbm(model,**kwargs):
        pass

if __name__ == '__main__':
        
        resultsname = sys.argv[1]
        label = '_fdr_1e-8_' #sys.argv[2]
        extension ='*_subfeatures.gzip' #sys.argv[3]

        ## Load in subfeature matrices
        begin = time.time()
        f_list  = find_files(label,extension,display=True)
        X,y = load_training_data(f_list,min_files=14)
        end = time.time()

        #metadata['time_to_load_data'] = end-begin
        print('Loading time:',end-begin,'\n')
        print(X.head(),'\n',y.head())
        assert X.shape[0] == y.shape[0]

        ##resultsname,label,extension = sys.argv[1:4]
        In_result,Out_result,_ = evaluate_LGBM_model(X,y)

        #fs = find_files()
        #X,y = load_training_data(fs)
        #X,feature_rankings = lgbm_rfe(X,y,feature_target=15)

        #remove_features_with_RFE(label,target = 15,stepsize = 10,save_df = True,display=True)
        #remove_features_with_RFE(label,target = 1000,stepsize = 1000,save_df = True,display=True)
        train_model(label,extension)

        ## Store predicted value in the results folder of tmp
        makedir(r_path)
        
        Out_result.to_csv(r_path + resultsname) #,index = False)
        metaname = r_path + resultsname + '.yaml'
        
        with open(metaname, 'w') as outfile:
                yaml.dump(data, outfile, default_flow_style=False)






        

