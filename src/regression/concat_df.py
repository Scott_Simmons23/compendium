import pandas as pd
import os,sys
from glob import glob
from sklearn.model_selection import train_test_split
from datetime import timedelta
from tsfresh.feature_selection.relevance import calculate_relevance_table
from scipy.stats import pearsonr

save = False
i = 111
tol = 2e-16
f_dir = 'tmp/feature_matrices'

## Load data
fhandle = 'WSRZ_tremor_data.csv'
WSRZ = pd.read_csv(fhandle).drop(columns = ['mf','hf','dsar'])
WSRZ.index = pd.to_datetime(WSRZ.time,format='%Y-%m-%d %H:%M:%S')
WSRZ.drop(columns = ['time'], inplace= True)

os.chdir(f_dir)
files = [ff for ff in os.listdir() if 'subfeatures' in ff]

datify = lambda x: x[:10].replace('_','-') + ' 00:00:00'

def make_target_(day):
    period = pd.to_datetime(day.split(' ')[0])
    start = period + timedelta(minutes=10)
    end = period + timedelta(days = 1)
    y = WSRZ.loc[start:end].squeeze() # convert to a pd.Series
    return y

# some code taken from stack exchange https://stackoverflow.com/questions/32893509/how-to-read-multiple-files-and-merge-them-into-a-single-pandas-data-frame
df_list,bad_files,ys = [],[],[]
for f in files:
    try:
        df_list.append(pd.read_parquet(f)) # download 
        day = datify(f)
        ys.append(make_target_(day)) ## download target
    except:
        bad_files.append(f)
dfs = pd.concat(df_list,axis = 0)
y = pd.concat(ys)

## Some preprocessing
dfs.dropna(how = 'any',inplace=True)
X = dfs[dfs.columns[dfs.max() < tol]] # remove values that are too small (close to machine 0)
X.set_index(y.index,inplace = True)

## Examine the correlation
corr = []
featname = []
for feat in X.columns:
    featname.append(feat)
    try:
        corr.append(pearsonr(X,y)[0])
    except:
        corr.append(0)

corr_tbl = pd.DataFrame(data = {'features':featname,'correlation':corr})
corr_tbl.sort_values(ascending=True,by = 'correlation',inplace=True)

## Calculate the p_values of the features
tbl = calculate_relevance_table(X,y,ml_task='regression')

## Split into train and test
X_train,X_test,y_train,y_test = train_test_split(X,y,test_size = 0.33,random_state=2021)


## Save all the files
if save:
    X_train.to_parquet('X_train_%d.gzip' % i)
    X_test.to_parquet('X_test_%d.gzip' % i)

    y_train.to_csv('y_train_%d.csv' % i)
    y_test.to_csv('y_test_%d.csv' % i)

    tbl.to_csv('p_value_table.csv')
    corr_tbl.to_csv('features')

## Inspect
print('\nLoaded files: ',df_list,'\nbad files: ',bad_files)
print(tbl.head())
print(corr_tbl.head())