#!/usr/bin/env python3

## Import from our functions

from dask.utils import funcname
from tsfresh.feature_extraction.extraction import _do_extraction
from tsfresh.feature_extraction.feature_calculators import spkt_welch_density
from RSAM_extraction import*
from RSAM_get_data import create_RSAM_df, download_RSAM_from_station
from RSAM_utils import makedir

## Tsfresh imports
from tsfresh.feature_selection import select_features
from tsfresh.feature_extraction.settings import EfficientFCParameters,MinimalFCParameters

## general imports
import sys,warnings
from functools import reduce
from datetime import date, datetime, timedelta
import pandas as pd
warnings.filterwarnings("ignore")

def save_X(X,fname):
    path = reduce(os.path.join,[os.getcwd(),'tmp','simple_features',''])
    makedir(path)
    X.to_parquet(path + fname+'.gzip')
    return

def simple_feature_extraction(startdate,
                            feature_fc = EfficientFCParameters(),
                            Ndays = 1,
                            station = 'WIZ',
                            select = True,
                            FDR = 1e-6):

    ''' Read in a day of data, extreact both features and subfeatures, then output the matrices and time taken to complete 
    
    Ndays is number of days incl the first full day
    '''

    ## Calculate window size and length parameters
    window_length = int(100*600) # seconds x sampling rate
    ##NumSubWindows = 144*Ndays

    ## Date info
    arr = startdate.split(' ')[0].split('-')
    yrNum,monthNum,dayNum = list(map(int,arr[0:3]))
    download_end = '{}-{}-{} 23:50:00'.format(yrNum,monthNum,dayNum+Ndays) # might need a try/except if dayNum doesnt exist eg. Feb 30th #*********************************
    #d = datetime.strptime('2013-05-15 00:00:00','%Y-%m-%d %H:%M:%S') - timedelta(seconds = 600)
    test_end = '{}-{}-{} 23:50:00'.format(yrNum,monthNum,dayNum)

    ## Download from API and read in some data as a pandas dataframe
    download_RSAM_from_station(station,startdate,download_end)
    dfs = []
    for i in range(Ndays):
        df = create_RSAM_df('{}-{}-{} 0:00:00'.format(yrNum,monthNum,dayNum+i),i, window_length)
        dfs.append(df)
    windowed_df = pd.concat(dfs,axis = 'rows').drop_duplicates()
    print('Data aquired!')

    ## Feature Extraction
    #--------------------
    
    ## Features on Features
    window_dates =  list(map(str,pd.date_range(startdate,test_end, freq = '10min')))
    X = calc_features(windowed_df,feature_fc,default= True)
    X.set_index(pd.to_datetime(window_dates),inplace=True)

    y = get_y(window_dates[0],Ndays)

    if select == True:
        X.set_index(y.index,inplace = True)
        X = select_features(X,y,ml_task='regression')
    
    ## Save extraction results
    s = '_selected_fdr_level_'+ '{:.1e}'.format(FDR) if select == True else ''
    fname = window_dates[0].split(' ')[0].replace('-','_') + s +'_FEATURES_ONLY'
    save_X(X,fname)

    return

if __name__ == '__main__':
    try:
        x = pd.read_csv('dates.csv', dtype = 'str', header = None).iloc[int(sys.argv[1]),0].replace("'","") #need to do this in order to remove ' characters
        
        calc = MinimalFCParameters() #EfficientFCParameters()
        simple_feature_extraction(startdate = x,Ndays = 1,feature_fc = calc,select = False)
    except: 
        #if no command line arguments
        calc = MinimalFCParameters() #EfficientFCParameters()
        simple_feature_extraction(startdate = '2013-08-19 00:00:00',Ndays = 1,feature_fc = calc,select = True)

    #x = pd.read_csv('dates.csv', dtype = 'str', header = None).iloc[int(sys.argv[1]),0].replace("'","") #need to do this in order to remove ' characters

    #calc = MinimalFCParameters() #EfficientFCParameters()
    #simple_feature_extraction(startdate = x,Ndays = 1,feature_fc = calc,select = False)
    
