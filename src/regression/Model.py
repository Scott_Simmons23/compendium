''' This file contains a model object that:
 displays information about the data/model/features/piplinee/ methods to fit/display/predict/evaluate?? etc
Author: Louis Jarvis
Date modified: 13/07/21
'''

##TODO: use for classification
## integrate with Pipeline objects?
## Unit tests
## Regular cross_validation
## appropriate default hyperparams

##from tsfresh.feature_extraction import extract_features
import lightgbm as lgb
##from numpy.core.fromnumeric import size
#from pandas.core.frame import DataFrame


from tsfresh import feature_selection
from tsfresh.feature_selection import select_features
from tsfresh.feature_selection.selection import calculate_relevance_table
from tsfresh.transformers import FeatureSelector

import lightgbm as lgb
from lightgbm import LGBMRegressor,LGBMClassifier
from lightgbm import plot_importance

#Scikitlearn
from sklearn.model_selection import GridSearchCV,cross_validate,RepeatedKFold,cross_val_score ##,train_test_split
from sklearn.model_selection import TimeSeriesSplit
#from sklearn.model_selection import cross_validate
#from sklearn.model_selection import train_test_split
from sklearn.metrics import*
from sklearn.preprocessing import StandardScaler
##from sklearn.utils import all_estimators # Give the user the option to use any scikitlearn estimator

# General imports
import numpy as np
import pandas as pd
from pandas.core.frame import DataFrame
import matplotlib.pyplot as plt
from datetime import datetime
import matplotlib.dates as dates 
import seaborn as sns
from math import isnan

#regression_list = list( zip(*all_estimators(type_filter='regressor')) )
#clasifier_list = list(zip(*all_estimators(type_filter='classifier')))
score_names = list(SCORERS.keys())

RMSE = lambda y_true,y_pred : np.sqrt(mean_squared_error(y_true,y_pred))

class tsModel:
    def __init__(self,X,y,fdr = 0.05,estimator = LGBMRegressor,params = None,task = 'regression',score_func = RMSE,error_measure = 'neg_root_mean_squared_error'):
        """
        
        Parameters:
        ---------- 
        X : pandas DataFrame
            the feature matrix with index that follows the format '%Y-%m-%d %H:%M:%S'
        
        y : pandas Series or 1D np.ndarray representing the target vector as a continuous variable or a vector of labels

        FDR : float
            False Discovery Rate, the expected proportion of irrelevant featutes, set to 10% by default 
        
        estimator : scikitlearn or lightgbm model object
            Model chosen to estimate the target, LGBMRegressor by default

        params  : dictionary or None (default)
            Dictionary of hyperparameters

        task : string
            set to 'regression' or 'classification' depending on the task. Set to 'regression' by default
        
        score_func : callable
            the fucntion that computes the model score

        error_measure : string
                The error measure for the classifier/regression model. set to ‘neg_root_mean_squared_error’
                ####options can be found at https://scikit-learn.org/stable/modules/model_evaluation.html under scoring parameter


        Attributes:
        ----------


        Methods:
        --------
        """
        
        ## Initial attributes

        # Check X and y compatible
        if (y.shape[0] != X.shape[0]):
            raise ValueError('X,y not the same length: X has {} rows, y has {}'.format(X.shape[0],y.shape[0] ))
        
        if isinstance(X.index,datetime):  # Check if index is instance datetime
            self.df = X.dropna(axis = 1,how = 'any',inplace = True) #Ensure no NaN cols
        else:
            try:
                X.index = pd.to_datetime(X.index, format='%Y-%m-%d %H:%M:%S')
                self.df = X.dropna(axis = 1,how = 'any',inplace = False) #Ensure no NaN cols
            except Exception:
                raise TypeError('dataframe index is not a suitable datetime format')

        if isinstance(y,pd.Series): # Check that y is the right datatype   
            self.y = y                             
        else:         
            raise TypeError('y must be a 1d pd.Series') 

        if (error_measure in score_names):## Error measure
            self.error_measure = error_measure  
        else:
            print('Invalid Scorer, look at sklearn.metrics.SCORERS.keys()\nerror measure will be set to default: RMSE')
            self.error_measure = 'neg_root_mean_squared_error'# RMSE by default

        self.model = estimator                              # ML model used
        self.score_func = score_func
        self.fdr =  fdr                                     # FDR
        self.params = None                                  # Hyperparameters
        self.task = task                                    # ML task
        
        ## After running model these are assigned
        self.accuracy = None       # Model Accuracy
        self.best_params  = None   # Dict of best model parameters after cross validating hyperparameters
        self.best_features = None  # ... of features selected by tsfresh


    def plot_time_series(self,feature_names = None,nrows = 1, ncols = 1, **kwargs):
        """ 
        Create a plot a time series, either feature(s) themselves on the same plot or the target vector y

        Parameters:
        -----------
        column_name : str or default = None
            the name of the column to be plotted
        
        feature names : list of strings
            the features to be plotted on the same axis

        Returns:
        --------
        
        """
        fig,axes = plt.subplots(nrows,ncols) # create fig,axes object

        if feature_names is None:                                       # Plot target vector by default 
            self.y.plot(ax = axes,**kwargs)
        elif (nrows*ncols > 1):                                         # plots on multiple axes
            for i,axis in enumerate(axes):
                self.df.plot(y = feature_names[i],ax = axes, **kwargs) 
        else:
            for ft in feature_names:                                    # loop through list of features to plot
                self.df.plot(y = ft,ax = axes, **kwargs)                
        
        axes.set_xlabel('Time')
        formatter = dates.DateFormatter('%Y-%m-%d %H:%M:%S')            # must do this to correctly format dates 
        plt.gcf().axes[0].xaxis.set_major_formatter(formatter) 
            
    def plot_residuals(self):
        sns.set_style('ticks')
        f,axis = plt.subplots(figsize = (20,5))
        x = np.linspace(0,len(r),len(r))
        #y = np.repeat(0,len(tree_2_pred.residuals))

        #ax.plot(x,y,linestyle = '--')
        #tree_2_pred.residuals.plot(linestyle = 'None', marker = 'o',ax = axis)
        axis.plot(x,r,linestyle = 'None', marker = 'o', color = 'tab:blue')
        axis.hlines(y=0, xmin=0, xmax=len(r), linewidth=1, color='black',linestyle = '--')
        axis.set_xlim([0,len(tree_2_pred.residuals)])
        axis.set_ylim([round(min(r),-2),round(max(r),-2)])
        axes = plt.gca()
        axes.yaxis.grid(linestyle = '--')
        #plt.show()
        return

    def feature_target_corr(self,fmat = None,ncols = 4):
        """
        Scatter plot that also displays pearson's correlation coefficent (up to 50 features)
        """
    
        if (fmat.shape[1] > 51): # more than 50 features (51 columns including RSAM)
            print('Too many features to plot (max: 50)')
            return
        
        if fmat is None:
            fmat = self.df
        
        # Default sizing params
        titlesize = 40
        labelsize = 15
        
        numftr = fmat.shape[1]
        nrows = int(numftr/ncols)
        
        #sns.set_style('darkgrid')
        f,axes = plt.subplots(nrows,ncols, figsize = (50,50), sharey = True)
        ##f.suptitle(fname,size = titlesize, y = 0.92)
        
        # Loop through Features and plot each feature against RSAM
        axes = axes.flatten()
        for ftr in range(numftr):
            if (fmat.columns[ftr] != 'rsam'):
                
                #Scatterplot of feature vs RSAM values
                fmat.plot(x = fmat.columns[ftr],
                        y = 'rsam',
                        kind = 'scatter',
                        ax = axes[ftr])
                
                # Line of best fit
                sns.regplot(x = fmat[fmat.columns[ftr]],
                            y = fmat['rsam'],
                            ax = axes[ftr],
                            ci = None,
                            scatter_kws={"color": "blue"}, # different colours for ponts and line
                            line_kws={"color": "red", 'linestyle' : 'dashed'})
                
                # Print the correlation score
                
                r = np.corrcoef(fmat[fmat.columns[ftr]], fmat['rsam'])
                
                if isnan(r[0][1]) is False:
                    axes[ftr].text(0.05,0.95,
                                "Pearson's \n correlation: {:.2f}".format(r[0][1]),
                                size = labelsize,
                                transform=axes[ftr].transAxes,
                                color = 'g',
                                ha="left", 
                                va="top",
                                bbox=dict(facecolor='none', edgecolor='green', pad=5.0))
                else:
                    axes[ftr].text(0.05,0.95,
                                "Pearson's \n correlation: NA",
                    size = labelsize,transform=axes[ftr].transAxes,color = 'g',
                    ha="left",
                    va="top",
                    bbox=dict(facecolor='none', edgecolor='green', pad=5.0) )

                
                #Labels
                ##axes[ftr].set_xlabel(axes[ftr].get_xlabel())
                ##axes[ftr].set_ylabel('RSAM [$\mathregular{ms^{-1}}$]')     ##keep this??
                #axes[ftr].xaxis.label.set_size(9)
        
        ##axes.flat[-1].set_visible(False) # suppress last (empty) plot
        #plt.xticks(fontsize = textsize)
        #plt.yticks(fontsize = textsize)
        #plt.show()
        ##plt.tight_layout()
        #plt.subplots_adjust(hspace = 5)
        return
    
    def choose_features(self,q):
        """ 

        ## take into consideration the p-values...

        Use tsfresh to select columns relevant for prediction, then assign result to object
        More info at: https://tsfresh.readthedocs.io/en/latest/api/tsfresh.feature_selection.html

        """
        #ncol = self.df.shape[1]
        #try:
        #    self.df = select_features(self.df,self.y,fdr_level= self.fdr)
        #except ValueError: # indices differ
        #    self.y.index = self.df.index # set indices to be equal to one another
        #    self.df = select_features(self.df,self.y,fdr_level= self.fdr)

        #self.best_features = self.df.columns # the best features according to hypothesis tests
        #print("Number of features reduced from {} to {} ".format(ncol,self.df.shape[1]))
        #return

    def cross_val_model(self,cval_func, Nsplit = 5,gap = 1,n_jobs = None,rnd = 2021):
        
        """
        Cross Validate the time series model by

        Parameters:
        -----------
        Nsplit : int
            the number of folds to cross validate over

        gap : int 1
            the number of datapoints between the training and test set, 1 by default so that the train and test set are unrelated
            see https://stats.stackexchange.com/questions/14099/using-k-fold-cross-validation-for-time-series-model-selection.
        
        """
        max_train_size = self.shape[0] - int( self.shape[0]*test_size ) - gap

        ##sc =StandardScaler() # parameterise this??
        mdl = self.estimator() if (self.params is None) else self.estimator(**self.params) #use default params if no hyperparams specified

        ## Fit model with repeated K fold cross-validating
        #tscv = TimeSeriesSplit(n_splits= Nsplit, gap = gap) # split the time series so that train comes before test
        tscv = cval_func

        if self.params is None:
            scores = cross_validate(estimator = self.model,X = self.df, 
                                    y = self.y, cv = tscv, scoring = self.error_measure, 
                                    return_train_score = True, n_jobs=n_jobs)
        else:
            scores = cross_validate(estimator = self.model,X = self.df,
                                    y = self.y, cv = tscv, scoring = self.error_measure,
                                    return_train_score = False, n_jobs = n_jobs, 
                                    fit_params=self.params)

        self.accuracy, std = np.mean(scores['test_results']), np.std(scores['test_results']) # get the mean accuracy and standard deviation of the error estimate
        print('Cross Validated {} : {} , standard deviation : {}'.format(self.error_measure,self.accuracy,std))
        
        return

    def calc_p_values():
        pass

    def predict_y(self,test_size):
        """


        """

        ## Assign a score
        mdl = self.model() if (self.params is None) else self.model(**self.params) #use default params if no hyperparams specified

        #self.model = mdl.fit(X_train,y_train)
        ypred = self.model.predict(X_test)
        self.accuracy = self.score_func(y_test,ypred)
        
        result_tbl = pd.DataFrame(data = {'y': y_test.values ,'ypred' : yhat,'residuals' : y2.values - yhat},index = y2.index)

        ## Output series and dataframe objects
        #ypred = pd.Series(data = ypred, index = self.y.iloc[test_idx].index ).sort_index(ascending = True)
        #y = self.y.iloc[test_idx].sort_index(ascending = True)
        #testX = self.df.iloc[test_idx].sort_index(ascending = True)

        #return ypred,y,testX

    def optimise_model(self):
        """Find the optimal set of model parameters using a grid search
        
        """
        pass  

    def feature_importance(self,fsize = (10,10),txtsize = 11): ### KEEEP?
        ''' Only applies if tree based model is used'''
        
        try:
            f,ax = plt.subplots(figsize = fsize)
            lgb.plot_importance(self.model, ax = ax)
            ax.tick_params(size = txtsize)
        except Exception:
            'LGMBRegressor or LGBMClassifier required as tsModel.model'
        return


#class RSAM_model(tsModel):
#    '''
#    '''
#    predict_RSAM()

if __name__ == '__main__':  
    tsModel(X,y)