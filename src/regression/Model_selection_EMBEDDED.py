import pandas as pd
import lightgbm as lgb
import numpy as np
import os,sys
import matplotlib as plt
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score
from scipy.stats import pearsonr
from tsfresh.feature_selection.relevance import calculate_relevance_table
from RSAM_predictions import transform_column_names

X = pd.read_parquet('X_aaa.gzip')
y = pd.read_csv('y_aa.csv')

## Drop all the columns smaller than machine 0 (~2e-16)
df = X[X.columns[X.max() > 2e-16]]
df = df.dropna(axis = 1,how = 'any') # drop columns with any NaNs
print(df.shape)

## Train test split
X_train,X_test,y_train,y_test = train_test_split(df,y,test_size = 0.2,random_state = 2021)
y_train.set_index(X_train.index,inplace = True) # need same index (integer)

## Table of Ffeature p_values
tbl  = calculate_relevance_table(X_train,y.rsam,ml_task = 'regression')
#tbl = tbl[abs(tbl.p_value != 0)] ## There are lots of 0.0000000 p_values (does not make sense - keep for simplicity?)

tb2 = tbl[['feature','p_value']]
tb2.reset_index(drop = True,inplace = True)

corr = []
featname = []
for feat in df.columns:
    featname.append(feat)
    try:
        corr.append(pearsonr(df[feat],y)[0])
    except:
        corr.append(0)

## Examine the feature correlations
corr_tbl = pd.DataFrame(data = {'feature':featname,'correlation':corr})
corr_tbl.sort_values(ascending=True,by = 'correlation',inplace=True)

## Feature Table
FEAT_tbl = pd.merge(tb2,corr_tbl,how = 'inner',on = 'feature')
FEAT_tbl = FEAT_tbl.drop_duplicates(['feature'])
#top_corr = corr_tbl[abs(corr_tbl['correlation']) > 0.5 ]

X1,namedict = transform_column_names(X_train)
X2,_ = transform_column_names(X_test)

model = lgb.LGBMRegressor()
fit = model.fit(X1,y_train.rsam) 
mod = fit.booster_

imp = pd.DataFrame(data = {'Importance' : mod.feature_importance() ,  'Name' : mod.feature_name() } )  
IMP_tbl = [imp.Importance > 0].sort_values('Importance',ascending = False) # Features with importance greater than 0
IMP_tbl.columns = ['Importance','feature']

# Table with information on p_value, correlation, feature importance (note these are the transformed feature names)
OUT = pd.merge(FEAT_tbl,IMP_tbl,on = 'feature')
OUT.to_csv('FEATURES_info.csv')

print('# of Features in final model is %d' % OUT.shape[0])

#Save new model after saving features
chosen = list(OUT.feature)
fit2 = lgb.LGBMRegressor.fit(X_train[chosen],y_train.rsam)
fit2.booster_.save.save_model('MODEL.mod')


