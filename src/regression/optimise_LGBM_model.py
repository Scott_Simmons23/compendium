'https://github.com/nesi/hpc_for_datascience_demos/blob/master/notebooks/hyperparameters_search_basic.ipynb'

## For gridsearch
from os import mkdir
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import make_scorer

from RSAM_predictions import train_test_split,load_training_data
from RSAM_utils import find_files

import time,sys
import numpy as np
from RSAM_predictions import percentage_error,transform_column_names
from RSAM_utils import*

from lightgbm import LGBMRegressor

## Best so far
gbest = {'learning_rate' : [0.0100,0.0050],
        'num_iterations':  [500,550,600], 
        'max_depth' :      [20,30,40,50,60,70],
        'num_leaves' :     [80,90,100,110,120],
        'max_bin' :        [255,265,275]    }        

## Grow deeper trees with more leaves
## increase accuracy
grid_0 = {  'learning_rate' : [0.005,0.010,0.050,0.100],      ## should increase accuracy
            'num_iterations': [500,750,1000],
            'num_leaves' :  [i for i in range(30,70,10)],     ## number of leaves each learner has
            'max_depth'  : [i for i in range(30,100,10)],      ## Deeper trees overfit more
            'feature_fraction': [0.4,0.6,0.8,1.0] }           ## high number of bins leads to overfitting

## ## Comprehensive
## reduce overfitting
grid_1 = {  'learning_rate' : [0.0001,0.001,0.01,0.1],        ## should increase accuracy
            'num_iterations': [100,250,500,750,1000],
            'num_leaves' :  [i for i in range(5,65,10)],      ## number of leaves each learner has, smaller amounts controls overfitting
            'lambda_l2'  : [i for i in range(0,125,25)],      ## Control overfitting
            'max_depth'  : [i for i in range(5,105,10)],      ## Deeper trees overfit more
            'feature_fraction': [0.4,0.6,0.8,1.0] }           ## high number of bins leads to overfitting

## Fewer params - more manageable
grid_2 = {  'learning_rate' : [0.001,0.01,0.1],             ## should increase accuracy
            'num_iterations': [400,500,600],                ## Small learning rate and high number of trees
            'max_depth' : [40,50,60,70,80,90],              ## Deeper trees overfit more (should be tuned with num leaves)
            'num_leaves' :  [40,50,60,70,80,90]}            ## number of leaves each learner has, smaller amounts controls overfitting,}       

## Increase accuracy,
## dont worry about overfitting as much
grid_3 = {  'learning_rate' : [0.001,0.01,0.1],      ## should increase accuracy
            'num_iterations': [100,250,500],         ## Small learning rate and high number of trees best for accuracy
            'max_depth' : [75,150,300],              ## Deeper trees overfit more (should be tuned with num leaves)
            'num_leaves' :  [10,20,40],              ## number of leaves each learner has, smaller amounts controls overfitting,
            'max_bins' : [255,300],
            'feature_fraction': [0.8,1.0] }

grid_4 = {  'learning_rate' : [0.001,0.01,0.1],      ## should increase accuracy
            'num_iterations': [100,250,500],         ## Small learning rate and high number of trees best for accuracy
            'num_leaves' :    [20,30,50],            ## Default 31.
            'max_bin' :       [300],
            'feature_fraction': [0.8,1.0] }          ## sample fraction of features for each tree

## To test on laptop
test_params = {'lambda_l1' : [30,50,70,90,200], 'lambda_l2': [20,30,50,200], 'learning_rate':[0.01,0.05,0.10,0.25]}
test_feature_names = ['rsam|sum_values__sum_values', 'rsam|sum_values__median','rsam|sum_values__mean', 'rsam|sum_values__length']


def optimise_lgbm_model(X_train,y_train,X_test,y_test,N = 5,params = grid_2,feature_names = None,test_size = 0.33, verbose = 1):

    X1,_ = transform_column_names(X_train)
    X2,_ = transform_column_names(X_test)

    ## Initialise GSCV object
    GSCV = GridSearchCV(LGBMRegressor(), 
                        param_grid = params,
                        scoring = make_scorer(percentage_error,greater_is_better=False),
                        cv = N,
                        verbose = verbose)
    
    ## Train
    start = time.perf_counter()
    GSCV.fit(X1, y_train)
    print('training time:',time.perf_counter() - start,'\n')

    ## Output
    mod = GSCV.best_estimator_
    print('Best parameters:',GSCV.best_params_,'\nBest {:d} fold cross-validated'.format(N),-1*GSCV.best_score_)

    ## Accuracy when predicting unseen test data
    y_pred = mod.predict(X_test)
    mape = percentage_error(y_pred,y_test)
    print('\nMean Average Percentage Error evaluated on hold out set:',mape,'\n')

    return GSCV.best_params_,mod

def optimiser(label,extension = '*_subfeatures.gzip',N = 5,feature_names = None,test_size  = 0.33,params = grid_3,verbose = 1):
    '''
    '''
    fs = find_files(label=label,extension = extension,display=True)
    X,y = load_training_data(fs,feature_names = feature_names)
    X_train,X_test,y_train,y_test = train_test_split(X,y,test_size = test_size,random_state=2021)

    best_params,fitted_tree = optimise_lgbm_model(X_train,y_train,X_test,y_test,N = N,params=params,test_size = test_size,verbose = verbose)

    return best_params,fitted_tree,[X_train,X_test,y_train,y_test]

if __name__ == '__main__':
    model_name = 'TEST_200_featues_v1'      ## Prefix for the output file
    label = 'MINIMAL_TEST' #'_fdr_1e-8_'    ## Prefix for the feature matrix file names
    save_results = True                     ## Write the model and the params to the data
    save_data = True
    Folds = 3                               ## Number of rounds of cross validation
    test = 0.40                             ## Proportion of data used as test data
    v = 0                                   ## Verbosity

    ## Load in feature names (features selected from RFE)
    feats = test_feature_names #load_list('200_feat_names.txt')

    ## Optimise
    best_params,fitted_tree,df_list = optimiser(label,
                                                N = Folds, 
                                                params = test_params, 
                                                feature_names = feats, 
                                                test_size = test,
                                                verbose = v)
    
    if save_results:
        ## Save model
        makedir(m_path)
        fitted_tree.booster_.save_model(m_path + model_name + '.joblib')
        print('Saved model:',m_path + model_name + '.joblib')

        ## Save parameter dictionary as a yaml
        save_dict(m_path + model_name + '_best_params.yaml',best_params)

    if save_data:
        ## Save Train and testmatrices associated with the model
        df_list[0].to_parquet(m_path + model_name + '_X_train.gzip')
        df_list[1].to_parquet(m_path + model_name + '_X_test.gzip')

        df_list[2].to_csv(m_path + model_name + '_y_train.csv')
        df_list[3].to_csv(m_path + model_name + '_y_test.csv')