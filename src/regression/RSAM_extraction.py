#!/usr/bin/env python3
'''
This file contains code modified from ...
Author: Louis Jarvis
Date: 07/07/2021
'''
## Tsfresh imports
from logging import info
from pandas.core.indexes import period
from tsfresh.feature_extraction import extract_features
#from tsfresh.feature_selection import select_features
from tsfresh.transformers import FeatureSelector
from tsfresh.feature_extraction.settings import from_columns

from RSAM_get_data import get_y
from RSAM_utils import*

## General imports
import traceback,datetime,os, sys, shutil, warnings, gc, joblib,yaml
from glob import glob
import numpy as np
from datetime import datetime, timedelta, date
import pandas as pd
from pandas._libs.tslibs.timestamps import Timestamp
from multiprocessing import Pool,cpu_count
from time import time
from functools import reduce
from matplotlib import dates

makedir = lambda name: os.makedirs(name, exist_ok=True)

def construct_windows(df,NumWindows,N_window,N_overlap = 0):
    '''
    Constructs overlapping time-series windows
    
    Adapted from Whakaari Code...
    https://github.com/ddempsey/whakaari/blob/master/whakaari/__init__.py line 637

    '''

    # Whakaari code uses a get_data method to capture a certain date range, But I currently dont do this...
    ti = df.index[0]
    ##dt = (1.-N_overlap/N_window)*N_window

    dfs = []
    window_dates = []

    if N_overlap == 0:
        for i in range(0,NumWindows):
            dfi = df.iloc[ int(i*(N_window)): int(i*(N_window) + N_window) ] # Take a subset of the oriiginal df (subsets share some values)
            window_dates.append(dfi.index[-1])
            try:
                dfi['id'] = pd.Series(np.ones(N_window, dtype=int)*i, index=dfi.index) # assign an id to each window for the purpose of grouping by
            except ValueError:
                print('hi')
            dfs.append(dfi)
    else:
        for i in range(0,NumWindows):
            dfi = df.iloc[ int(i*(N_overlap)): int(i*(N_overlap) + N_window) ]
            window_dates.append(dfi.index[-1])
            try:
                dfi['id'] = pd.Series(np.ones(N_window, dtype=int)*i, index=dfi.index) # assign an id to each window for the purpose of grouping by
            except ValueError:
                print('hi')
            dfs.append(dfi)
    
    df = pd.concat(dfs) # join all the dataframes together
    df = df.reset_index(drop = True)
    
    #window_dates = [ti + i*dt for i in range(NumWindows)]
    #window_dates = np.repeat(window_dates,N_window)
    return df, window_dates

def custom_make_fc(column_names):
    """
    Temporary function to get the important subfeatures (will upgrade later)
    Calls tsfresh.utils from_columns()
    """

    subfeatures_fc = from_columns(column_names)
    fdict = [name.replace('|','__') for name in subfeatures_fc]
    features_fc = from_columns([str(name) for name in fdict])

    return features_fc, subfeatures_fc

def calc_features(windowed_df,feature_fc,default):

    #use_default = False
    #if isinstance(feature_fc, type(None)): #if a kind_to_fc_parameters is not given then the keyword arguement default_fc_parameters must be
    #    use_default = True
    
    # Perform feature extraction once then assign timestamp and id
    if default:
        X = extract_features(windowed_df,column_id='id', column_sort='seconds',default_fc_parameters=feature_fc, disable_progressbar = False) # extract a small set of features
    else:
        X = extract_features(windowed_df,column_id='id', column_sort='seconds',kind_to_fc_parameters=feature_fc, disable_progressbar = False)
    X.dropna(axis=1,inplace = True)
    return X

def do_selection(X,y,q,transform = True):
    X.set_index(y.index,inplace=True) ##Xneeded?
    selector = FeatureSelector(ml_task="regression",fdr_level=q).fit(X,y) # intialise selector transformer object
    
    ## Get the p-values and corresponding feature names (same index)
    X_selected = selector.transform(X) if transform else X
    names = selector.features #if transform else X_selected.columns
    
    # Hypothesis test info as a tuple of feature names and p_values
    test = (names,selector.p_values[:X_selected.shape[1]])
    return X_selected,test

def custom_extract_subfeatures(windowed_df,
                            window_dates,
                            NumSubwindows,
                            feature_fc,
                            subfeature_fc = None,
                            q = 1e-6,
                            selection = False,
                            subwindow_length = 600,
                            label = 'Efficient',
                            N_overlap = 0):
    ''' This function takes a dataframe
    Wrapper 
    '''
    use_default = False
    Ndays = int(windowed_df.shape[0]/8.64e6)
    if isinstance(subfeature_fc, type(None)): #if a kind_to_fc_parameters is not given then the keyword arguement default_fc_parameters must be
        subfeature_fc = feature_fc
        use_default = True
    
    X0 = find_matrix(window_dates,label = label) # check for the feature matrix to speed up computation
    if isinstance(X0,type(None)): #if nothing found
        X0 = calc_features(windowed_df,feature_fc,use_default)

    X0.columns = [str(col).replace('__', '|') for col in X0.columns] # Rename columns
    X0['id'] = X0.index ## needed?
    X0.set_index(pd.to_datetime(window_dates),inplace=True) ## needed?
    delta_t = X0.index - X0.index[0]
    X0['seconds'] = delta_t.total_seconds() ##+ delta_t.microseconds/1e6
    save_feat(X0,window_dates,label+'_features') # custom function saves features as parquet in /tmp/Feature Matrices

    #Format df
    X0_windowed,window_dates2 = construct_windows(X0,NumSubwindows,subwindow_length,N_overlap) # assign windows to new df
    
    # Features on Features
    X1 = calc_features(X0_windowed,subfeature_fc,use_default)
    y = get_y(window_dates[0],Ndays)

    ## Save Model results
    #s = '_fdr_level_'+ '{:.1e}'.format(q) if selection == True else ''

    if selection is True: ## (Optional) Performs the feature selection as well
        X1_selected,testing = do_selection(X1,y,q,transform = True) # return only selected features and their p_values
        save_feat(X1_selected,window_dates,label +'_selected_subfeatures') #+ s
        return X0
    else:
        save_feat(X1,window_dates,label +'_subfeatures') # Full SubFeature Matrix #+ s
        X1_selected,testing = do_selection(X1,y,q,transform = False) #return full df and all p-vals
        return X0,X1,testing # return both dataframes


if __name__ == '__main__':
    # Feature Calculation
    construct_windows(df, NumWindows, N_window)
    custom_make_fc(column_names)
    calc_features(windowed_df,subfeature_fc,feature_fc)
    do_selection(X,y,q)
    custom_extract_subfeatures(windowed_df,window_dates,NumSubwindows,feature_fc)


    