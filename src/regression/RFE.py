from RSAM_predictions import remove_features_with_RFE

if __name__ == '__main__':
        
        resultsname = 'TEST.csv' #sys.argv[1]
        label = 'MINIMAL_TEST' #'_fdr_1e-8_' #sys.argv[2]
        extension ='*_subfeatures.gzip' #sys.argv[3]

        ## Load in subfeature matrices
        begin = time.time()
        f_list  = find_files(label,extension,display=True)
        X,y = load_training_data(f_list,min_files=14)
        end = time.time()

        #metadata['time_to_load_data'] = end-begin
        print('Loading time:',end-begin,'\n')
        print(X.head(),'\n',y.head())
        assert X.shape[0] == y.shape[0]

        #X,feature_rankings = lgbm_rfe(X,y,feature_target=15)

        remove_features_with_RFE(label,target = 15,stepsize = 10,save_df = True,display=True)
        #remove_features_with_RFE(label,target = 1000,stepsize = 1000,save_df = True,display=True)