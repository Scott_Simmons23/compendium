#from simple_RSAM_predict import rmse
from os import fsdecode
import matplotlib.pyplot as plt
from matplotlib import dates
from numpy.core.fromnumeric import size
from numpy.lib.function_base import angle
from pandas.io.parsers import read_csv
import seaborn as sns
import numpy as np
import pandas as pd
from math import isnan
from sklearn.metrics import r2_score

from RSAM_get_data import create_RSAM_df,get_y,get_data_for_day
from RSAM_predictions import rms,percentage_error

##
def compare_station_distributions(day = '2013-12-05 00:00:00'):
    try:
        WIZ = create_RSAM_df(day,i=1)
        WSRZ = create_RSAM_df(day,i=1)
    except:
        get_data_for_day('WIZ',day)
        WIZ = create_RSAM_df(day,i=1)
    pass

def example_input_signal(day = '2013-12-05 00:00:00',save = True,figsize = (10,5),titlesize = 10,size = 5,subtitlesize = 8,suppress_title = True):
    
    f,ax = plt.subplots(figsize = figsize)

    if suppress_title is not True:
        plt.suptitle('Raw Seismic Velocity Data Taken From WIZ station',size = titlesize,fontweight = 'bold')
    #plt.text(x=0.5, y=0.92,s= 'Date: '.format(day.split(' ')[0]),fontsize=subtitlesize,ha="center",transform=f.transFigure)
    
    try:
        X = create_RSAM_df(day,i=1)
    except:
        get_data_for_day('WIZ',day)
        X = create_RSAM_df(day,i=1)
    
    X.plot(x = 'seconds',y = 'rsam',ax = ax)
    ax.set_xlabel('Time [s]',size = size)
    ax.set_ylabel('Seismic Velocity data',size = size)
    ax.tick_params(axis='both', which='major', labelsize=size)
    ax.legend(fontsize = size)
    #Xplt.xticks(np.round(np.linspace(0,int((X.shape[0]/100)),24)/3600))
    #plt.xticks(np.arange(0, 8.64e6, step=1e4))
    #formatter = dates.DateFormatter('%Y-%m-%d %H:%M:%S')            # must do this to correctly format dates 
    #plt.gcf().axes[0].xaxis.set_major_formatter(formatter) 

    if save:
        plt.savefig('#.png')
    return

def example_target_signal(day = '2013-12-05 00:00:00',save = True,figsize = (10,5),titlesize = 10,size = 5,subtitlesize = 8,suppress_title = True):

    f,ax = plt.subplots(figsize = figsize)
    if suppress_title is not True:
        plt.title('RSAM at WSRZ station',size = titlesize,fontweight = 'bold')
    #plt.text(x=0.5, y=0.92,s= 'Date: '.format(day.split(' ')[0]),fontsize=subtitlesize,ha="center",transform=f.transFigure)
    
    y = get_y(day,1)
    y.plot(ax = ax)
    #plt.plot(x = y.index,y = y.values,ax = ax)
    ax.set_xlabel('Time',size = size)
    ax.set_ylabel('RSAM [$\mathregular{nms^{-1}}$]',size = size)
    ax.tick_params(axis='both', which='major', labelsize=size)
    ax.legend(fontsize = size)
    #formatter = dates.DateFormatter('%Y-%m-%d %H:%M:%S')            # must do this to correctly format dates 
    #plt.gcf().axes[0].xaxis.set_major_formatter(formatter) 
    
    if save:
        plt.savefig('example_output_signal.png')
    return

def plot_prediction_results(results,rmse,mape,description_string,
                            figsize = (10,5),
                            title_size = 10,
                            subtitlesize = 8,
                            size = 5,
                            labelsize = 8,
                            textsize = 5,
                            markersize = 2,
                            hspace = 0.3,
                            wspace = 0.2,
                            linecolour = 'red',
                            alpha = 0.5,
                            linewidth = 0.5,
                            save = True,
                            fname ='prediction_results_subplot.png'):
        '''
        results: dataframe
                dataframe with columns time, y, y_pred, residuals
        '''

        ##############################
        ## Plot the two time series ##
        ##############################
        f,ax = plt.subplots(nrows = 2,ncols=2,figsize = figsize,gridspec_kw = {'width_ratios': [2,1]} ) ## axes formatting
        gs = ax[1, 1].get_gridspec()
        ax[1,1].remove()
        ax[0,1].remove()
        axbig = f.add_subplot(gs[:,1])

        plt.suptitle('Out of Sample Predicted RSAM at Station WSRZ',size = title_size,fontweight='bold')
        ax[0,0].set_title('Comparison of Predicted vs True RSAM', size = subtitlesize )
        plt.xticks(fontsize=size)
        plt.yticks(fontsize = size)
        plt.text(x=0.5, y=0.92,s= description_string,fontsize=size,ha="center",transform=f.transFigure)
        
        results.plot(x = 'time',y = 'y_pred', label = 'Predicted RSAM',color = linecolour,linewidth = linewidth, linestyle = '--', ax = ax[0,0])
        results.plot(x = 'time',y = 'y', label = 'RSAM',linewidth = linewidth, ax = ax[0,0])

        ax[0,0].set_xlabel('Time',size = size)
        ax[0,0].set_ylabel('RSAM [$\mathregular{nms^{-1}}$]',size = size)
        ax[0,0].text(0.20,0.95,"RMSE:{:.1f}\nMAPE: {:.1f}%".format(rmse,mape),size = textsize,transform=ax[0,0].transAxes,color = 'g',ha="right",va="top",bbox=dict(facecolor='none', edgecolor='green', pad=5.0) )
        ax[0,0].tick_params(axis='both', which='major', labelsize=size)
        ax[0,0].legend(fontsize = textsize)
        ##ax[0,0].sharex(ax[1,0])                                     ## Get the x axes to share scale
        #plt.xticks(fontsize=size)
        #plt.yticks(fontsize = size)
        
        ###################################
        ## Plot the residuals underneath ##
        ###################################
        sns.set_style('ticks')
        ax[1,0].set_title('Fitted Residuals', size = subtitlesize )
        r = results.residuals
        #x = results.y_pred
        upper = max(results.y_pred)+100
        low = min(results.y_pred)-100

        results.plot(x= 'y_pred',y = 'residuals',linestyle = 'None', marker = 'o', color = 'tab:blue',markersize = markersize, alpha = alpha,ax = ax[1,0])
        #ax[1,0].plot(x,r,linestyle = 'None', marker = 'o', color = 'tab:blue',markersize = markersize, alpha = alpha )
        ax[1,0].hlines(y=0, xmin=0, xmax=upper, linewidth=1, color='black',linestyle = '--')
        
        ax[1,0].set_xlabel('Predicted RSAM',size = size)
        ax[1,0].set_ylabel('Residuals',size = size)
        ax[1,0].set_xlim([low,upper])
        ax[1,0].get_legend().remove()
        #ax[1,0].set_ylim([round(min(r),-2),round(max(r),-2)])
        ax[1,0].tick_params(axis='both', which='major', labelsize=labelsize)
        ax[1,0].yaxis.grid(linestyle = '--')
        #ax[1,0] = plt.gca()

        #plt.xticks(fontsize=size)
        #plt.yticks(fontsize = size)

        ############################################
        ## Prediction and True values Scatterplot ##
        ############################################
        results.plot(x = 'y_pred',y = 'y',linestyle = 'None', marker = 'o', color = 'tab:blue',markersize = markersize, ax = axbig, alpha = alpha)
        axbig.set_title('Predicted and True RSAM Correlation', size = subtitlesize )#, fontweight = 'bold')
        #ax[1].set_title('Fitted Residuals', size = 20, fontweight = 'bold' )
        #plt.plot(x,abline_values, linestyle = '-.', color = 'g')
        lower = min(min(results.y_pred),min(results.y))
        upper = max(max(results.y_pred),max(results.y))

        axbig.set_xlim([lower,upper])
        axbig.set_ylim([lower,upper])
        axbig.set_xlabel('Predicted RSAM',size = size)
        axbig.set_ylabel('True RSAM',size = size)
        axbig.tick_params(axis='both', which='major', labelsize=size)

        f.tight_layout(rect=[0, 0.03, 1, 0.95]) # nb rect gives space for title
        plt.subplots_adjust(hspace = hspace,wspace=wspace)
        plt.xticks(fontsize=size)
        plt.yticks(fontsize = size)
        
        if save:
            plt.savefig(fname)
        return

def prediction_scatter_plot(results,
                            figsize = (10,5),
                            title_size = 10,
                            subtitlesize = 8,
                            size = 5,
                            labelsize = 8,
                            textsize = 5,
                            markersize = 2,
                            hspace = 0.3,
                            wspace = 0.2,
                            alpha = 0.5,
                            xlim = [],
                            ylim = [],
                            save = True,
                            fname ='example_output_signal.png',
                            suppress_title = True):

    f,ax = plt.subplots(figsize = figsize)
    results.plot(x = 'y_pred',y = 'y',linestyle = 'None', marker = 'o', color = 'tab:blue',markersize = markersize, ax = ax, alpha = alpha)
    
    if suppress_title is not True:
        ax.set_title('Predicted and True RSAM Correlation', size = subtitlesize )#, fontweight = 'bold')
        #ax[1].set_title('Fitted Residuals', size = 20, fontweight = 'bold' )
        #plt.plot(x,abline_values, linestyle = '-.', color = 'g')
    
    if ((len(xlim) == 0) and (len(ylim)==0)):
        lower = min(min(results.y_pred),min(results.y))
        upper = max(max(results.y_pred),max(results.y))
        ax.set_xlim([lower,upper])
        ax.set_ylim([lower,upper])
    else:
        ax.set_xlim(xlim)
        ax.set_ylim(ylim)
    
    ax.set_xlabel('Predicted RSAM',size = size)
    ax.set_ylabel('True RSAM',size = size)
    ax.tick_params(axis='both', which='major', labelsize=size)
    ax.get_legend().remove()

    f.tight_layout(rect=[0, 0.03, 1, 0.95]) # nb rect gives space for title
    plt.subplots_adjust(hspace = hspace,wspace=wspace)
    plt.xticks(fontsize=size)
    plt.yticks(fontsize = size)
        
    if save:
        plt.savefig(fname,bbox_inches = 'tight')
    return

def compare_time_series(results,rmse,mape,
                            figsize = (10,5),
                            title_size = 10,
                            size = 5,
                            linecolour = 'red',
                            alpha = 0.5,
                            linewidth = 0.5,
                            save = True,
                            suppress_title = False,
                            fname ='prediction_results_subplot.png'):
        '''
        results: dataframe
                dataframe with columns time, y, y_pred, residuals
        '''

        ##############################
        ## Plot the two time series ##
        ##############################
        f,ax = plt.subplots(figsize = figsize) #(nrows = 2,ncols=2,figsize = figsize,gridspec_kw = {'width_ratios': [2,1]} ) ## axes formatting
        #gs = ax[1, 1].get_gridspec()
        #ax.remove()
        #ax.remove()
        #axbig = f.add_subplot(gs[:,1])

        #plt.suptitle('Comparison of true and predicted RSAM',size = title_size,fontweight='bold')
        
        if suppress_title is False:
            ax.set_title('Comparison of Predicted and True RSAM', size = title_size, fontweight='bold' )
        plt.xticks(fontsize=size)
        plt.yticks(fontsize = size)
        #plt.text(x=0.5, y=0.92,s= description_string,fontsize=size,ha="center",transform=f.transFigure)
        
        results.plot(x = 'time',y = 'y_pred', label = 'Predicted RSAM',color = linecolour,linewidth = linewidth, ax = ax) #linestyle = '--'
        results.plot(x = 'time',y = 'y', label = 'RSAM',linewidth = linewidth, ax = ax)

        ax.set_xlabel('Time',size = size)
        ax.set_ylabel('RSAM [$\mathregular{nms^{-1}}$]',size = size)
        ax.text(0.20,0.95,"RMSE:{:.1f}\nMAPE: {:.1f}%".format(rmse,mape),size = size,transform=ax.transAxes,color = 'g',ha="right",va="top",bbox=dict(facecolor='none', edgecolor='green', pad=5.0) )
        ax.tick_params(axis='both', which='major', labelsize=size)
        ax.legend(fontsize = size)

        if save:
            plt.savefig(fname,bbox_inches = 'tight')
        return 

def plot_residuals_overlaid(results,save = True,showlines = True,figsize = (10,5),title_size = 10,markersize = 0.5,suppress_title = True,linewidth = 1,size = 8,fname = 'residuals overlaid.png'):
    time = results.time
    y = results.y_pred

    fig,ax = plt.subplots(figsize = figsize)
    if suppress_title is False:
        plt.suptitle('RSAM forecast with residuals',size = title_size,fontweight='bold')
    
    #ax.plot(time,y, color = 'tab:blue', linewidth = linewidth)
    results.y_pred.plot(color = 'tab:blue', linewidth = linewidth,ax = ax)
    ax.scatter(time,y + results.residuals, color = 'red', marker = 'o',s = markersize) ## overlay the residuals
    
    if showlines:
        ax.vlines(time,y,y + results.residuals,color = 'red',linewidth = linewidth) ## show lines representing the residuals
    
    ax.set_xlabel('Time',size = size)
    ax.set_ylabel('True RSAM',size = size)
    ax.tick_params(axis='both', which='major', labelsize=size)
    ax.yaxis.set_major_locator(plt.MaxNLocator(5))
    ax.xaxis.set_major_locator(plt.MaxNLocator(5)) ## restrict the number of xaxis ticks to
    ax.legend(['Prediction','True Values'])
    ax.yaxis.grid(linestyle = '--')
    #plt.gcf().autofmt_xdate()
    #plt.xticks(rotation=70)

    #formatter = dates.DateFormatter('%Y-%m-%d %H:%M:%S') 
    #plt.gcf().axes[0].xaxis.set_major_formatter(formatter)
    
    if save:
        plt.savefig(fname,bbox_inches = 'tight')
    
    return

def plot_feature_time_series(df,feature_names,nrows = 1, ncols = 1,figsize = (10,5),size = 5, save = True,fname = 'Feature Time Series.png'):
        """ 
        Create a plot a time series, either feature(s)

        Parameters:
        -----------
        column_name : str or default = None
            the name of the column to be plotted
        
        feature names : list of strings
            the features to be plotted on the same axis

        Returns:
        --------
        
        """
        fig,axes = plt.subplots(nrows,ncols) # create fig,axes object
        assert (nrows*ncols == len(feature_names))
        #if feature_names is None:                                       # Plot target vector by default 
        #    self.y.plot(ax = axes,**kwargs)
        if (nrows*ncols > 1):                                         # plots on multiple axes
            for i,axis in enumerate(axes):
                df.plot(y = feature_names[i],ax = axis) 
                #formatter = dates.DateFormatter('%Y-%m-%d %H:%M:%S')            # must do this to correctly format dates 
                #plt.gcf().axes[0].xaxis.set_major_formatter(formatter)
        else:
            for ft in feature_names:                                    # loop through list of features to plot
                df.plot(y = ft,ax = axes)
        
        #formatter = dates.DateFormatter('%Y-%m-%d %H:%M:%S')            # must do this to correctly format dates 
        #plt.gcf().axes[0].xaxis.set_major_formatter(formatter)                
        axes.set_xlabel('Time') 
        
        if save:
            plt.savefig(fname)

        return

def plot_residuals(results,save = True,figsize = (10,5),title_size = 10,size = 5,markersize = 2,alpha = 0.5,fname = 'residual plot png'):
    
    sns.set_style('ticks')
    f,ax = plt.subplots(figsize = figsize)
    ax.set_title('Fitted Residuals', size = title_size )
    r = results.residuals
    #x = results.y_pred
    upper = max(results.y_pred)+100
    low = min(results.y_pred)-100

    results.plot(x= 'y_pred',y = 'residuals',linestyle = 'None', marker = 'o', color = 'tab:blue',markersize = markersize, alpha = alpha,ax = ax)
        #ax[1,0].plot(x,r,linestyle = 'None', marker = 'o', color = 'tab:blue',markersize = markersize, alpha = alpha )
    ax.hlines(y=0, xmin=0, xmax=upper, linewidth=1, color='black',linestyle = '--')
        
    ax.set_xlabel('Predicted RSAM',size = size)
    ax.set_ylabel('Residuals',size = size)
    ax.set_xlim([low,upper])
    ax.get_legend().remove()
    #ax[1,0].set_ylim([round(min(r),-2),round(max(r),-2)])
    ax.tick_params(axis='both', which='major', labelsize=size)
    ax.yaxis.grid(linestyle = '--')

    if save:
        plt.savefig(fname)

    return

def feature_target_corr(X,y,ncols = 4,
                    figsize = (10,5),
                    size = 5,
                    titlesize = 10,
                    textsize = 5,
                    save = True,
                    hspace = 0.4,
                    wspace = 0.4,
                    markersize = 1,
                    fname = 'Feature Time Series Correlation.png',
                    title_string = 'Feature on Feature correlations with the target',
                    bestfit = True):

    """
    Scatter plot that also displays pearson's correlation coefficent (up to 50 features)

    table:
        feature matrix 

    """
   
    numftr = X.shape[1]-1
    rownum = int(numftr/ncols)
    nrows = 1 if rownum == 0 else rownum
    position  = X.shape[1]-1
    X.insert(position,'rsam',y)

    if (X.shape[1] > 50): # more than 50 features (51 columns including RSAM)
        print('Too many features to plot (max: 50)')
        return
    elif (ncols*nrows < numftr):
        print('Too few columns')
        return
    
    #sns.set_style('darkgrid')
    f,axes = plt.subplots(nrows,ncols, figsize = figsize, sharey = True)
    f.suptitle(title_string,size = titlesize)
    
    # Loop through Features and plot each feature against RSAM
    a = axes.flatten()
    for ftr in range(numftr+1):
        if (X.columns[ftr] != 'rsam'):
            
            #Scatterplot of feature vs RSAM values
            X.plot(x = X.columns[ftr],y = 'rsam',linestyle = 'None', marker = 'o', color = 'tab:blue',ax = a[ftr],markersize = markersize)
            
            # Line of best fit
            #if bestfit:
            #    sns.regplot(x = X[X.columns[ftr]],y = X['rsam'],ax = a[ftr],ci = None, scatter_kws={"color": "tab:blue"},line_kws={"color": "red", 'linestyle' : 'dashed'})
            
            # Print the correlation score
            r = np.corrcoef(X[X.columns[ftr]], X['rsam'])       
            if isnan(r[0][1]) is False:
                a[ftr].text(0.05,0.95,"Pearson's \n correlation: {:.2f}".format(r[0][1]),size = textsize,transform=a[ftr].transAxes,color = 'g',ha="left",va="top",bbox=dict(facecolor='none', edgecolor='green', pad=5.0))
            else:
                a[ftr].text(0.05,0.95,"Pearson's \n correlation: NA",size = textsize,transform=a[ftr].transAxes,color = 'g',ha="left",va="top",bbox=dict(facecolor='none', edgecolor='green', pad=5.0) )

            #Labels
            a[ftr].get_legend().remove()
            a[ftr].set_xlabel(a[ftr].get_xlabel())
            a[ftr].set_ylabel('RSAM [$\mathregular{nms^{-1}}$]') 
            a[ftr].xaxis.label.set_size(textsize)
            a[ftr].tick_params(axis='both', which='major', labelsize=size)
    
    if (ncols*nrows > numftr):
        n = int(ncols*nrows - numftr)
        a[-n].set_visible(False) # suppress empty plots
    
    
    plt.xticks(fontsize = size)
    plt.yticks(fontsize = size)
    #plt.show()
    plt.tight_layout(rect=[0, 0.03, 1, 0.95])
    plt.subplots_adjust(hspace = hspace, wspace= wspace)

    if save:
        plt.savefig(fname)

    return

def plot_WIZ(ti = '2013-05-01 00:00:00',tf = '2020-11-15 00:00:10',figsize = (20,10),title_size = 10,size = 8,linewidth = 0.5,save = True,fname = 'WSRZ by year.png', hspace = 0.3,wspace = 0.3):
    
    df = pd.read_csv('WSRZ_tremor_data.csv')
    df.set_index(df.time,inplace=True)
    df['year'] = df.time.map(lambda x:pd.to_datetime(x).year)

    #Nyrs = df['years'][-1]-df['years'][1]
    f,axes = plt.subplots(ncols = 4,nrows = 2,figsize = figsize,sharey=True)
    a = axes.flatten()
    yr = 2013
    for i in range(0,8):
        df.loc[df.year == yr].plot('time','rsam',ax = a[i],linewidth = linewidth )
        a[i].set_xlabel('Date',size = size)
        a[i].set_ylabel('RSAM',size = size)
        a[i].get_legend().remove()
        a[i].tick_params(axis='both', which='major', labelsize=5)
        a[i].set_title(str(yr))
        yr += 1
    #a[-1].remove()
    plt.subplots_adjust(hspace = hspace,wspace=wspace)
    #plt.xticks(fontsize=size)
    #plt.yticks(fontsize = size)
    plt.gcf().autofmt_xdate()

    # split it by year


    #f,ax = plt.subplots(figsize = figsize)
    #plt.suptitle('WSRZ Station RSAM')
    #df.loc[ti:tf,:].plot('rsam',ax = ax,linewidth = 2)
    if save:
        plt.savefig(fname,bbox_inches = 'tight')

    return

def WSRZ_distribtuion(ti ='2013-05-01 00:00:00',tf = '2020-11-15 00:00:10',nbins = 25,save = True,figsize = (10,5),title_size = 10,size = 5,markersize = 2,xlim = [],alpha = 0.5,fname = 'WSRZ distribution.png'):
    'See if the distribution of training data matches the test data'
    df = pd.read_csv('WSRZ_tremor_data.csv')
    df.set_index(df.time,inplace=True)
    mat = df.loc[ti:tf,:].rsam

    f,axes  = plt.subplots()
    mat.plot.hist(bins = nbins,ax = axes,color = 'tab:blue')
    axes.set_ylabel('Frequency',size = size)
    axes.set_xlabel('RSAM',size = size)
    plt.xticks(fontsize = size)
    plt.yticks(fontsize = size)
    axes.xaxis.set_major_locator(plt.MaxNLocator(10))
    if ((len(xlim) == 0)) is False:
        axes.set_xlim(xlim)
        #axes.set_ylim(ylim)

    if save:
        plt.savefig(fname,bbox_inches = 'tight')

    return


def plot_eruptions():
    pass

def run_ALL_PLOTS():
    ''' Run and save every plot for the results'''
    pass

if __name__ == '__main__':
    
    #rmse = rms(results.y_pred,results.y)
    #mape = percentage_error(results.y_pred,results.y)

    compare_time_series(results,rmse,mape)
    prediction_scatter_plot(results,fname = 'prediction results scatterplot 1e-6 v2.png',figsize=(5,5),size=10,labelsize=10,textsize=10,xlim = [1500,3500],ylim = [1500,6000],save=False)
    plot_residuals(results)
    WSRZ_distribtuion(ti = '2019-10-15 00:00:00', tf = '2019-11-21 00:00:00',nbins = 100,save = False,size = 14)
