#!/bin/bash -e

#SBATCH --job-name    ParalellisedFeatExtraction
#SBATCH --time        00:20:00
#SBATCH --mem         20GB
#SBATCH --array=0-291
#SBATCH --cpus-per-task=4
# SBATCH --gpus-per-node=2

# Try 40GB, 16 CPus, 2 hours... 15 september on efficient

# Currently 5 signals per job... 
# 64 cpus per job for 10 sigs per job... 
# 80 GB for 10 signals per job..
# for efficient features use: 4 hours of walltime, 3 cpus per task, 30GB max memory limit
# for minimal features use: 30 seconds of walltime, 10GB limit, no GPU

# There are 194 signals with faults. So the balanced dataset should have a length of 388. So 0-387 is the optimal range is a good range for the array task ID for the BALANCED TRAINING DATASET
# The total number of training signals is 2904. So 2554 is the number of signals for the REMAINING DATASET   
# The total number of testing signals is 6779 so 0-6779 is good array range
# note since there are restrictions on array size. Do 0-5800; 5801-6779 as two separate slurm scripts
module load Python/3.8.2-gimkl-2020a 
export SLURM_EXPORT_ENV=ALL


# WITH selected features, 4 hours 50GB 4 CPUS is sufficnet for EFficient FC

#TEST TRAIN SPLIT NEW:
# train len: 0-65 
# test len: 0-175
# kaggle: 0-1360

# two command line args. First is the slurm array task id. Second is the path to the config file
# extract the features. They are put into the features directory as parquet files

# For extracting balanced features: config_extract_and_select_features_on_balanced_training_data_MINIMAL.yaml
#python extract_features_para_script.py $SLURM_ARRAY_TASK_ID config_extract_and_select_features_on_balanced_training_data_MINIMAL.yaml

# for extracting the remaining features on minimal: config_extract_features_on_remaining_training_data_RELEVANT_FROM_MINIMAL.yaml
#python extract_features_para_script.py $SLURM_ARRAY_TASK_ID config_extract_features_on_remaining_training_data_RELEVANT_FROM_MINIMAL.yaml

# for extracting features on the testing data: config_extract_and_select_features_on_all_test_data_RELEVANT_FROM_MINIMAL.yaml
#python extract_features_para_script.py $SLURM_ARRAY_TASK_ID config_extract_features_on_all_test_data_RELEVANT_FROM_MINIMAL.yaml

# for extracting on all features irrespective of their partition....
python extract_features_para_script.py $SLURM_ARRAY_TASK_ID config_extract_features_on_all_train_data_MINIMAL.yaml

# for extracting simple features.... NOTE: this is not robust. Lines need to be uncommented in the vsb_data module for it to work..
#python extract_features_para_script.py $SLURM_ARRAY_TASK_ID config_extract_all_simple_features_MINIMAL.yaml
