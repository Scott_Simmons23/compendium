#!/bin/bash -e

#SBATCH --job-name    ParalellisedFeatExtraction
#SBATCH --time        00:20:00
#SBATCH --mem         30GB
#SBATCH --array=0-291
#SBATCH --cpus-per-task=4
# SBATCH --gpus-per-node=2

module load Python/3.8.2-gimkl-2020a 
export SLURM_EXPORT_ENV=ALL


# for extracting simple features.... NOTE: this is not robust. Lines need to be uncommented in the vsb_data module for it to work..
python extract_simple_features_para_script.py $SLURM_ARRAY_TASK_ID config_extract_all_simple_features_MINIMAL.yaml
