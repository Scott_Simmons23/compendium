# All code in one file for now. Partition into modules later...
from datetime import datetime
from sklearn.model_selection import RandomizedSearchCV, GridSearchCV
import lightgbm as lgb
from lightgbm import reset_parameter,Dataset
from scipy.stats import randint as sp_randint
from scipy.stats import uniform as sp_uniform
import time
import random
from tsfresh.transformers import FeatureSelector
from tqdm import tqdm
from collections import Counter
import seaborn as sns
import matplotlib.pyplot as plt
from imblearn.under_sampling import RandomUnderSampler
import joblib
from sklearn.pipeline import Pipeline
from sklearn.feature_selection import RFECV
from sklearn.feature_selection import RFE
import sys
from xgboost import XGBClassifier
import numpy as np
from sklearn.model_selection import cross_val_score, RepeatedStratifiedKFold, train_test_split
from sklearn.metrics import confusion_matrix, matthews_corrcoef, f1_score
from sklearn.model_selection import KFold, cross_validate, StratifiedKFold
from sklearn.metrics import make_scorer, accuracy_score, precision_score, recall_score, f1_score
from lightgbm import LGBMClassifier
import joblib
from statsmodels.stats.multitest import multipletests
from tsfresh.transformers import FeatureSelector
import re
import math
import vsb_utils
import yaml
import os
import random
import glob
import pandas as pd
import pyarrow.parquet as pq
from fastparquet import ParquetFile
import numpy as np
from tsfresh.feature_extraction import extract_features
from tsfresh.feature_extraction.settings import MinimalFCParameters, EfficientFCParameters, from_columns
from tsfresh.transformers import FeatureSelector
from tsfresh import select_features

# Notes on the VSB Data:
# 800,000 rows
# each column is one observation of a time series eg phase0,1,2
# each row is a point in time.
# Each timeseries is 0.2 ms long


# classes and methods
class VSBData:
    ''' 
    Class for handling the VSB data
    
        Attributes:
            pq_path (str): path to the parquetfile
            pFile (parquetfile). The parquetfile with the data
            metadata (parquet metadata object): the metadata of the parquetfile.
            more_info (pd.DataFrame): dataframe containing the metadata in the metadata_*.csv file
            is_labelled (bool): true if data is labelled, otherwise false
            mids_train, mids_test,mid (array-like): the VSB measurement ids for the full parquetfile
            y_train,y_test,y (array-like): the responses for the full parquetfile
             
    '''

    def __init__(self, pq_path, metadata, is_labelled= True):
        '''
        Constructor for the VSBData class. See https://www.kaggle.com/c/vsb-power-line-fault-detection/data for info on the initial data layout 

            params:
                pq_path (str): path to the parquetfile with the data
                metadata (str): path to the csv file holding the metadata
                is_labelled (bool): true if the data is labelled, else false.
        '''

        # path to the parquet files.
        self.pq_path = pq_path

        # The parquet files... each column is a separate time series signal.
        self.pFile = pq.ParquetFile(pq_path)

        # Supplementary info on phase, measurement_id, signal_id, and binary response.
        # signal_id is the foriegn key which links to each time series stored in the
        # columns of the  parquet files
        self.more_info = pd.read_csv(metadata)
        self.more_info = self.more_info.set_index("signal_id")

        # pq_metadata on the parquet files
        self.metadata = self.pFile.metadata

        # flag saying if the data is lablled or not
        self.is_labelled = is_labelled
        
        # generates the test train split IDs for extraction. 
        # NOTE: THIS IS ONLY RELEVANT IF EXTRACTING ON TEST AND TRAIN IDs AS 
        # SEPARATE STAGES. If extracting features on everything, this doesnt 
        # get used.
        if is_labelled: self.gen_train_test_split(test_size = 0.15) 

    def load_signal(self, meas_id, window_length):
        '''
        Loads and windows in one VSB measurement (a timeseries of all 3 phases + 18 engineered VSB signals
           
           Params:
               meas_id (int or string): VSB measurement id
               window_length (int): window size
        '''
        meas_id = int(meas_id)

        # map measurement id to the unique id for each phase
        sig_ids = [3*meas_id, 3*meas_id + 1, 3*meas_id + 2]
        sig_ids = [str(x) for x in sig_ids]
    
        # get 3 timeseries (one for each phase) from the pq file
        ts = pd.read_parquet(path = self.pq_path, engine = "fastparquet", columns = sig_ids)
    
        # get the info on phase, response, etc
        relevant_meta = self.more_info[self.more_info["id_measurement"] == meas_id]
    
        #### asserts #####
        # check that signal ids correpond properly to the meta... i.e. no missing values
        assert list(relevant_meta.index) == [int(x) for x in sig_ids]
        # check that phases are 0,1,2 in that order...
        assert  (np.diff(relevant_meta["phase"]) > 0).all() and relevant_meta["phase"].isin([0,1,2]).all()
    
        # rename columns
        ts = ts.rename(columns=dict(zip(list(ts.columns), ["phase0","phase1","phase2"])))
        ts["measurement_id"] = meas_id # store measurment id
        ts["time_index"] = ts.index # store time index as a series object
        if self.is_labelled: ts["response"] = self.y[self.mid == meas_id] # if applicable, store the binary response
    
        # assign windows based on the window length. This just adds an index for each window ID.
        ts = segment_ts(ts, n = len(ts.index)/window_length)
    
        # compute the 18 other engineered time series
        ts = compute_signal_differences(ts)
    
        # return the final dataframe. Each column is a timeseries. Each row is an observation of an individual datapoint in that timeseries
        return ts

    def load_signals(self, measurement_ids, window_length):
        '''
        Calls load_signal() for multiple VSB measurement IDs. Returns a dataframe

            Parameters:
                    measurement_ids (list): List of ints
                    window_length (int): Window size...

            Returns:
                    ts (pd.DataFrame): VSB measurements.
        '''
        # loads multiple signals, then stacks into a large timeseries. Signals are distinguishable via measurement_id
        signals = [self.load_signal(meas_id = ID, window_length = window_length) for ID in measurement_ids]
        signals = pd.concat(signals)
        return signals

    def get_y_for_each_meas_id(self):
        '''
        Assigns a vector of responses and a measurement ids to the VSBDATA object. responses and measurement ids correspond
        to each other. These values are also returned by the function.

            Returns:
                mid (pd.index): measurements ids
                y (pd.series): responses 
        '''
        # used to get y and mid in a nice format. For a given signal id if at least one response == fault then set mid = fault
        assert self.is_labelled == True

        # Assumption: if, for a given VSB measurement, if at least one phase (i.e. one VSB signal) has a fault then the full
        # VSB measurement is considered a fault.
        responses_for_each_meas_id = self.more_info.groupby(["id_measurement"]).mean()
        responses_for_each_meas_id["target"] = np.ceil(responses_for_each_meas_id["target"])
        y = responses_for_each_meas_id["target"]
        mid = responses_for_each_meas_id.index
        # Assignn to object
        self.y = y
        self.mid = mid
        # Return
        return mid, y
 
        
    def get_all_ids(self):
        '''
        Returns all measurement ids for a given VSB object
            
            Returns: 
                all_ids (list): list of all the measurement ids associated with the VSB object.
        
        NOTE: This probebaly should be DEPRECATED however it is still used in the vsb_utils module.
        '''
        all_ids = self.more_info["id_measurement"].unique().tolist()
        assert len(all_ids) == len(set(all_ids))
        return all_ids


    def gen_train_test_split(self, test_size):
        '''
            Assigns the VSB data object with train and test sets. 
            DEPRECATED. In this pipeline the undersampling strategy was not ideal. It happened before
            feature selection.
            
            Params:
                test_size (float): fraction of the data which will be assigned to the test set

        ''' 
        # Step one: stratify data into train and test. Step 2: Undersample on the training data.
        mid, y = self.get_y_for_each_meas_id()
        # split up mids into train and test 
        mids_train, mids_test, y_train, y_test = train_test_split(mid, y, test_size = test_size, stratify = y, random_state = 700)
        # Downweight the non faults..... undersampling
        undersample = RandomUnderSampler(sampling_strategy='majority', random_state = 700)
        # fit and apply the transform
        print(type(mids_train))
        mids_train, y_train = undersample.fit_resample(mids_train.values.reshape(-1,1), y_train)
        self.mids_train = mids_train
        self.mids_test = mids_test 
        self.y_train = y_train
        self.y_test = y_test 
        print("Successfully made the train test split for labelled data.. random seed = 700")



# functions
def segment_ts(ts, n):
    ''' 
        
    # assigns a window ID as a new column to a timeseries dataframe, based on the order of the timeseries input.
    # non-overlapping windows

        params:
            ts (pd.DataFrame): dataframe representing a time series. Rows are the obervations.
            n (int): number of windows.
        
        returns:
            ts (pd.DataFrame): mutated dataframe representing the same input time series, but with a new column for window id.
    '''

    ts_segments = np.array_split(ts, n) # this function accounts for the final window being smaller in size.

    for window_id, timeseries in enumerate(ts_segments):
        timeseries["window_id"] = window_id

    ts = pd.concat(ts_segments)
    return ts

def compute_signal_differences(ts):
    ''' 
        time series differencing operations to add new engineered time series to the input time series
        
        params:
             ts (pd.DataFrame): time series input with 3 phases ---- phase0, phase1, phase2.
        returns:
             ts (pd.DataFrame): time series with 3 phases and 18 signals. See function implementation for details
    '''
    # temporal derivatives dt0, dt1, dt2
    ts["dt0"] = ts["phase0"].diff()
    ts["dt1"] = ts["phase1"].diff()
    ts["dt2"] = ts["phase2"].diff()

    # phase derivatives D01, D02, D12
    ts["D01"] = ts["phase0"] - ts["phase1"]
    ts["D02"] = ts["phase0"] - ts["phase2"]
    ts["D12"] = ts["phase1"] - ts["phase2"]

    # phase derivatives of phase derivatives
    ts["D_D01D02"] = ts["D01"] - ts["D02"]
    ts["D_D01D12"] = ts["D01"] - ts["D12"]
    ts["D_D02D12"] = ts["D02"] - ts["D12"]

    # temporal derivatives of phase derivatives
    ts["dt_D01"] = ts["D01"].diff()
    ts["dt_D02"] = ts["D02"].diff()
    ts["dt_D12"] = ts["D12"].diff()
    ts.loc[0,["dt0","dt1","dt2", "dt_D01", "dt_D02", "dt_D12"]] = 0 # adjust for the NaN value for temporal derivatives at first index...

    # phase derivatives of temporal derivatives
    ts["D_dt0dt1"] = ts["dt0"] - ts["dt1"]
    ts["D_dt0dt2"] = ts["dt0"] - ts["dt2"]
    ts["D_dt1dt2"] = ts["dt1"] - ts["dt2"]

    # temporal derivatives of temporal derivatives
    ts["dt_dt0"] = ts["dt0"].diff()
    ts["dt_dt1"] = ts["dt1"].diff()
    ts["dt_dt2"] = ts["dt2"].diff()
    ts.loc[0,["dt_dt0", "dt_dt1", "dt_dt2"]] = 0 # adjust for NaN value at first index...

    return ts




# main features on features framework
def features_on_features_vsb(ts,
                             first_fc_params,
                             second_fc_params,
                             fc_params_is_kind,
                             replacement_token):
    '''     
    main algorithm that uses tsfresh which computes features on features (feature dynamics) for the VSB data
    NOTE: The DATA format that this function supports is input option 1 for feature extraction https://tsfresh.readthedocs.io/en/latest/text/data_formats.html

        params:
            ts (pd.DataFrame): The VSB measurements that will be processed.
            first_fc_params, second_fc_params (dictionary): feature sets for (1) the extraction of feature time series and (2) the extraction of feature dynamics
            fc_params_is_kind (bool): if the feature dictionaries maps each separate VSB signal (ts kind) to a different feature set then this value is True, otherwise false.
            replacement_token (str): token that replaces double unscore in feature naming convention. This adjustment is required for featue dynamics extraction
        returns:   
            X1 (pd.DataFrame): Feature dynamics matrix
            y1 (pd.Series): Response vector.    
    '''

    # map the reponse variable to each row in the features on features matrix if the timeseries is test data otherwise do nothing...
    try:
        y = ts.groupby("measurement_id").last()["response"] 
    except:
        y = None


    # assign unique pairs of (mes_id, window_id) to each element
    ts["column_id"] = ts["measurement_id"].astype(str) + ", " + ts["window_id"].astype(str)


    # drop the columns which are not relevant to feature extraction
    try:
        ts = ts.drop(columns = ["measurement_id", "window_id", "response"]) # for labelled data
    except:
        ts = ts.drop(columns = ["measurement_id", "window_id"]) # non labelled data

    print("TS INPUT {}".format(ts))
    # first round of feature extraction FEATURE TIME SERIES
    X0 = (extract_features(ts, column_id = "column_id",column_sort = "time_index", kind_to_fc_parameters = first_fc_params, disable_progressbar = True) if fc_params_is_kind
          else extract_features(ts, column_id = "column_id",column_sort = "time_index", default_fc_parameters = first_fc_params, disable_progressbar = True))

    print("FIRST {}".format(X0.shape))

    # drop any features that produce any NaNs/NAs
    if X0.isnull().values.any():
        # store dropped features
        dropped_feature_names = [col_name for col_name in X0.columns[X0.isna().any()].tolist()]
        # store the feature calculators that fail in a file that is constantly updated.
        with open("dropped_feature_names.txt", "a") as f:
            for feature in dropped_feature_names: f.write(feature[feature.index("__") + 2:] + "\n") # 2 is a magic number. It works. But this should be refactored..

        print("found " + str(len(X0.columns[X0.isna().any()].tolist())) + " features from the set of " + str(len(X0.columns)) + " features which should be dropped before being input into second feat extraction")
        X0 = X0.dropna(axis = "columns")


    # tsfresh cant handle double underscores twice so change this in preparation for the second feature extraction
    X0.columns = [str(col_name).replace("__",replacement_token) for col_name in X0.columns]

    # assign windows as the original measurment ID... i.e. extracting "mes_id" from (mes_id, window_id)
    X0["column_id"] = X0.index.to_series().str.split(", ", expand = True).iloc[:,0]

    print("FEATURE TS INPUT {}".format(X0))

    # second round of feature extraction FEATURE DYNAMICS
    X1 = (extract_features(X0, column_id = "column_id", kind_to_fc_parameters = second_fc_params, disable_progressbar = True) if fc_params_is_kind
          else  extract_features(X0, column_id = "column_id", default_fc_parameters = second_fc_params, disable_progressbar = True))

    X1.index.name = "measurement_id"

    # drop any features which are null or na
    if X1.isnull().values.any():
        print("found " + str(len(X1.columns[X1.isna().any()].tolist())) + " features from the set of " + str(len(X1.columns)) + " features which should be dropped before being considered as the final output...")
        X1 = X1.dropna(axis = "columns")


    # sort column names 
    X1.sort_index(axis="columns", inplace=True)

    print("X1 output {}, {}".format(X1.shape, X1))

    # returning the feature matrix, the response variable corresponding to each feature matrix window, and optionally the dropped colnames
    return (X1, y)



# Scripts
##################################################################################

def script_extract_simple_features_parallel(array_task_id,
                                     m_ids,
                                     vsb_data_obj,
                                     first_fc_params,
                                     feature_extractions_dir,
                                     ts_kind_list,
                                     valid_ts_kinds,
                                     num_signals_per_job,
                                     window_length):

    '''
    Designed to be used with the xxxx.py file which in turn calls the xxx.sl file.

    Extracts features (not feature dynamics) on a given range of measurement ids and writes these features
    to directories. Used in the final results to extract the features for the benchmark model.

        params:
            array_task_id (int,str): the slurm job number that this script corresponds to. 
            m_ids (list): the VSB measurement IDs that features will be extracted for
            first_fc_params (dictionary): the feature set containing the relevant feature calculators 
            feature_extractions_dir (str): top level directory where the extracted features will be dumped
            ts_kind_list (list): the VSB signals that will be extracted.
            valid_ts_kinds (list): the correct names of all valid VSB signals.
            num_signals_per_job (int): number of VSB measurements that should be computed for a given array_task_id number
            window_length (int): not relevant because no windowing is applied to simple features.
    '''
  
    print("begin simple f extraction")
    # mapping from array id to a range of measurement_ids...
    start = time.time()

    groups_of_meas_ids = [m_ids[i:i + num_signals_per_job] for i in range(0, len(m_ids), num_signals_per_job)]

    assert array_task_id < len(groups_of_meas_ids)

    assert set(ts_kind_list) - set(valid_ts_kinds) == set(), "ts_kind_list contains invalid ts_kind names"

    # array_task_id is an int that can range from 0,1,.....N-1 where N-1 is 2 * number of signals with a fault

    # mapping from the array task ID 1,2,3,...N to a range of measurement ID [x1,x2,x3...M]
    meas_ids = groups_of_meas_ids[array_task_id]
    print("Extracting features on the following IDS: {}".format(meas_ids))

    # load in one signal at a time. Function can handle multiple meas_ids but is computationally expensive
    signals = vsb_data_obj.load_signals(measurement_ids = meas_ids,
                                       window_length = window_length)

    # drop cols that aren't going to be extracted
    signals.drop(set(valid_ts_kinds) - set(ts_kind_list), inplace = True, axis = "columns")

    print(signals)


    y = signals.groupby("measurement_id").last()["response"]
    signals = signals.drop(columns = ["window_id", "response"])
    for col in signals.columns: print(col)
    X = extract_features(signals, column_id = "measurement_id",column_sort = "time_index", default_fc_parameters = first_fc_params, disable_progressbar = True)
    X = X.dropna(axis = 1)

    X["response"] = y.values
    print("Successful extraction")

    # write extracted features to files in an efficient way
    for ts_kind in ts_kind_list:
        features_related_to_ts_kind = [feature for feature in X if feature.startswith(ts_kind)]
        assert features_related_to_ts_kind is not None


        # write the ts_kind to parquet
        feature_vector_of_given_kind = pd.DataFrame(X, columns=features_related_to_ts_kind + ["response"]).T
        feature_vector_of_given_kind.columns = feature_vector_of_given_kind.columns.astype(str)
        print("writing extracted feature vector to parquet")
        print(feature_vector_of_given_kind)

        feature_vector_of_given_kind.to_parquet(path = feature_extractions_dir + "/"+ ts_kind + "/feature_vectors/num_meas_ids_{}_cluster_num_{}_ts_kind_{}_features.parquet".format(num_signals_per_job, array_task_id, ts_kind), index = True, engine = "fastparquet", compression = "snappy")

        print("Wrote features to path: {}".format(feature_extractions_dir + "/"+ ts_kind + "/feature_vectors/num_meas_ids_{}_cluster_num_{}_ts_kind_{}_features.parquet".format(num_signals_per_job, array_task_id, ts_kind)))

    end = time.time()
    with open("feature_extraction_log.log", "a+") as f:
        f.write("SIMPLE MINIMAL FEATURE EXTRACTION yielding N_features: {} to be dumped into the extraction directory: {} in TIME: {}\n".format("?", feature_extractions_dir, end - start))


def script_extract_features_parallel(array_task_id,
                                     m_ids,
                                     vsb_data_obj,
                                     first_fc_params,
                                     second_fc_params,
                                     fc_params_is_kind,
                                     feature_extractions_dir,
                                     window_length,
                                     replacement_token,
                                     ts_kind_list,
                                     valid_ts_kinds,
                                     num_signals_per_job):

    '''
    Designed to be used with the xxxx.py file which in turn calls the xxx.sl file.

    Extracts features (not feature dynamics) on a given range of measurement ids and writes these features
    to directories. Used in the final results to extract the features for the benchmark model.

        params:
            array_task_id (int,str): the slurm job number that this script corresponds to. 
            m_ids (list): the VSB measurement IDs that features will be extracted for
            vsb_data_obj: The instance of the VSBData class to extract features on.
            first_fc_params, second_fc_params (dictionary): the feature set containing the relevant feature calculators for the extraction of (1) feature time series and (2) feature dynamics
            fc_params_is_kind (bool): if the feature dictionaries maps each separate VSB signal (ts kind) to a different feature set then this value is True, otherwise false.
            feature_extractions_dir (str): top level directory where the extracted features will be dumped
            replacement_token (str): token that replaces double unscore in feature naming convention. This adjustment is required for featue dynamics extraction
            ts_kind_list (list): the VSB signals that will be extracted.
            valid_ts_kinds (list): the correct names of all valid VSB signals.
            num_signals_per_job (int): number of VSB measurements that should be computed for a given array_task_id number
            window_length (int): not relevant because no windowing is applied to simple features.
    '''



    # mapping from array id to a range of measurement_ids...
    start = time.time()
    
    groups_of_meas_ids = [m_ids[i:i + num_signals_per_job] for i in range(0, len(m_ids), num_signals_per_job)]

    assert array_task_id < len(groups_of_meas_ids)

    assert set(ts_kind_list) - set(valid_ts_kinds) == set(), "ts_kind_list contains invalid ts_kind names"

    # array_task_id is an int that can range from 0,1,.....N-1 where N-1 is 2 * number of signals with a fault

    # mapping from the array task ID 1,2,3,...N to a range of measurement ID [x1,x2,x3...M]
    meas_ids = groups_of_meas_ids[array_task_id]
    print("Extracting features on the following IDS: {}".format(meas_ids))

    # load in one signal at a time. Function can handle multiple meas_ids but is computationally expensive
    signals = vsb_data_obj.load_signals(measurement_ids = meas_ids,
                                       window_length = window_length)

    # drop cols that aren't going to be extracted
    signals.drop(set(valid_ts_kinds) - set(ts_kind_list), inplace = True, axis = "columns")


    # get features on features and write to an output directory
    (X1, y1)  = features_on_features_vsb(ts = signals,
                                         first_fc_params = first_fc_params,
                                         second_fc_params = second_fc_params,
                                         fc_params_is_kind = fc_params_is_kind,
                                         replacement_token = replacement_token)

    # lazy way to support getting the labels if and only if the data is labelled
    try:
        X1["response"] = y1.values
    except:
        print("Hit an error or you are extracting features that are unlabelled")


    with open("feature_encoding.yaml", "r") as f:
        encoding = yaml.safe_load(f)

    # write extracted features to files in an efficient way
    for ts_kind in ts_kind_list:
        features_related_to_ts_kind = [feature for feature in X1 if feature.startswith(ts_kind)]
        assert features_related_to_ts_kind is not None


        print("ts_kind: {}".format(ts_kind))
        compressed_feature_names = []


        #compressed_feature_names = [encoding.get(feature_name.replace(ts_kind + replacement_token, "")).replace("PREFIX",ts_kind) for feature_name in features_related_to_ts_kind]
        for feature_name in features_related_to_ts_kind:
            try:
               encoded_feature_name = get_encoded_feature_name(full_feature_name = feature_name, encoding_dictionary = encoding, ts_kind = ts_kind, replacement_token = replacement_token, is_verbose = False)
            except AssertionError:
                continue # skip processing if not in the encoding...


            compressed_feature_names.append(encoded_feature_name)

        X1 =  X1.rename(columns={old_name:new_name for old_name,new_name in zip(features_related_to_ts_kind, compressed_feature_names)})
        # write the ts_kind to parquet
        feature_vector_of_given_kind = pd.DataFrame(X1, columns=compressed_feature_names + ["response"]).T
        print("writing extracted feature vector to parquet")
        print(feature_vector_of_given_kind)

        feature_vector_of_given_kind.to_parquet(path = feature_extractions_dir + "/"+ ts_kind + "/feature_vectors/num_meas_ids_{}_cluster_num_{}_ts_kind_{}_features.parquet".format(num_signals_per_job, array_task_id, ts_kind), index = True, engine = "fastparquet", compression = "snappy")
        
        print("Wrote features to path: {}".format(feature_extractions_dir + "/"+ ts_kind + "/feature_vectors/num_meas_ids_{}_cluster_num_{}_ts_kind_{}_features.parquet".format(num_signals_per_job, array_task_id, ts_kind)))

    end = time.time()
    with open("feature_extraction_log.log", "a+") as f:
        f.write("FEATURE EXTRACTION yielding N_features: {} to be dumped into the extraction directory: {} in TIME: {}\n".format(len(compressed_feature_names), feature_extractions_dir, end - start))











def minimal_model_fitting(fdr_level, features_dump_path,VSB_obj):
    ''' 
    This script does the main operations of feature selection, model training, and model testing on a minimal feature set.
    The operations could be done in this way because of the assumption that the features are minimal - no functionality 
    exists in this function for efficient features. A key advantage of this script is an effective feature selection 
    strategy is used. The cross validated estimates in this case are not biased.

       params:
           fdr_level (float): The FDR level.
           features_dump_path (str): The location of the extreacted features
           VSB_obj: The VSB data that the problem will use
    '''
    # hardcoded to test out new approach on minimal
    set_op = "intersection" # union or intersection as the feature selection strategy
    # Must know cluster largest cluster num a priori
    n_clusters = 291 

    # assemble the features and responses into one feature matrix
    start = time.time()
    dfs_all_clusters_list = []
    for cluster_num in range(n_clusters):
        paths_for_given_cluster = glob.glob(features_dump_path + "/*/feature_vectors/*cluster_num_{}_*".format(cluster_num))
        cluster_dfs_list = []
        for filename in paths_for_given_cluster:
            pf = ParquetFile(filename)
            cluster_df = pf.to_pandas()
            cluster_dfs_list.append(cluster_df)
        df_for_full_cluster = pd.concat(cluster_dfs_list,axis=0)
        df_for_full_cluster = df_for_full_cluster[~df_for_full_cluster.index.duplicated(keep='first')]
        df_for_full_cluster.drop(["response"])
        dfs_all_clusters_list.append(df_for_full_cluster)
    X = pd.concat(dfs_all_clusters_list,axis = 1).T.sort_index()     
    X.index = X.index.astype(int)
    print(X)
    X = X.dropna(axis = 1)
    print(X)
    y = VSB_obj.y.loc[X.index]
    print(y)
    assert np.all(y.index == X.index)
    X.drop(["response"],axis = 1,inplace = True)
    assert "response" not in X.columns
    

    # Get p values for informational purposes 
    selector = FeatureSelector(fdr_level = fdr_level)
    selector.fit(X, y)
    p_values = pd.DataFrame({'p_value': selector.p_values,'feature': selector.features})
    print(p_values)
     
    # Write p values to file...
    #p_values.to_csv("p_values_minimal_FDR_{}.csv".format(fdr_level))
    #p_values.to_csv("SIMPLE_p_values_minimal_FDR_{}.csv".format(fdr_level))
    #########################################################################################

    # get holdout set and training set
    X_train, X_holdout, y_train, y_holdout = train_test_split(X, y, test_size=0.15, random_state=700, stratify=y) 
    print("Len holdout set: {}. Len train set {}".format(len(X_holdout.index),len(X_train.index)))
    MCCs = []
    print("Number of faults in the test (holdout) set: {} from total observations of {}".format(sum(y_holdout),len(y_holdout)))
    print("Number of faults in the train set: {} from total observations of {}".format(sum(y_train),len(y_train)))

    # CROSS VALIDATION + FEATURE SELECTION STRATEGY
    f1s = [] 
    best_features_list_of_sets = []
    n_splits = 3 # tune this
    scoring = {'mcc' : make_scorer(matthews_corrcoef)} 
    kfold = StratifiedKFold(n_splits=n_splits, shuffle=True, random_state=700)
    for train_ix, test_ix in kfold.split(X_train, y_train):
	# select rows
        train_X, test_X = X_train.iloc[train_ix,:], X_train.iloc[test_ix,:]
        train_y, test_y = y_train.iloc[train_ix], y_train.iloc[test_ix]
        # summarize train and test composition
        train_0, train_1 = len(train_y[train_y==0]), len(train_y[train_y==1])
        test_0, test_1 = len(test_y[test_y==0]), len(test_y[test_y==1])
        print('>Train: 0=%d, 1=%d, Test: 0=%d, 1=%d' % (train_0, train_1, test_0, test_1))

        #select_features which are relevant
        selector = FeatureSelector(fdr_level = fdr_level)
        selector.fit(train_X, train_y)
        relevant_features = selector.relevant_features
        # Transform X to give only the selected features
        train_X = selector.transform(train_X)
        test_X = selector.transform(test_X)

        # MODEL TRAINING FOR CROSS VALIDATION
        # undersample on train...
        undersample = RandomUnderSampler(sampling_strategy='majority', random_state = 700)
        print(train_X)
        print(train_y)
        train_X, train_y = undersample.fit_resample(train_X, train_y)
        
        # train the model with the balanced data
        clf = lgb.LGBMClassifier(random_state = 700, objective = "binary")  
        clf.fit(train_X, train_y)
 
        # make predictions for a fold..
        y_pred = clf.predict(test_X)
        
        # evaluate with unbiased CV estimate
        print("MCC CV {}".format(matthews_corrcoef(test_y, y_pred)))
        print("F1 CV {}".format(f1_score(test_y, y_pred)))
        MCCs.append(matthews_corrcoef(test_y, y_pred))
        f1s.append(f1_score(test_y, y_pred))
        print(confusion_matrix(test_y, y_pred)) 
        
        # Store the best features for a given fold
        best_features = train_X.columns.values
        best_features_list_of_sets.append(set(best_features)) 
    
    # CV folds complete. Now eval the scores and select the final feature set
    print("CV done:")
    print("Mean f1: {}".format(np.mean(f1s)))
    print("Mean MCC: {}".format(np.mean(MCCs)))
    print("Standard error: {}".format(np.std(MCCs)/np.sqrt(n_splits)))
    robust_relevant_features = list(set.intersection(*best_features_list_of_sets)) # voting strategy is to select features that are selected in all k-folds
    all_relevant_features = list(set.union(*best_features_list_of_sets)) # voting strategy is to select all features that are selected at least once
    # it was shown that intersection is better than taking the union.

    if set_op == "union":
        relevant_features = all_relevant_features 
    elif set_op == "intersection":
        relevant_features = robust_relevant_features
    print("The number of robust features in the set intersection of features in all folds is {}".format(len(robust_relevant_features)))
    print("The number of total features in the set union of selected features in all folds is {}".format(len(all_relevant_features)))


    # now train the final model on all train data with those features    
    undersample = RandomUnderSampler(sampling_strategy='majority', random_state = 700)
    X_train, y_train = undersample.fit_resample(X_train, y_train) 
    clf_final = lgb.LGBMClassifier(random_state = 700, objective = 'binary')
    clf_final.fit(X_train[relevant_features],y_train)

    # now test final model on holdout set
    y_pred = clf_final.predict(X_holdout[relevant_features])        
    y_prob_predict = clf_final.predict_proba(X_holdout[relevant_features])[:,1]
    prediction_df = pd.DataFrame({"prediction_prob":y_prob_predict,"prediction":y_pred,"actual":y_holdout})
  

  
    # write predictions to file. This includes probabilities 

    #prediction_df.to_csv("SCOTTS_ROC_DATA_TEST.csv")
    #prediction_df.to_csv("SIMPLE_ROC_DATA_TEST.csv")
    
    # write to files for vis purposes

    #X_holdout.to_csv("SCOTTS_TEST_MATRIX.csv")
    #X_train.to_csv("SCOTTS_TRAIN_MATRIX.csv")
    #y_holdout.to_csv("SCOTTS_TEST_Y.csv")
    #y_train.to_csv("SCOTTS_TRAIN_Y.csv")
    #X_holdout.to_csv("SIMPLE_TEST_MATRIX.csv")
    #X_train.to_csv("SIMPLE_TRAIN_MATRIX.csv")
    #y_holdout.to_csv("SIMPLE_TEST_Y.csv")
    #y_train.to_csv("SIMPLE_TRAIN_Y.csv")


    # evaluate the MCC out of sample results
    print("MCC OOS {}".format(matthews_corrcoef(y_holdout, y_pred)))
    print("F1 OOS {}".format(f1_score(y_holdout, y_pred)))
    stop = time.time()
    print("NUM FEATURES IN TEST: {}".format(len(X_holdout.columns)))
    print("NUM FEATURES IN TRAIN (after undersampling): {}".format(len(X_train.columns)))
    print("NUM OBSERVATIONS IN TRAIN: {}".format(len(X_train.index)))
    print("NUM OBSERVATIONS IN TEST: {}".format(len(X_holdout.index)))

    print("TIME: {}".format(stop - start))
    model_save_path = "Models_SIMPLE/SIMPLE_fitted_model_MINIMAL_FDR_{}_NUM_FEATURES_{}_MCC_SCORE_{}_NFOLDS_{}_{}_TIME_{}.joblib".format(fdr_level, len(relevant_features), matthews_corrcoef(y_holdout, y_pred),n_splits,set_op,datetime.now()) if features_dump_path == "SIMPLE_all_features_extractions_dump_MINIMAL" else "Models_final_filter/fitted_model_MINIMAL_FDR_{}_NUM_FEATURES_{}_MCC_SCORE_{}_NFOLDS_{}_{}_TIME_{}.joblib".format(fdr_level, len(relevant_features), matthews_corrcoef(y_holdout, y_pred),n_splits,set_op, datetime.now()) 

    # now save model
    joblib.dump(clf_final, model_save_path)

    print("SAVED MODEL TO {}".format(model_save_path))
    

    # get feature importances  
    print("Feature importance")
    feature_imp = pd.DataFrame(sorted(zip(clf_final.feature_importances_,clf_final.feature_name_), reverse = True), columns=['Value','Feature'])
    print(feature_imp)
    feature_imp.to_csv("feature_importances_FDR_{}_MINIMAL_num_features_{}_MCC_{}.csv".format(fdr_level, len(feature_imp.index), matthews_corrcoef(y_holdout, y_pred)))
    

    
def minimal_model_interpretation(model_path, output_file_name):
    # p values, feature importance, etc
    clf_final = joblib.load(model_path)
    
    # f importances
    feature_imp = pd.DataFrame(sorted(zip(clf_final.feature_importances_,clf_final.feature_name_), reverse = True), columns=['Value','Feature'])
    feature_imp.to_csv(output_file_name)
    print("success")

    # p values final model has an FDR of 0.3 so load that model in




def get_encoded_feature_name(full_feature_name, encoding_dictionary, ts_kind, replacement_token, is_verbose):
    ''' 
    Maps a full feature name to an encoded feature name, based on an encoding dictionary
        
        params:
            full_feature_name (str): the feature name input
            encoding_dictionary (dict): the encoding dictionary
            ts_kind (str): the VSB signal that the feature correponds to
            replacement_token (str):  token that replaces double unscore in feature naming convention. This adjustment is required for featue dynamics extraction
            is_verbose (bool): flag indicating how much should be printed to stdout

       returns:
            str: the encoded feature name output
    '''
    # takes in a feature name and converts it to the encoding. Also
    if is_verbose:
         print("feature_name: {}".format(full_feature_name))
         print("feature_name after replacing: {}".format(full_feature_name.replace(ts_kind, "")))
         print("Get the encoding version: {}".format(encoding_dictionary.get(full_feature_name.replace(ts_kind, ""))))

    # feature name may be dropped when first generating the dictionary on a signal but not dropped when reading THIS PARTICULAR signal... so try see if it exists in the dictionary
    try:
        encoding_feature_name = encoding_dictionary.get(full_feature_name.replace(ts_kind, ""))
        assert encoding_feature_name is not None
    except AssertionError:
        print("ERROR: {} not found in the encoding dictionary.".format(full_feature_name.replace(ts_kind,"")))
        raise AssertionError # skip processing if not in the encoding...

    if is_verbose: print("now we replace PREFIX with {} to get the following: {}\n\n\n".format(ts_kind, encoding_feature_name.replace("PREFIX",ts_kind)))

    return encoding_feature_name.replace("PREFIX",ts_kind)


def gen_feature_names_encoding(feature_names, replacement_token, ts_kind_list):
    ''' 
    This is the procedure which is called to generate the encoding dictionary. Run once only to generate the dictionary, after which it should never be run again.

       params:
           feature_names (list): the list of feature names to generate compressed names for
           replacement_token (str):  token that replaces double unscore in feature naming convention. This adjustment is required for featue dynamics extraction
           ts_kind_list (list): the VSB signals that will be extracted i.e. ts_kinds
    '''
    # takes in a list of column names and returns a compressed form of column names
    # does not include prefixes...
    # just use phase1 features only...
    ts_kind = ts_kind_list[0]

    unique_features = [feature for feature in feature_names if feature.startswith(ts_kind)]


    feature_names_without_prefixes = [feature_name.replace(ts_kind, '', 1).strip() for feature_name in unique_features]

    compressed_names = []
    for idx, feature_name in enumerate(feature_names_without_prefixes):
        compressed_name = "PREFIX{}f_{}".format(replacement_token, idx)
        compressed_names.append(compressed_name)

    feature_encoding = dict(zip(feature_names_without_prefixes, compressed_names))


    with open("feature_encoding.yaml", 'w') as f:
        yaml.dump(feature_encoding, f, default_flow_style=False)




def display_fitted_model_info(model_path):
    clf = joblib.load(model_path)
    print("NUM FEATURES: {}".format(clf.n_features_))


    # convert encoding to actual interpretation for top 10 features:
    with open("feature_encoding.yaml", "r") as f:
        encoding = yaml.load(f)


    df = pd.DataFrame(clf.feature_name_, index =clf.feature_importances_, columns = ["Feature name"])
    print(df)

    print("Get top 5 features:")
    df.sort_index(axis=0,ascending=False, inplace=True)
    print(df.head(5))
    top_five = df.head(5)

    print("counts")    
    print(df.count(axis=0))

    f_names_full = []
    for f_name in top_five["Feature name"].tolist():
        prefix = f_name.split("||")[0]
        f_name_without_prefix_encoding = f_name.replace(prefix + "||", "")

        f_name_without_prefix = [full_name_no_prefix for full_name_no_prefix, encoded_name_without_prefix in encoding.items() if encoded_name_without_prefix.replace("PREFIX||","") == f_name_without_prefix_encoding]

        f_names_full.append(f_name.split("||")[0] +  f_name_without_prefix[0])
 

    top_five["full_names"] = f_names_full
    print("TOP FIVE FEATURES")
    print(top_five)
    print(top_five["full_names"])


   

def plot_importance(clf_path, fdr_level):
    print("loading model")    
    clf = joblib.load(clf_path)
    print("building feature importance df")
    
    feature_imp = pd.DataFrame(sorted(zip(clf.feature_importances_,clf.feature_name_)), columns=['Value','Feature'])
    
    print("Only looking at the top 200 features")
    feature_imp = feature_imp.head(200)
    print("plotting")
    sns.barplot(x="Value", y="Feature", data=feature_imp.sort_values(by="Value", ascending=False))
    plt.title('LightGBM feature relevance for model {}'.format(fdr_level))
    plt.tight_layout()
    plt.savefig('lgbm_importances__FDR_{}.png'.format(fdr_level))
    plt.show()



































































































































######################### DEPRECATED THINGS. Leaving code here in case it will be adapted in the future ############################
def script_feature_selection_combined(feature_selection_target_dir, ts_kind_list, replacement_token,features_dict_key, fdr_level, valid_ts_kinds, feature_extractions_dump, path_to_relevant_feature_dictionaries):
    '''
    DEPRECATED - feature selection based on p-values for efficient and minimal features.
    '''
    start = time.time()
    assert set(ts_kind_list) - set(valid_ts_kinds) == set(), "ts_kind_list contains invalid ts_kind names"

    print("Feature selection for an FDR level of: {} on features dictionary: {}".format(fdr_level,features_dict_key))
    #### get the p values ####

    # List of lists. Each element of list is a list of p value paths for a given ts_kind
    print("reading P-values in...")
    list_of_p_val_paths = glob.glob(feature_extractions_dump + "/*/p_values/*p_values.parquet")
    p_values_df = []
    for filename in list_of_p_val_paths:
            pf = ParquetFile(filename)
            p_values_df_subset = pf.to_pandas() # maybe do this column by column...
            p_values_df.append(p_values_df_subset)

    print("P-Values have been loaded into list. Concatenating list") 
    features_and_p_values = pd.concat(p_values_df)
    print(features_and_p_values.sort_values(by = ["p_value"]))

    print("This many features have p values that are less than 0.05")
    print(features_and_p_values[features_and_p_values["p_value"] < 0.05].shape)
    print("This many features and p vals are there in total")
    print(features_and_p_values.shape)

    #### compute the p value corrections using by procedure ####

    # drop the features associated with NaN p values... this is TSfresh's way of classifying features that are constant acorss all observations i.e. length of the timeseries
    features_and_p_values.dropna(inplace = True)
    print("Performing multipletests")
    info_obj = multipletests(pvals=features_and_p_values["p_value"].values, alpha=fdr_level, method="fdr_by")
    is_selected = info_obj[0]
    print(is_selected)
    features_and_p_values["p_values_corrected"] = info_obj[1]
    features_and_p_values["is_relevant"] = is_selected 
    features_and_p_values.to_csv("p_values_total_{}_FDR_{}.csv".format(features_dict_key,fdr_level))
    # filter for the relevant feature names
    relevant_features_info = features_and_p_values[is_selected]
    print(relevant_features_info)
    relevant_feature_names = relevant_features_info["feature"].tolist()

    # write the relevant feature dictionaries to file
    print("writing the feature relevant dictionaries to this path {}".format(path_to_relevant_feature_dictionaries))
    vsb_utils.derive_features_dictionaries(filtered_features = relevant_feature_names, dicts_from = features_dict_key, replacement_token = replacement_token, fdr_level = fdr_level,path_to_relevant_feature_dictionaries = path_to_relevant_feature_dictionaries)

    # write the feature names to a file
    with open("relevant_feature_names_{}_FDR_{}.yaml".format(features_dict_key,fdr_level), "w") as f:
        yaml.dump(relevant_feature_names, f)

    relevant_feature_matrix = read_distributed_features_into_pandas(features_dump_path = feature_extractions_dump, ts_kind_list = ts_kind_list, feature_names = relevant_feature_names + ["response"])

    print("relevant feature matrix is here:")
    print(relevant_feature_matrix)
    # write the relevant feature matrix to file
    relevant_feature_matrix.T.to_parquet(path = feature_selection_target_dir + "FDR_" + str(fdr_level) +  "_selected_features.parquet", index = True, engine = "fastparquet", compression = "snappy")
    print("written selected feature matrix to file and saved feature dictionaries")
    end = time.time()
    with open("feature_selection_log.log", "a+") as f:
        f.write("FEATURE SELECTION WITH FDR: {} yielding N_features: {} from the extraction directory: {} in TIME: {}\n".format(fdr_level, len(relevant_feature_names), feature_extractions_dump, end - start))



def simple_features_end_to_end_script(fdr_level, vsb_data_obj, features_dictionary_key):
    '''DEPRECATED'''
    print("Running simple features full workflow")

    # Get the feature dictionary
    feature_dictionary, _,_ = vsb_utils.features_dict_selector(key = features_dictionary_key, path_to_relevant_feature_dictionaries = None, fdr_level = fdr_level)

    ##################################################################################################
    # Get the signal ids for train and test, also get the response for train and test
    window_length = 800
    train_ids = vsb_data_obj.mids_train
    test_ids = vsb_data_obj.mids_test

    print("Getting the train and test signals")
    signals_train = vsb_data_obj.load_signals(measurement_ids = train_ids,window_length = window_length)
    signals_test = vsb_data_obj.load_signals(measurement_ids = test_ids, window_length = window_length)

    # drop cols that aren't going to be extracted
    signals_train.drop(set(valid_ts_kinds) - set(ts_kind_list), inplace = True, axis = "columns")
    signals_test.drop(set(valid_ts_kinds) - set(ts_kind_list), inplace = True, axis = "columns")

    ##################################################################################################
    # extract features on the train data
    print("Extracting first set of features")
    X_train = extract_features(signals_train, column_id = "column_id",column_sort = "time_index", default_fc_parameters = feature_dictionary, disable_progressbar = True)
    y_train = vsb_data_obj.y[train_ids]
    print("Selecting features")
    selector = FeatureSelector()
    selector.fit(X_train, y_train)
    ##################################################################################################
    # extract features on the test data
    X_test = extract_features(signals_train, column_id = "column_id",column_sort = "time_index", kind_to_fc_parameters = feature_dictionary, disable_progressbar = True) 
    X_test = selector.transform(X_train)
    y_test = vsb_data_obj.y[test_ids]
    #####################################################################################################
    # Train the model, in sample testing on the model, out of sample testing on the model


    scoring = {'accuracy' : make_scorer(accuracy_score),
               'precision' : make_scorer(precision_score),
               'recall' : make_scorer(recall_score),
               'f1_score' : make_scorer(f1_score),
               'mcc' : make_scorer(matthews_corrcoef)}


    # model training and in sample testing
    clf = LGBMClassifier(objective = "binary")
    cv = RepeatedStratifiedKFold()
    scores = cross_validate(clf, X_train, y_train, scoring=scoring, cv=cv, n_jobs=-1)
    clf.fit(X_train, y_train)
    for item in scores:
        print("CV Metric: Mean {} is: {}".format(item, sum(scores[item]/len(scores[item]))))

    y_pred = clf.predict(X_train)

    print(confusion_matrix(y_train, y_pred))

    # test model
    y_pred = clf.predict(X_test)

    print("MCC score out of sample {}".format(matthews_corrcoef(y_test, y_pred)))
    print("F1 score out of sample {}".format(f1_score(y_test, y_pred)))

    print(confusion_matrix(y_test, y_pred))


    # write model to path 


###################################################################################################









def get_test_train_X_y(features_dump_path_balanced, fdr_level,features_dump_path_remaining, ts_kind_list, features_dict_key):
    ''' DEPRECATED '''
    # gets X_train, X_test, y_train, y_test
    pf_balanced = ParquetFile(features_dump_path_balanced + "FDR_{}_selected_features.parquet".format(fdr_level))
    X_train = pf_balanced.to_pandas().T
    assert X_train.isnull().values.any() == False
    
    with open("relevant_feature_names_{}_FDR_{}.yaml".format(features_dict_key.replace("RELEVANT_FROM_",""),fdr_level), "r") as f:
        feature_names = yaml.load(f)

    X_test = read_distributed_features_into_pandas(features_dump_path = features_dump_path_remaining, ts_kind_list = ts_kind_list, feature_names = feature_names + ["response"])
    assert X_test.isnull().values.any() == False
    print("in get_test_train_X_y, X_test has been read in as: {}".format(X_test))
    X_train.sort_index(axis=1, inplace=True) # sorting columns alphanumerically... 
    X_test.sort_index(axis=1, inplace=True) # sorting columns alphanumerically... 
    y_train = X_train["response"]
    y_test = X_test["response"]
    X_train.drop(["response"], axis = "columns", inplace = True)
    X_test.drop(["response"], axis = "columns", inplace = True)

    return X_train, X_test, y_train, y_test

def read_distributed_features_into_pandas(features_dump_path, ts_kind_list, feature_names, is_labelled = True):
    ''' DEPRECATED '''
    # reliable function to read in features that are distributed acorss multiple directories.. 
    if is_labelled == True: assert "response" in feature_names
    print("Getting DFs from path: {}".format(features_dump_path))
    nested_list_of_paths_for_each_ts_kind = [glob.glob(features_dump_path + "/" + ts_kind + "/feature_vectors/*.parquet") for ts_kind in ts_kind_list]
    list_of_dfs_to_ver_concat = []
    for paths_for_given_ts_kind in nested_list_of_paths_for_each_ts_kind:
        list_of_dfs_to_hor_concat = []
        for filename in paths_for_given_ts_kind: # files in this loop have unique mids.... and common fnames 
            pf = ParquetFile(filename)
            mini_df = pf.to_pandas()
            print(mini_df)
            if mini_df.empty or mini_df is None: print("{} NOT FOUND....".format(filename))
            list_of_dfs_to_hor_concat.append(mini_df)
        mini_df_all_mids = pd.concat(list_of_dfs_to_hor_concat, axis = "columns", join = "inner") # horizontal concatenation of MIDS 
        list_of_dfs_to_ver_concat.append(mini_df_all_mids)
    feature_matrix = pd.concat(list_of_dfs_to_ver_concat, axis =0, join = "inner").T

    selected_columns = feature_matrix[feature_matrix.columns.intersection(feature_names)]
    print("in read_distributed_features_into_pandas() selected columns are: {}".format(selected_columns))
    if is_labelled == True:
         response = feature_matrix["response"]
         feature_matrix = pd.concat([response, selected_columns], axis = "columns")
    
    feature_matrix = feature_matrix.loc[:,~feature_matrix.columns.duplicated()]
    feature_matrix.sort_index(axis=1, inplace=True) # sorting columns alphanumerically... 
    print("In read_distributed_features_into_pandas() the return value is:{}".format(feature_matrix))
    return feature_matrix


def get_num_of_fits(fdr):
    ''' DEPRECATED '''
    filename="model_fitting_fdr_{}_log.log".format(fdr)
    with open(filename, "a+") as f:
        f.seek(0)
        val = int(f.read() or 0) + 1
        f.seek(0)
        f.truncate()
        f.write(str(val))
        return val

def test_out_of_sample(fdr_level,extracted_features_dump_path,ts_kind_list, features_dict_key):
    ''' DEPCRECATED '''
    # import the model

    # grab a bunch of remaining features

    # chuck a few faults in there that ideally have not been seen in training

    # split response and feature matrix

    # predict

    # get accuracy score

    scoring = {'accuracy' : make_scorer(accuracy_score),
               'precision' : make_scorer(precision_score),
               'recall' : make_scorer(recall_score),
               'f1_score' : make_scorer(f1_score),
               'mcc' : make_scorer(matthews_corrcoef)}

    print("Extracted features dump: {}".format(extracted_features_dump_path))

    # using the relevant feature names as keys, get the relevant feature matrix from the extracted features
    nested_list_of_paths_for_each_ts_kind = [glob.glob(extracted_features_dump_path + "/" + ts_kind + "/feature_vectors/*.parquet") for ts_kind in ts_kind_list]
    list_of_dfs_to_ver_concat = []
    for paths_for_given_ts_kind in nested_list_of_paths_for_each_ts_kind:
        list_of_dfs_to_hor_concat = []
        for filename in paths_for_given_ts_kind: # files in this loop have unique mids....
            pf = ParquetFile(filename)
            mini_df = pf.to_pandas()
            list_of_dfs_to_hor_concat.append(mini_df)

        mini_df_all_mids = pd.concat(list_of_dfs_to_hor_concat, axis = "columns", join = "inner")
        list_of_dfs_to_ver_concat.append(mini_df_all_mids)

    test_feature_matrix = pd.concat(list_of_dfs_to_ver_concat, join = "inner").T

    # now drop stuff that might not be in there for a given FDR level...
    with open("relevant_feature_names_{}_FDR_{}.yaml".format(features_dict_key.replace("RELEVANT_FROM_",""),fdr_level), "r") as f:
        feature_names = yaml.load(f)


    print("test feature matrix before swatting away features if the current FDR is less than the max FDR {}".format(test_feature_matrix))
    test_feature_matrix = test_feature_matrix[test_feature_matrix.columns.intersection(feature_names + ["response"])]


    print("test feature matrix after swatting away features if current fdr less than max fdr {}".format(test_feature_matrix))


    test_feature_matrix = test_feature_matrix.loc[:,~test_feature_matrix.columns.duplicated()]
    response = test_feature_matrix["response"].astype(int)
    print("NUmber of faults: {}".format(sum(response)))
    print("here is the response vector {}".format(response))

    test_feature_matrix = test_feature_matrix.drop(labels = ["response"], axis = "columns")

    clf = joblib.load("LOCAL_fitted_model_{}_FDR_{}.joblib".format(features_dict_key.replace("RELEVANT_FROM_",""), fdr_level))
    test_feature_matrix = test_feature_matrix[clf.feature_name_]
    test_feature_matrix = test_feature_matrix.reindex(columns=clf.feature_name_)
    print(test_feature_matrix)
    print("here is the test feature matrix after swatting away response columns {}".format(test_feature_matrix))
    assert test_feature_matrix.isnull().values.any() == False


    # now test the fitted model
    print(clf.feature_name_)
    y_pred = clf.predict(X = test_feature_matrix)


    print("Now doing test results on unseen data:")


    print("MCC score out of sample {}".format(matthews_corrcoef(response, y_pred)))
    print("F1 score out of sample {}".format(f1_score(response, y_pred)))





    #with open("logging.log", "a") as f:

    #    for metric_name, scorer in scoring.items():
    #        score = scorer(pipeline, test_feature_matrix, response)
    #        print("TEST {} score: {}".format(metric_name, score))
    #        f.write("TEST {} score: {} on {} observations".format(metric_name, score, len(response)))

def read_feature_matrix_for_given_feature_timeseries(feature_time_series_name,ts_kind,extracted_features_paths, encoding, dictionary_key):
    ''' DEPRECATED ''' 
    # get the feature names that correspond to the specified feature time series...
    print("looking for features starting with {}".format(feature_time_series_name))
    relevant_encoded_names = [encoded_name.replace("PREFIX",ts_kind) for actual_name,encoded_name in encoding.items() if actual_name.startswith(feature_time_series_name + "__")]
    things_to_ver_concat = []
    # read in one parquet file at a time
    for filename in extracted_features_paths:
        pf = ParquetFile(filename)
        features_df = pf.to_pandas()
        features_df = features_df.T
        # update the feature names to ensure that they all work fine...
        with open("p_value_extraction_errors_{}.txt".format(dictionary_key), "r") as f:
           banned_feature_names = [line.rstrip() for line in f]

        relevant_encoded_names = list(set(relevant_encoded_names) - set(banned_feature_names))
        things_to_hor_concat = []
        # get response(s)
        response_vec = pd.to_numeric(features_df["response"], downcast = "integer")
        # read in one feature at a time for a given pq file
        for feature in relevant_encoded_names:
            # try read in a feature. If this doesnt work, record the failure.
            try:
                feature_values = features_df[feature]
                things_to_hor_concat.append(feature_values)
            except Exception as e:
                print(e)
                print("error reading feature from parquet. Feature name: {}".format(feature))
                with open("p_value_extraction_errors_{}.txt".format(dictionary_key), "a") as f:
                   f.write(feature + "\n")
        # combine each feature for a given pq file into a df
        feature_time_series_matrix = pd.concat(things_to_hor_concat + [response_vec], axis = "columns")
        things_to_ver_concat.append(feature_time_series_matrix)
    # combine pq files into a df
    feature_matrix_for_given_fts = pd.concat(things_to_ver_concat)
    if feature_matrix_for_given_fts.shape[1] == 1:
        print("{} not found in the extracted feature dump. Exiting program".format(feature_time_series_name))
        sys.exit(0)
    return feature_matrix_for_given_fts





def kaggle_submission(X_kaggle_test, y_kaggle_test, fdr_level, features_dict_key, X_test, y_test, main_model):
    ''' DEPRECATED '''

    # repeat OOS scores
    fake_pred = main_model.predict(X = X_test)
    print("MCC score out of sample NONKAGGLE {}".format(matthews_corrcoef(y_test, fake_pred)))
    print("F1 score out of sample NONKAGGLE {}".format(f1_score(y_test, fake_pred)))
    print(confusion_matrix(y_test, fake_pred))
    print("Number of faults in the fake prediction: {}".format(np.sum(fake_pred)))

    # actual scores
    test_prediction = main_model.predict(X = X_kaggle_test).astype(int)
    print("Made a set of predictions")
    print("Number of predicted faults: {}".format(sum(test_prediction)))


    # load in our models for a given FDR
    #predictions = []
    #model_paths = glob.glob("fitted_model_{}_FDR_{}_NUM_*.joblib".format(features_dict_key,fdr_level))
    #print("HERE ARE THE PATHS: {}".format(model_paths))
    #for model_path in model_paths:
    #    lgbm_model = joblib.load(model_path)



        # doing fake predictions
    #    fake_pred = lgbm_model.predict(X = X_test)
    #    print("MCC score out of sample NONKAGGLE {}".format(matthews_corrcoef(y_test, fake_pred)))
    #    print("F1 score out of sample NONKAGGLE {}".format(f1_score(y_test, fake_pred)))
    #    print(confusion_matrix(y_test, fake_pred))
    #    print("Number of faults in the fake prediction: {}".format(np.sum(fake_pred)))


        # now test the fitted model on the kaggle set
    #    test_prediction = lgbm_model.predict(X = X_kaggle_test)
    #    predictions.append(test_prediction)
    #    print("Made a set of predictions")
    #    print("Number of predicted faults: {}".format(sum(test_prediction)))
    #    break # just fit one model for now until I am confitdent in column orderings...
    
    #print("Several Predictions made: converting to array")
    #predictions = np.array(predictions)
    #print(predictions)
    #print("Taking the average prediction")
    #test_prediction = np.mean(predictions, axis = 0)
    #print(test_prediction)
    #print("rounding the predictions up always")
    #test_prediction = np.ceil(test_prediction).astype(int)
    #print(test_prediction)

    print("Submitting to kaggle")
    submission = pd.DataFrame({'measurement_id': X_kaggle_test.index, 'target': test_prediction})
    meas_ids = submission["measurement_id"].astype(int)

    print("Mapping predictions for measurement ID to predictions for signal ID")
    signal_ids = [item for sublist in list(map(lambda meas_id : [3*int(meas_id), 3*int(meas_id) + 1, 3*int(meas_id) + 2], submission["measurement_id"].tolist())) for item in sublist]

    submission = pd.DataFrame(np.repeat(submission.values, 3, axis=0), columns=submission.columns)
    submission["signal_id"] = signal_ids
    submission.drop("measurement_id", axis = "columns", inplace = True)
    # reverse columns
    submission = submission.iloc[:, ::-1].sort_values("signal_id")
    submission.to_csv('FDR_{}_submission_{}_NUM_FEATURES_{}.csv'.format(fdr_level, features_dict_key,len(X_kaggle_test.columns)), index=False)
    print(submission)
    print("sucessfully wrote submission to csv")









def script_model_fitting(fdr_level, relevant_balanced_feature_matrix_path, remaining_features_dump, ts_kind_list, replacement_token, features_dict_key):
    '''DEPRECATED '''
    # fits model, evaluates model...
    scoring = {'mcc' : make_scorer(matthews_corrcoef), "precision":make_scorer(precision_score),"recall": make_scorer(recall_score), "f1_score":make_scorer(f1_score)}

#################################################################################################################
#################################################################################################################
#################################################################################################################
    print("remaining features dump: {}".format(remaining_features_dump))
    # getting X_train, X_test, y_train, y_test 
    X_train, X_test, y_train, y_test = get_test_train_X_y(features_dump_path_balanced = relevant_balanced_feature_matrix_path, fdr_level = fdr_level,features_dump_path_remaining = remaining_features_dump, ts_kind_list = ts_kind_list, features_dict_key = features_dict_key)
    
    assert np.alltrue(X_train.index == y_train.index)
    assert np.alltrue(X_test.index == y_test.index)
    
    print("STAGE ONE: Getting X_train, X_test, y_train, y_test")
    print("X train: {}".format(X_train)) 
    print("X_test: {}".format(X_test))
    print("y train: {}".format(y_train))
    print("y_test: {}".format(y_test))
#################################################################################################################
#################################################################################################################
#################################################################################################################
    # get kaggle features X_kaggle_test
    #print("READING KAGGLE FEATURES")
    #kaggle_test_features_dump_path = remaining_features_dump.replace("remaining_features_extractions_dump_{}".format(features_dict_key), "testing_data_extractions_dump_{}".format(features_dict_key))
    
    with open("relevant_feature_names_{}_FDR_{}.yaml".format(features_dict_key.replace("RELEVANT_FROM_",""),fdr_level), "r") as f:
        feature_names = yaml.load(f)

    #X_kaggle_test = read_distributed_features_into_pandas(features_dump_path = kaggle_test_features_dump_path, ts_kind_list = ts_kind_list, feature_names = feature_names, is_labelled = False)
    #y_kaggle_test = X_kaggle_test["response"]
    #X_kaggle_test = X_kaggle_test.drop(labels = ["response"], axis = "columns")
    #assert X_kaggle_test.isnull().values.any() == False

#################################################################################################################
#################################################################################################################
#################################################################################################################
    # features are dropped from extracting features on test, train, remaining subsets. These could be different. Ideally the encodings should eliminate the differences but this is not always happening. Therefore we must take the intersection of all features to ensure consistency before model fitting, testing, and training.
    common_features_list = [set(X_train.columns), set(X_test.columns)]
    common_features = list(set.intersection(*common_features_list))
    X_train = X_train[common_features]
    X_test = X_test[common_features]
    print("STAGE TWO: Printing reduced dataframes:")
    print("X_train reduced: {}".format(X_train))
    print("X_test reduced: {}".format(X_test))
    print("y_test reduced: {}".format(y_test))
    print("y_train reduced: {}".format(y_train))
#################################################################################################################
#################################################################################################################
#################################################################################################################
    print("Number of classes")
    print("train")
    print(sorted(Counter(y_train).items()))
    print("test")
    print(sorted(Counter(y_test).items()))
#################################################################################################################
#################################################################################################################
#################################################################################################################
    print("STAGE THREE: RECURSIVE FEATURE ELIMINATION")
    # fit the model and evaluate...
    kfold = StratifiedKFold(n_splits = 5)
    rfecv = RFECV(estimator=LGBMClassifier(objective='binary'), step=1, cv=kfold, scoring=make_scorer(matthews_corrcoef))
    rfecv.fit(X_train, y_train)

    # TEMP FOR NOW. UNCOMMENT ABOVE CODE WHEN WANTING TO DO CV....
    optimal_num_features = rfecv.n_features_
    print('Optimal number of features: {}'.format(optimal_num_features))

    rfe = RFE(estimator=LGBMClassifier(objective = 'binary'), n_features_to_select=optimal_num_features, step = 1)
    
    #model = LGBMClassifier(objective = "binary")
    #pipeline = Pipeline(steps=[('s',rfe),('m',model)])
    

    _ = rfe.fit(X_train, y_train)
    X_train = X_train.loc[:,rfe.support_]
    X_test = X_test.loc[:,rfe.support_]
    print("X_train after RFE transform: {}".format(X_train)) 
    print("X_test after RFE transform: {}".format(X_test)) 
    print("y_train after RFE transform: {}".format(y_train)) 
    print("y_test after RFE transform: {}".format(y_test)) 
#################################################################################################################
#################################################################################################################
#################################################################################################################
    #print(X_kaggle_test)
    #X_kaggle_test = X_kaggle_test.loc[:,rfe.support_]
    #print(rfe.ranking_) # do this stuff later
    # fit model
    #pipeline.fit(feature_matrix, response)

    #print("Number of features in fitted model: {}".format(pipeline.named_steps["m"].feature_name_))

    # predict on insample:
    #pipeline.predict(feature_matrix)
    print("STAGE FOUR: CV the classifier...")
    clf_final = lgb.LGBMClassifier(random_state = 700, objective = "binary") 
    cv = RepeatedStratifiedKFold(n_splits=5, n_repeats=5, random_state=700)
    scores = cross_validate(clf_final, X_train, y_train, scoring=scoring, cv=cv, n_jobs=-1)
    clf_final.fit(X_train, y_train)
    
    print("In sample prediction:")
    y_pred = clf_final.predict(X_train)
    for item in scores:
        print("CV Metric: Mean {} is: {}".format(item, sum(scores[item]/len(scores[item]))))
    print("Confusion matrix for in sample prediction:")
    print(confusion_matrix(y_train, y_pred))

    #for metric_name, scorer in scoring.items():
    #    score = scorer(pipeline, feature_matrix, response)
    #    print("{} score: {}".format(metric_name, score)) # save the model

#################################################################################################################
#################################################################################################################
#################################################################################################################
    print("STAGE FIVE: Feature importance")
    with open("relevant_feature_names_{}_FDR_{}.yaml".format(features_dict_key.replace("RELEVANT_FROM_",""), fdr_level), "r") as f:
        f_names = yaml.load(f)

    with open("feature_encoding.yaml", "r") as f:
        encoding = yaml.load(f)

    feature_imp = pd.DataFrame(sorted(zip(clf_final.feature_importances_,clf_final.feature_name_), reverse = True), columns=['Value','Feature'])

    print("Feature importance")
    print(feature_imp)
    feature_imp.to_csv("feature_importances_FDR_{}_{}_num_features_{}.csv".format(fdr_level, features_dict_key, len(feature_imp.index)))

#################################################################################################################
#################################################################################################################
#################################################################################################################
    #feature_imp_col_nums = [int(fullname.replace("Column_", "")) for fullname in feature_imp.sort_values(by = "Value", ascending = False)["Feature"]]

    #f_names = [f_names[i] for i in feature_imp_col_nums]

    #f_names_full = []
    #for f_name in f_names:
    #    prefix = f_name.split("||")[0]
    #    f_name_without_prefix_encoding = f_name.replace(prefix + "||", "")

     #   f_name_without_prefix = [full_name_no_prefix for full_name_no_prefix, encoded_name_without_prefix in encoding.items() if encoded_name_without_prefix.replace("PREFIX||","") == f_name_without_prefix_encoding]

     #   f_names_full.append(f_name.split("||")[0] +  f_name_without_prefix[0])

    print("STAGE SIX: OUT OF SAMPLE RESULTS")
    # Get out of sample test metrics too...
    print("Out of sample")
    y_pred = clf_final.predict(X_test)

    print("MCC score out of sample {}".format(matthews_corrcoef(y_test, y_pred)))
    print("F1 score out of sample {}".format(f1_score(y_test, y_pred)))

    print(confusion_matrix(y_test, y_pred))

    y_prob_predict = clf_final.predict_proba(X_test)[:,1] # prob of a fault

#################################################################################################################
#################################################################################################################
#################################################################################################################
    print("STAGE SEVEN: SAVING MODEL")
    print("WRITING PREDICTED OUTPUT TO FILE")
    oos_results = pd.DataFrame({'predicted prob':y_prob_predict, 'predicted value': y_pred, 'actual': y_test.tolist()}, columns=['predicted prob','predicted value', 'actual'])
    oos_results.to_csv("out_of_sample_results_FDR_{}_NUM_FEATURES_{}_DICT_{}.csv".format(fdr_level,len(feature_imp.index),features_dict_key),index = True)
    print("SUCCESS")
    print("WRITING MODEL TO FILE")
    with open("model_fitting_and_eval_log.log", "a+") as f:
        f.write("Model fitted with FDR: {}, number of features: {} from feature dictionary: {}, giving out of sample MCC score: {}".format(fdr_level,len(feature_imp.index),features_dict_key,matthews_corrcoef(y_test,y_pred)))

    model_save_path = "fitted_model_{}_FDR_{}_NUM_FEATURES_{}_MCC_SCORE_{}_TIME_{}.joblib".format(features_dict_key, fdr_level, len(feature_imp.index), matthews_corrcoef(y_test,y_pred), datetime.now())

    joblib.dump(clf_final, model_save_path)

    print("SAVED MODEL TO {}".format(model_save_path))

    # now do actual test stuff which will be sent to kaggle...
    #print("testing models on kaggle")
    #kaggle_submission(X_kaggle_test = X_kaggle_test, y_kaggle_test = y_kaggle_test, fdr_level = fdr_level, features_dict_key = features_dict_key, X_test = X_test, y_test = y_test, main_model = clf_final)
    print("model tested sucessfully")



def script_p_value_extraction_parallel(array_task_id, ts_kind_list, feature_extractions_dump, replacement_token, features_dict_key, fdr_level, valid_ts_kinds):
    '''
    DEPRECATED - parallised extraction of p-values
    '''
    start = time.time()
    assert set(ts_kind_list) - set(valid_ts_kinds) == set(), "ts_kind_list contains invalid ts_kind names"

    with open("feature_ts_names.txt", "r") as f:
        feature_ts_names = [line.rstrip() for line in f]

    if features_dict_key == "MINIMAL":
        feature_ts_names = [replacement_token + minimal_name for minimal_name in MinimalFCParameters() if replacement_token + minimal_name in feature_ts_names]
    print(feature_ts_names)


    with open("feature_encoding.yaml", "r") as f:
        encoding = yaml.safe_load(f)


    # map array task to a feature timeseries
    assert array_task_id < len(feature_ts_names)
    feature_ts_without_prefix = feature_ts_names[array_task_id]

    # get the directory where all of the extracted feature vectors for the given ts_kind live
    for ts_kind in ts_kind_list:
        extracted_feature_vectors_dump = glob.glob(feature_extractions_dump + "/" + ts_kind + "/feature_vectors/*.parquet")
        feature_ts_name = feature_ts_without_prefix
        print("extracting p_values for feature time series: {}".format(feature_ts_name))

        # get all the features in a single dataframe in an efficient way.
        feature_matrix = read_feature_matrix_for_given_feature_timeseries(feature_time_series_name = feature_ts_name,ts_kind =ts_kind, extracted_features_paths = extracted_feature_vectors_dump, encoding = encoding, dictionary_key = features_dict_key)
        print(feature_matrix)
        #assert feature_matrix.isnull().values.any() == False

        # get the response from the parquet file and then drop this column from the feature matrix
        response = feature_matrix["response"]
        feature_matrix = feature_matrix.drop(labels = "response", axis = "columns").dropna(axis = "columns")

        # get the p_values for each feature
        selector = FeatureSelector()
        selector.fit(X = feature_matrix, y  = response.astype(int))
        p_values = pd.DataFrame({'feature':selector.features,'p_value':selector.p_values})

        print(p_values)

        # write the p values to a file
        p_values.to_parquet(path = feature_extractions_dump + "/{}/p_values/{}_array_chunk_num_{}_p_values.parquet".format(ts_kind,ts_kind,array_task_id), index = True, engine = "fastparquet", compression = "snappy")
        end = time.time()
        with open("p_value_extraction_log.log", "a+") as f:
             f.write("P VALUE EXTRACTION for ts_kind {} in TIME: {}\n".format(feature_ts_name, end - start))

        print("sucessfully wrote p_values for ts_kind {}".format(feature_ts_name))




