import sys
from pathlib import Path
import yaml
import vsb_data
from tsfresh.feature_extraction import MinimalFCParameters
import vsb_utils

WINDOW_LENGTH = 800
NUM_SIGNALS_PER_JOB = 10
# sys.argv[1] is the array task ID which maps to a particular signal to extract
# sys.argv[2] is the yaml config file

# read in the configuration
with open(sys.argv[2], 'r') as stream:
    try:
        config = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)


# make all required directories but dont overwrite or throw errors
for ts_kind in config["keys"]["ts_kinds_to_select_from"]:
     Path(config["target_dirs"]["feature_extraction_target_dir"] + "/" + ts_kind + "/p_values").mkdir(parents=True, exist_ok=True)
     print("made dir: {}".format(config["target_dirs"]["feature_extraction_target_dir"] + "/" + ts_kind + "/p_values"))
     Path(config["target_dirs"]["feature_extraction_target_dir"] + "/" + ts_kind + "/feature_vectors").mkdir(parents=True, exist_ok=True)

# refresh the cache which stores the dropped features to get rid of duplicates
vsb_utils.streamline_dropped_features_cache()

# get training data
train_data = vsb_data.VSBData(pq_path = config["file_paths"]["data_path"], metadata = config["file_paths"]["metadata_path"], is_labelled = config["keys"]["is_train_data"])


fdr_level = max(config["keys"]["fdr_levels"])


# get measurement IDs based on key: (BALANCED or REMAINDER)
measurement_ids = vsb_utils.measurement_id_selector(key = config["keys"]["measurement_ids_key"],VSB_data_obj =  train_data, is_already_loaded = False)

print("HERE IS THE VECTOR OF {} MEAS_IDS WHICH WILL BE EXTRACTED UPON: {}".format(len(measurement_ids),measurement_ids))

# get features dictionary based on key (EFFICICENT, MINIMAL, RELEVANT).
first_fc_params, second_fc_params, is_kind = vsb_utils.features_dict_selector(key = config["keys"]["features_dict_key"], path_to_relevant_feature_dictionaries = config["target_dirs"]["path_to_relevant_feature_dictionaries"], fdr_level = fdr_level)

# get the directory where the extracted features will be dumped
feature_extractions_dir = config["target_dirs"]["feature_extraction_target_dir"]

# script takes a command line argument which defines the array task id 0,1,...N-1 where N-1 is 2*the number of measurements with faults


vsb_data.script_extract_simple_features_parallel(array_task_id = int(sys.argv[1]),
                                     m_ids = measurement_ids,
                                     vsb_data_obj = train_data,
                                     first_fc_params = first_fc_params,
                                     feature_extractions_dir = feature_extractions_dir,
                                     ts_kind_list = config["keys"]["ts_kinds_to_select_from"],
                                     valid_ts_kinds = config["keys"]["valid_ts_kinds"],
                                     num_signals_per_job = NUM_SIGNALS_PER_JOB,
                                     window_length = WINDOW_LENGTH)
