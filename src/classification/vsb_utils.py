import yaml
import pandas as pd
from tsfresh.feature_extraction.settings import MinimalFCParameters, EfficientFCParameters, from_columns

# Functions useful for configuration:

def measurement_id_selector(key, VSB_data_obj, is_already_loaded):
    '''
    Function that selects the relevant measurement IDS based on a key
   
    
       params:
           key (str): BALANCED, REMAINING or ALL measurement IDs
           VSB_data_obj (VSBData): the full VSB dataset we are working with
           is_already_loaded (bool): DEPRECATED. True is the IDs have been written out to a file, otherwise false 

       returns:
           meas_ids (list): the relevant measurement ids for the task at hand.  


    '''
    assert key == "BALANCED" or key == "REMAINING" or key == "ALL"

    if key == "BALANCED": meas_ids = VSB_data_obj.mids_train # training
    elif key == "REMAINING": meas_ids = VSB_data_obj.mids_test # for testing on representitave subset
    elif key == "ALL": meas_ids = VSB_data_obj.get_all_ids() # all ids for kaggle

    return meas_ids


def features_dict_selector(key,path_to_relevant_feature_dictionaries, fdr_level):
    ''' 
    Function to select the correct features dictionary
    
       params:
           key (str): key for the features set used. EFFICIENT (full dictionary) MINIMAL (full dictionary) RELEVANT_FROM_* (dictionaries after selecting features)
           path_to_relevant_feature_dictionaries (str): Where the relevant feature dictionaries are located
           fdr_level (float): The FDR level

       returns:
           first_features_dict, second_features_dict (dict): feature sets for feature time series extracttion and feature dynamics extraction
           is_kind (bool): true if the dictionaries map individual ts_kinds to distinct feature sets, false otherwise
    '''

    assert key == "EFFICIENT" or key == "MINIMAL" or key == "RELEVANT_FROM_MINIMAL" or key == "RELEVANT_FROM_EFFICIENT"
    if key == "EFFICIENT":
        first_features_dict,second_features_dict, is_kind = efficient_FC_parameters_wrapped()
    elif key == "MINIMAL":
        first_features_dict,second_features_dict, is_kind = minimal_fc_parameters_wrapped()
    else: first_features_dict,second_features_dict, is_kind = relevant_FC_parameters(key = key, path_to_relevant_feature_dictionaries = path_to_relevant_feature_dictionaries, fdr_level = fdr_level)

    return first_features_dict, second_features_dict, is_kind

def relevant_FC_parameters(key, path_to_relevant_feature_dictionaries, fdr_level):
    ''' 
    Get the relevant dictionary using dictionary on dictionary strategy.

        params:
            key (str): EFFICIENT or MINIMAL representing the main feature set that features were selected from
            path_to_relevant_feature_dictionaries (str): Where the relevant feature dictionaries are located.
            fdr_level (float): the FDR level
       
        returns:
            first_feature_params, second_feature_params (dict): feature sets for feature time series extracttion and feature dynamics extraction
            is_kind (bool): true if the dictionaries map individual ts_kinds to distinct feature sets, false otherwise

    '''
    with open(path_to_relevant_feature_dictionaries + "FDR_" + str(fdr_level) + "_first_relevant_feature_dict_from_" + key.split("RELEVANT_FROM_")[1].lower() + ".yaml", "r") as f:
        first_feature_params = yaml.load(f)
    with open(path_to_relevant_feature_dictionaries + "FDR_" + str(fdr_level) + "_second_relevant_feature_dict_from_" + key.split("RELEVANT_FROM_")[1].lower() + ".yaml", "r") as f:
        second_feature_params = yaml.load(f)

    is_kind = True

    return first_feature_params, second_feature_params, is_kind


def minimal_fc_parameters_wrapped():
    '''
    Returns the features dictionary defined by MinimalFCParameters() in a form that works well in the ML pipeline developed
        Returns:
            feature_params (dict): feature dictionary MinimalFCParameters less the "banned features"
            is_kind (bool): true if the dictionaries map individual ts_kinds to distinct feature sets, false otherwise 
    '''
    feature_params = MinimalFCParameters()


    with open("dropped_feature_names.txt", "r") as f:
        feature_names_to_remove = [name.strip() for name in f.readlines()]

    for name in set(feature_names_to_remove):
        if name in feature_params: del feature_params[name]

    is_kind = False
    return feature_params, feature_params, is_kind


def efficient_FC_parameters_wrapped():
    '''
    Returns the features dictionary defined by EfficientFCParameters() in a form that works well in the ML pipeline developed
    
        Returns:
            feature_params (dict): feature dictionary EfficientFCParameters less the "banned features"
            is_kind (bool): true if the dictionaries map individual ts_kinds to distinct feature sets, false otherwise 
    '''
    feature_params = EfficientFCParameters()
    del feature_params["benford_correlation"]

    with open("dropped_feature_names.txt", "r") as f:
        feature_names_to_remove = [name.strip() for name in f.readlines()]

    for name in set(feature_names_to_remove):
        if name in feature_params: del feature_params[name]

    is_kind = False
    return feature_params, feature_params, is_kind


def derive_features_dictionaries(filtered_features, dicts_from, replacement_token, path_to_relevant_feature_dictionaries, fdr_level):
    '''
    Derives and writes out two feature dictionaries which can be used in the features_on_features framework.
    
    Should return the dictionaries as a single object, and a flag specifying what type of dictionary... i.e. if it is columns --> feature dict
    
        params:
            filtered_features (list): the relevant features in the form of <ts_kind>||<feature_time_series>__<feature_dynamic> 
            dicts_from (str): the key EFFICIENT or MINIMAL. 
            replacement_token (str):  token that replaces double unscore in feature naming convention. This adjustment is required for featue dynamics extraction
            path_to_relevant_feature_dictionaries (str): Where the relevant feature dictionaries are located.
            fdr_level (float): The FDR level
    '''
    assert dicts_from == "MINIMAL" or dicts_from == "EFFICIENT"


    # map names back from the encoding to the actual feature names
    with open("feature_encoding.yaml".format(dicts_from), "r") as f:
        encoding = yaml.safe_load(f)

    # from encoded filtered features to actual filtered features..
    feature_on_feature_names = []
    for encoded_name in filtered_features:
        encoded_name_prefix, encoded_name = encoded_name.split(replacement_token)
        for full_name_without_prefix, encoded_name_with_prefix in encoding.items():
            if encoded_name_with_prefix.replace("PREFIX" + replacement_token, "") == encoded_name:
                full_name_with_prefix = encoded_name_prefix + full_name_without_prefix
                feature_on_feature_names.append(full_name_with_prefix)



    f_on_f_mapping = from_columns(feature_on_feature_names)
    f_mapping = from_columns([str(x).replace(replacement_token,"__") for x in [*f_on_f_mapping]])

    assert "measurement_id" not in f_on_f_mapping and "measurement_id" not in f_mapping

    # now write these dictionaries to objects
    print("Writing relevant dicts to {}".format(path_to_relevant_feature_dictionaries))
    try:
        with open(path_to_relevant_feature_dictionaries + "FDR_" + str(fdr_level) + '_first_relevant_feature_dict_from_' + dicts_from.lower() + ".yaml", 'w') as f:
            yaml.dump(f_mapping, f)
        with open(path_to_relevant_feature_dictionaries + "FDR_" + str(fdr_level) + '_second_relevant_feature_dict_from_' + dicts_from.lower() + ".yaml", 'w') as f:
            yaml.dump(f_on_f_mapping, f)

    except Exception as e:
        print(e)
        print("ERROR: DEBUG")


def streamline_dropped_features_cache():
    ''' removes duplicates in the cache. Not essential to run'''

    with open("dropped_feature_names.txt", "r") as f:
        feature_names_in_cache = [name.strip() for name in f.readlines()]
    unique_names = list(set(feature_names_in_cache))
    with open("dropped_feature_names.txt", "w") as f:
       for feature in unique_names: f.write(feature + "\n")
