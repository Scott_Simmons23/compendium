import yaml
import sys
import vsb_data
from pathlib import Path



# read in the configuration
with open(sys.argv[2], 'r') as stream:
    try:
        config = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)

array_task_id = int(sys.argv[1])

fdr_levels = config["keys"]["fdr_levels"]
assert array_task_id < len(fdr_levels)
fdr_level = fdr_levels[array_task_id]

train_data = vsb_data.VSBData(pq_path = "train.parquet", metadata = "metadata_train.csv", is_labelled = True)



# run the model fitting which will include selection
vsb_data.minimal_model_fitting(fdr_level = fdr_level, features_dump_path = config["target_dirs"]["feature_extraction_target_dir"], VSB_obj = train_data)
