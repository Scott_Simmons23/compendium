# Description of structure

This directory contains all the code and dependancies needed to reproduce results in the NESI environment. 

It has brief details about reproducing results. Contact ssim323@aucklanduni.ac.nz if there are any questions.

## Main code modules

* `vsb_data.py`

    Used for feature extraction, feature selection, and model evaluation procedures.

* `vsb_utils.py`
    
    Main purpose is to aid the interaction of data in the config files with the code inside `vsb_data.py`

* `vsb_model_interpretation.py`

    Contains code for conditional histograms and P-R curves

## Python Scripts, python scripts, and config files

Slurm scripts (.sl) are run on nesi using sbatch\*.sl. They have been written to take config files (config_\*.yaml) as input. These Slurm scripts call python scripts. Lastly, python scripts call functions in `vsb_data.py` and `vsb_utils.py` 

The config files contain information on directories, the FDR levels used, the location of the data, among other things.

There are 3 slurm scripts used which are run in the nesi environment (and they should work in any environment that uses the slurm workload manager).

- `extract_features_batch_script.sl` extracts feature dynamics for the largest FDR level found in the config file. 

- `extract_simple_features_batch_script.sl` extracts features for the largest FDR level found in the config file.

- `minimal_model_fitting.sl` (evaluates models with different FDR levels). Run with `config_extract_features_on_all_train_data_MINIMAL.yaml` for the feature dynamics model, and run with `config_extract_all_simple_features_MINIMAL.yaml` for the benchmark model. 

## Process of reproducing results

NOTE: You will need access to the slurm workload manager to reproduce the results.

- 1. Go to https://www.kaggle.com/c/vsb-power-line-fault-detection/data to download the data.

- 2. Run: `sbatch extract_features_batch_script.sl` and `sbatch extract_simple_features.sl`

- 3. Open: `minimal_model_fitting.sl` and uncomment the command under FEATURE DYNAMICS so that code to fit and evaluate the feature dynamics models is used. Then do the same for the benchmark model by instead uncommenting the line under SIMPLE FEATURES.

- 4. Inspect the slurm outputs for the model results for each FDR level the slurm output files contain information . Additionally, the model objects are written to the Models_SIMPLE and Models_DYNAMICS directories.

- 5. There may be FDR levels that are of interest (for example FDR levels that correspond to the "best model"). In `vsb_data.minimal_model_fitting()`, uncomment lines that write feature importances and p-values to files. Re-run `sbatch minimal_model_fitting.sl` with the array_task_ids correponding to FDR level(s) of interest. This will write the p-values, predictions, and feature importances to files which can then be used in the report.

## Jupyter notebooks

* `explanatory_figs.ipynb` shows steps required to get the figures used to interpret the models described in the report. 

* `model_figs.ipynb` shows steps to get the figures used as explanatory guides in the report. 

## Deprecated code in vsb_data

* Code in the vsb_data that was not used in the final report is still in the file at the bottom. The reason for deprecated code is that a larger feature space was being explored, which led to computational bottlenecks. This required a different process which is not detailed here nor is it reproducable here. 

