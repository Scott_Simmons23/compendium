import pandas as pd
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import auc
import matplotlib.patches as mpatches
import matplotlib
from imblearn.under_sampling import RandomUnderSampler
from sklearn.model_selection import cross_val_score, RepeatedStratifiedKFold, train_test_split
import glob
import numpy as np
import seaborn as sns
import vsb_data
import vsb_utils
import matplotlib.pyplot as plt
from fastparquet import ParquetFile
from sklearn.metrics import roc_curve, roc_auc_score


def ROC(roc_csv_path):
    '''
    Plots Precision Recall curve given a csv that contains the response and predicted probabilities
    (yes, ROC is a bad name for a PR curve plot function)
    '''
    roc_data = pd.read_csv(roc_csv_path)
    print(roc_data)
    y_prob = roc_data["prediction_prob"].values
    y_actual = roc_data["actual"].values
    precision, recall, thresholds = precision_recall_curve(y_actual, y_prob)


    # plot
    plt.subplots(1, figsize=(10,10))
    plt.title('Precision-Recall Curve')
    plt.plot(recall, precision, marker='.', label='LGBM with no feature dynamics',color = "orange")
    plt.ylabel('Precision')
    plt.xlabel('Recall')
    print("pr_auc_score:", auc(recall, precision))
    plt.legend()
    plt.show()

def conditional_hist(feature_name,n,X,y):
    '''
    Plots conditional feature distributions for fault and non faults.

        params:
            feature_name (str): The encoded name of the feature
            n (int): the feature ranking (printed on the plot)
            X (pd.DataFrame): the feature matrix
            y (pd.Series): the response vector

    '''
    font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 22}

    matplotlib.rc('font', **font)
    f,ax = plt.subplots(figsize=(10, 8), dpi=80)
    print(feature_name)


    # read stuff in
    X = pd.read_csv(X)
    y = pd.read_csv(y)

    feature = X[feature_name]
    df = pd.concat([feature, y],axis = 1)
    df.rename(columns={feature_name: 'feature', 'target': 'response'}, inplace=True)
    print(df)

    sns.histplot(data=df,hue = "response", x = "feature")#, #stat = "density")

    # log transform
    #feature_and_response["logged_feature"] = np.log(1+ feature_and_response[feature_name]- min(feature_and_response[feature_name]))
    #print("There are this many faults in the data:{}".format(len(feature_and_response[feature_and_response["response"] == 1].index)))
    #sns.histplot(data=feature_and_response,hue = "response", x = feature_name, stat = "density")

    ax.set_xlabel("Feature value")
    ax.set_ylabel("Frequency")
    ax.set_title("Feature distribution. Feature ranking: {}".format(n))
    orange_patch = mpatches.Patch(color='sandybrown', label='Faults')
    blue_patch = mpatches.Patch(color='lightskyblue', label='Non Faults')
    plt.legend(handles=[blue_patch,orange_patch])

    mssg = "feature_{}_ranking_{}.png".format(feature_name,n)
    f.savefig(mssg, dpi=f.dpi)
    plt.show()


def set_size(width, fraction=1):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float
            Document textwidth or columnwidth in pts
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure (in pts)
    fig_width_pt = width * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim





def RSAM_windowed_time_series_explaination_plot():
    '''
    Generates figures showing how RSAM feature time series is made.
    '''
    # 100HZ
    # 40 minutes
    # filtered for the 2-5 HZ frequency range...
    # sneed.gzip is from code written by louis that is 40 minutes of row seismic data.
    pf = ParquetFile("sneed.gzip")
    df = pf.to_pandas()
    print(len(df.index))


    df["time in minutes"] = df.index / (100 *60)
    df["seismic velocity"] = df["rsam"]
    print(df)

    fig, ax = plt.subplots()
    ax = sns.lineplot(data = df, x = "time in minutes", y = "seismic velocity", color = "blue", label = "Raw Seismic Velocity", alpha = 0.6)
    ax.set(xlabel = "Time [minutes]", ylabel = "Raw seismic velocity [m/s]")
    ax.set_title("40 minutes of raw seismic velocity data")
    ax.legend()
    fig.savefig("RSAM_raw.png")


    # window up into 10 min intervals
    df = vsb_data.segment_ts(n = 4, ts = df)
    print(df)

    fig, ax = plt.subplots()
    ax = sns.lineplot(data = df, x = "time in minutes", y = "seismic velocity", hue = "window_id")
    for window_id in df["window_id"].unique().tolist():
        plt.axvline(x=df[df["window_id"] == window_id]["time in minutes"].iloc[0], linestyle = "--", color = "k")

    ax.set(xlabel = "Time [minutes]", ylabel = "Raw seismic velocity [m/s]")
    ax.set_title("Windowing the raw input time series")
    plt.legend([],[], frameon=False)
    fig.savefig("RSAM_raw_windowed.png")

    # plot the RSAM
    rsam_function = lambda series: series.abs().mean()
    first_feature = df.groupby('window_id').agg({'rsam': [rsam_function],'time in minutes':['min']})
    first_feature_index = first_feature["time in minutes"]["min"]
    print(first_feature)
    first_feature_values = first_feature["rsam"]["<lambda>"]
    ax = sns.scatterplot(x = first_feature_index, y = first_feature_values,  s = 50, color = "red", zorder = 20) # make a scatter
    ax.set(xlabel = "Time [minutes]", ylabel = "Raw seismic velocity [m/s]")
    ax.set_title("Creating the 10 mimute RSAM feature time series")
    ylims_list = ax.get_ylim()
    plt.legend([],[], frameon=False)
    fig.savefig("RSAM_feature_time_series_windowed_.png")
    plt.show()


    print("success")




