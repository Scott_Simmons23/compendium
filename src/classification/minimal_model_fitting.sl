#!/bin/bash -e

#SBATCH --job-name    MinimalFIT
#SBATCH --time        00:20:00
#SBATCH --mem         5GB
#SBATCH --cpus-per-task=4
#SBATCH --array=0-13
module load Python/3.8.2-gimkl-2020a
export SLURM_EXPORT_ENV=ALL




# SCRIPTS

#1
# For features on features array = 6 expect 0.611 MCC OSS
# FEATURE DYNAMICS
#python minimal_model_fitting_script.py $SLURM_ARRAY_TASK_ID config_extract_features_on_all_train_data_MINIMAL.yaml 


#2
# For simple features
# SIMPLE FEATURES
python minimal_model_fitting_script.py $SLURM_ARRAY_TASK_ID config_extract_all_simple_features_MINIMAL.yaml























# NOTES:
# CONFIG for the best model with best features
# 0.85 0.15 train test 
# 3 folds
# set intersection
# FDR of 0.3


# config for the best simple model
# 0.85 0.15 split
# 3 folds
# set intersection
# FDR of 1.0
