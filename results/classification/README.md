# Structure

### best_model_data

Contains the feature importances, p_values, and prediction data for the best feature dynamics model (FDR = 0.3) and the best benchmark model (FDR = 1.0). Also includes intermediate data as the feature matrix and the response vector for the train and test data.

### plots

The plots which were used for the reports + other plots.

### log_files

Log files for feature extraction, selection, and model evaluation.

### previous_benchmark_models, previous models

Models that were fit over the course of the project. The MCC printed is the out of sample MCC score. 
