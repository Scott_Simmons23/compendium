# README #

# Machine Learning for Extremely Long Time Series.

* This repository stores the results, data, and code, literature review for the **Machine Learning for Extremely Long Time Series** Part 4 Project  

## Structure

The compendium is structured in the following way:


- **src** - contains source code and notebooks required to reproduce figures and results. This code is able to reproduce the results in the NESI environment or an environment which can use the slurm workload manager.



- **results** - contains intermediate data, modelling results, and figures.

***

The project was run on two benchmark problems: Time Series Classification and Regression. The directories are also partitioned in this way.

### Contact Details ###

* ssim323@aucklanduni.ac.nz
* ljar069@aucklanduni.ac.nz



